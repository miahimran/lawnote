package com.txlabz.lawnote.ViewHolder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.interfaces.AnswerListener;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public abstract class BaseViewHolderTemplateQuestions extends RecyclerView.ViewHolder {

    public View mContentContainer;
    public CardView mCardView;
    public TextView mTitle;
    public ImageView ivInfo;
    protected AnswerListener answerListener;


    public BaseViewHolderTemplateQuestions(View itemView) {
        super(itemView);
        mContentContainer = itemView.findViewById(R.id.content_container);
        mCardView  = (CardView) itemView.findViewById(R.id.card_view);
        mTitle     = (TextView) itemView.findViewById(R.id.tv_title);
        ivInfo= (ImageView) itemView.findViewById(R.id.ivInfo);

    }

    public void setAnswerListener(AnswerListener answerListener) {

        this.answerListener=answerListener;
    }
}
