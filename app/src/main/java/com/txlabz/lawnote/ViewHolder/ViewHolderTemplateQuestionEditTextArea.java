package com.txlabz.lawnote.ViewHolder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.AddEditDescriptionActivity;
import com.txlabz.lawnote.activities.BaseActivity;
import com.txlabz.lawnote.dialogUtil.GetDescriptionDialog;
import com.txlabz.lawnote.interfaces.OnResultListener;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class ViewHolderTemplateQuestionEditTextArea  extends BaseViewHolderTemplateQuestions implements OnResultListener {

    public TextView  mAddEditDescription;
    public TextView mDescDetails;

    public View  mEditAreaContainer;
    private QuestionModel questionModel;


    public ViewHolderTemplateQuestionEditTextArea(View itemView) {
        super(itemView);
        mCardView  = (CardView) itemView.findViewById(R.id.card_view);
        mTitle     = (TextView) itemView.findViewById(R.id.tv_title);


        mEditAreaContainer= itemView.findViewById(R.id.edit_text_area);
        mAddEditDescription = (TextView) itemView.findViewById(R.id.tv_add_edit_description);
        mDescDetails  = (TextView) itemView.findViewById(R.id.tv_description_details);
    }

    public  void setupEditAreaView(final Context context, final QuestionModel questionModel) {

        this.questionModel=questionModel;
        mDescDetails.setText(questionModel.getAnswer());
        mAddEditDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle=new Bundle();
                bundle.putString(AppConstants.QUESTION_TEXT, questionModel.getQuestionText());
                bundle.putString(AppConstants.QUESTION_DESC, mDescDetails.getText().toString());
                bundle.putInt(AppConstants.POSITION, getAdapterPosition());


                GetDescriptionDialog getDescriptionDialog=new GetDescriptionDialog(ViewHolderTemplateQuestionEditTextArea.this);
                getDescriptionDialog.setArguments(bundle);

                FragmentTransaction transaction= ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                // For a little polish, specify a transition animation
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                // To make it fullscreen, use the 'content' root view as the container
                // for the fragment, which is always the root view for the activity
                transaction.add(android.R.id.content, getDescriptionDialog)
                        .addToBackStack(null).commit();

            }
        });
    }


    @Override
    public void onDone(String newDesc) {

        mDescDetails.setText(newDesc);
        questionModel.setAnswer(newDesc);

        if(answerListener!=null)
        {
            answerListener.onAnswerSet(newDesc);
        }


    }
}