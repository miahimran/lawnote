package com.txlabz.lawnote.ViewHolder;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.OptionsModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;

import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class ViewHolderTemplateQuestionSpinner  extends BaseViewHolderTemplateQuestions {


    public Spinner mSpinner;



    public ViewHolderTemplateQuestionSpinner(View itemView) {
        super(itemView);
        mSpinner      = (Spinner) itemView.findViewById(R.id.spinner);

    }

    public void setupSpinnerOptions(Context context, QuestionModel questionModel) {
        Log.d(AppConstants.TAG,"Handle spinner options.");


        final ArrayList<OptionsModel> spinnerOptions=questionModel.getOptionsModel();
        ArrayAdapter<OptionsModel> spinnerArrayAdapter = new ArrayAdapter<OptionsModel>(context, android.R.layout.simple_spinner_item, spinnerOptions); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(spinnerArrayAdapter);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(answerListener!=null)
                {
                    answerListener.onAnswerSet(spinnerOptions.get(i).getOptionValue());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if(!TextUtils.isEmpty(questionModel.getAnswer())) {
            int selectedOp= Integer.parseInt(questionModel.getAnswer());
           if(selectedOp>0) mSpinner.setSelection(selectedOp-1);

        }
    }
}
