package com.txlabz.lawnote.ViewHolder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.activities.CreateYourOwnActivity;
import com.txlabz.lawnote.adapters.TemplateAdapter;
import com.txlabz.lawnote.fragments.TemplatesListFragment;
import com.txlabz.lawnote.interfaces.CallbackListener;
import com.txlabz.lawnote.modelClasses.Categories;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.StorageUtility;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 10/25/2016.
 */
public class TemplateListItemViewHolder extends RecyclerView.ViewHolder {

    public TextView heading,desc,date;
    public RelativeLayout rel_main;


    public TemplateListItemViewHolder(View itemView) {
        super(itemView);

        heading=(TextView)itemView.findViewById(R.id.heading);
        desc=(TextView)itemView.findViewById(R.id.desc);
        date=(TextView)itemView.findViewById(R.id.date);
        rel_main=(RelativeLayout)itemView.findViewById(R.id.rel_main);
    }

    public void setData(final Context context, final TemplateModel templateModel, final int templatesType)
    {
        heading.setText(templateModel.getTemplateTitle());
        desc.setText(templateModel.getTemplateDescription());
        date.setText(Utils.calToStr(Utils.getDateInLocal(templateModel.getDateTime())));

        rel_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTemplateDetail(context,templateModel,templatesType);
            }
        });


    }


    private void showTemplateDetail(Context context,TemplateModel templateModel,int templateType) {

        Intent intent = null;
        if (templateModel.isCreatedByUser()) {
            intent = new Intent(context, CreateYourOwnActivity.class);
            intent.putExtra(AppConstants.IS_FROM_PENDING, "1");
        } else
            intent = new Intent(context,CreateEditTemplateActivity.class);

        intent.putExtra(AppConstants.SELECTED_TEMPLATE,templateModel);
        switch (templateType)
        {
            case TemplatesListFragment.TEMPLATES_TYPE_DRAFT:
                intent.putExtra(CreateEditTemplateActivity.INIT_TABS_FOR,CreateEditTemplateActivity.INIT_TABS_FOR_EDIT_TEMPLATE);break;

            case TemplatesListFragment.TEMPLATES_TYPE_PENDING:
                intent.putExtra(CreateEditTemplateActivity.ALREADY_SAVED,true);
                intent.putExtra(CreateEditTemplateActivity.INIT_TABS_FOR,CreateEditTemplateActivity.INIT_TABS_FOR_EDIT_TEMPLATE);break;

            case TemplatesListFragment.TEMPLATES_TYPE_COMPLETED:
                intent.putExtra(CreateEditTemplateActivity.INIT_TABS_FOR,CreateEditTemplateActivity.INIT_TABS_FOR_COMPLETED_PREVIEW);break;

            case TemplatesListFragment.TEMPLATES_TYPE_RECEIVED:
                intent.putExtra(CreateEditTemplateActivity.INIT_TABS_FOR,CreateEditTemplateActivity.INIT_TABS_fOR_RECEIVED_TEMPLATE);break;
        }

        context.startActivity(intent);

    }


}
