package com.txlabz.lawnote.ViewHolder;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.OptionsModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DynamicViewsHalper;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class ViewHolderTemplateQuestionCheckBox extends BaseViewHolderTemplateQuestions {



    public LinearLayout lytCheckboxHolder;
    private QuestionModel questionModel;


    public ViewHolderTemplateQuestionCheckBox(View itemView) {
        super(itemView);

        lytCheckboxHolder = (LinearLayout) itemView.findViewById(R.id.linearLayout);
    }

    public void setupCheckOptions(Context context, QuestionModel questionModel) {

        lytCheckboxHolder.removeAllViews();
        this.questionModel=questionModel;
        ArrayList<OptionsModel> options = questionModel.getOptionsModel();



        for(int i = 0; i < options.size(); i++) {
            CheckBox cb = new CheckBox(context);


            cb.setTag(options.get(i).getOptionValue());
            cb.setLayoutParams(DynamicViewsHalper.getViewParameters());
            cb.setTypeface(DynamicViewsHalper.getFont(context));

            cb.setOnClickListener(checkBoxClickListener);
            cb.setText(options.get(i).getOptionText());
            lytCheckboxHolder.addView(cb);
        }

        if(!TextUtils.isEmpty(questionModel.getAnswer()))
        {
            String selectedOptionsStr=questionModel.getAnswer();
            int[] selectedOptions = Utils.getSelectedOptions(selectedOptionsStr);

            if(selectedOptions.length>0)
            {
                for(int i=0;i<selectedOptions.length;i++){
                    ((CheckBox)lytCheckboxHolder.getChildAt(selectedOptions[i]-1)).setChecked(true);
                }
            }
        }

    }

    View.OnClickListener checkBoxClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            CheckBox checkBox= (CheckBox) view;
            String  optionValue= (String) checkBox.getTag();

            if(checkBox.isChecked())
            {
                answerListener.onAnswerSet(addValueToAnswer(questionModel.getAnswer(),optionValue));
            }
            else answerListener.onAnswerSet(removeValeFromAnswer(questionModel.getAnswer(),optionValue));
        }
    };

    private String addValueToAnswer(String answer, String optionValue) {

        Log.d(AppConstants.TAG,"Adding option value to answer "+answer+" : "+optionValue);

        if(!TextUtils.isEmpty(answer))
        return answer+","+optionValue;
        else return optionValue;
    }

    private String removeValeFromAnswer(String answer, String optionValue)
    {
        Log.d(AppConstants.TAG,"removing option value to answer "+answer+" : "+optionValue);

        if(TextUtils.isEmpty(answer))
            return "";
      answer=  answer.replace(optionValue,"").trim();

        String[] newOptions = answer.split(",");

        String newAns="";
        if(newOptions.length>0)
            newAns =newOptions[0];

        int i=1;
        if(newOptions.length>1) {

        for(;i<newOptions.length;i++) {
            if(!TextUtils.isEmpty(newOptions[i].trim()))
            newAns+=","+newOptions[i];
        }

        }


        return newAns;
    }
}
