package com.txlabz.lawnote.ViewHolder;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.ProfileQuestionActivity;
import com.txlabz.lawnote.modelClasses.OptionsModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DynamicViewsHalper;

import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class ViewHolderTemplateQuestionRadio  extends BaseViewHolderTemplateQuestions {


    public RadioGroup mRadioGroup;

    public ViewHolderTemplateQuestionRadio(View itemView) {
        super(itemView);

        mRadioGroup   = (RadioGroup) itemView.findViewById(R.id.radioGroup);

    }

    public  void setupRadioView(Context context, QuestionModel questionModel) {

        mRadioGroup.removeAllViews();


        final ArrayList<OptionsModel> optionsModelArrayList = questionModel.getOptionsModel();
        for (int i=0; i<optionsModelArrayList.size(); i++) {

            RadioButton radioButton=new RadioButton(context);


            radioButton.setLayoutParams(DynamicViewsHalper.getViewParameters());
            radioButton.setTypeface(DynamicViewsHalper.getFont(context));

            radioButton.setText(optionsModelArrayList.get(i).getOptionText());
            mRadioGroup.addView(radioButton);
        }

        if(!TextUtils.isEmpty(questionModel.getAnswer()))
        {
            int value= Integer.parseInt(questionModel.getAnswer());
            if(value>0)
            {
                RadioButton r = (RadioButton) mRadioGroup.getChildAt(value-1);
                if(r!=null) r.setChecked(true);
            }

        }



        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i=0; i<optionsModelArrayList.size(); i++)
                {

                    RadioButton r=(RadioButton) mRadioGroup.getChildAt(i);
                    if (r.isChecked())
                    {

                        String radioItem=optionsModelArrayList.get(i).getOptionValue();
                        if(answerListener!=null) {
                            answerListener.onAnswerSet(radioItem);
                            break;
                        }

                    }
                }
            }
        });

    }


}
