package com.txlabz.lawnote.ViewHolder;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.DateUtil;
import com.txlabz.lawnote.utils.Utils;
import java.util.Calendar;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class ViewHolderTemplateQuestionDateInput  extends BaseViewHolderTemplateQuestions {

    public TextView tvPickDate;


    public ViewHolderTemplateQuestionDateInput(View itemView) {
        super(itemView);
        tvPickDate = (TextView) itemView.findViewById(R.id.tvPickDate);

    }

    public void setupDateView(final Context context, final QuestionModel questionModel) {
        final String optionAnswer = questionModel.getAnswer();
        final Calendar myCalender= DateUtil.parserServerDate(optionAnswer);

        if(!TextUtils.isEmpty(optionAnswer))
        tvPickDate.setText(optionAnswer);


        tvPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utils.showDatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                        myCalender.set(Calendar.YEAR,i);
                        myCalender.set(Calendar.MONTH,i1);
                        myCalender.set(Calendar.DAY_OF_MONTH,i2);

                        tvPickDate.setText(DateUtil.formatLocalDateFormat(myCalender));

                        if(answerListener!=null)
                        {
                            answerListener.onAnswerSet(DateUtil.formatServerDateFormat(myCalender));
                        }
                    }
                },false,true,myCalender);
            }
        });


    }


}