package com.txlabz.lawnote.ViewHolder;

import android.provider.SyncStateContract;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.interfaces.OnFocusListener;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class ViewHolderTemplateQuestionEditText  extends BaseViewHolderTemplateQuestions {

    private final OnFocusListener onFocuListener;
    private final View parentView;
    public EditText etAnswerInput;
    private static String TAG="LN_EDIT_TEXT";


    public ViewHolderTemplateQuestionEditText(View itemView,OnFocusListener onFocusListener) {
        super(itemView);
        etAnswerInput = (EditText) itemView.findViewById(R.id.question);
        parentView=itemView.findViewById(R.id.card_view);
        this.onFocuListener=onFocusListener;

    }

    public void clearFocus(){
        etAnswerInput.clearFocus();
    }
    public void setupEditTextView(final QuestionModel questionModel) {
        final String answer = questionModel.getAnswer();
        if (answer.length() > 0) {
            etAnswerInput.setText(answer);
        } else
            etAnswerInput.setText("");

        etAnswerInput.clearFocus();
        final TextWatcher textWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(answerListener!=null)
                {
                    answerListener.onAnswerSet(s.toString());
                }

            }
        };

        etAnswerInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etAnswerInput.addTextChangedListener(textWatcher);
                    if(onFocuListener!=null){
                        onFocuListener.onFocusReceived(parentView);
                    }
                  //  Log.d(TAG,"Scroll to view ");
                }else {
                    etAnswerInput.removeTextChangedListener(textWatcher);

                    if(onFocuListener!=null){
                        onFocuListener.onFocusLost(parentView);
                    }
                  //  Log.d(TAG,"Scroll to view close.");

                }
            }
        });
    }


}
