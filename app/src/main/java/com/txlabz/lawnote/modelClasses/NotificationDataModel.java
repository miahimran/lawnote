package com.txlabz.lawnote.modelClasses;

import com.google.gson.annotations.SerializedName;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.StringValueParser;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 12/08/2017.
 */

public class NotificationDataModel {
    @SerializedName("notification")
    private ArrayList<NotificationModel> notificationModel;
    @SerializedName("received_notification_count")
    private int receivedNotificationCount;
    @SerializedName("completed_notification_count")
    private int completedNotificationCount;
    @SerializedName("pending_notification_count")
    private int pendingNotificationCount;

    public ArrayList<NotificationModel> getNotificationModel() {
        return notificationModel;
    }

    public int getReceivedNotificationCount() {

        if(notificationModel==null||notificationModel.size()==0)
            return 0;

        int notificationCount=0;
        for(int i=0;i<notificationModel.size();i++)
        {
            if(StringValueParser.parseInt(notificationModel.get(i).getFlag())== AppConstants.NOTIFICATION_TYPE_RECEIVED||
                    StringValueParser.parseInt(notificationModel.get(i).getFlag())== AppConstants.NOTIFICATION_TYPE_SIGNED_BY_SENDER)
                notificationCount++;
        }
        return notificationCount;

      // return receivedNotificationCount;
    }

    public int getCompletedNotificationCount() {

        if(notificationModel==null||notificationModel.size()==0)
            return 0;

        int notificationCount=0;
        for(int i=0;i<notificationModel.size();i++)
        {
            if(StringValueParser.parseInt(notificationModel.get(i).getFlag())== AppConstants.NOTIFICATION_TYPE_COMPLETED)
                notificationCount++;
        }
        return notificationCount;
//        return completedNotificationCount;
    }

    public int getPendingNotificationCount() {

        if(notificationModel==null||notificationModel.size()==0)
            return 0;

        int notificationCount=0;
        for(int i=0;i<notificationModel.size();i++)
        {
            if(StringValueParser.parseInt(notificationModel.get(i).getFlag())== AppConstants.NOTIFICATION_TYPE_SIGNED_BY_OTHER_PENDING)
                notificationCount++;
        }
        return notificationCount;
      //  return pendingNotificationCount;
    }

    public void setNotificationModel(ArrayList<NotificationModel> notificationModel) {
        this.notificationModel = notificationModel;
    }
}
