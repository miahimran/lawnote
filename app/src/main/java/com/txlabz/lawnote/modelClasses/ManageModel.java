package com.txlabz.lawnote.modelClasses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 10/25/2016.
 */
public class ManageModel implements Serializable {


    @SerializedName("status")
    String status;

    @SerializedName("drafts")
    public ArrayList<TemplateModel> draftsTemplateModels;


    @SerializedName("pending")
    public ArrayList<TemplateModel> pendingTemplateModels;


    @SerializedName("completed")
    public ArrayList<TemplateModel> completedTemplateModels;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<TemplateModel> getDraftsTemplateModels() {
        return draftsTemplateModels;
    }

    public void setDraftsTemplateModels(ArrayList<TemplateModel> draftsTemplateModels) {
        this.draftsTemplateModels = draftsTemplateModels;
    }

    public ArrayList<TemplateModel> getPendingTemplateModels() {
        return pendingTemplateModels;
    }

    public void setPendingTemplateModels(ArrayList<TemplateModel> pendingTemplateModels) {
        this.pendingTemplateModels = pendingTemplateModels;
    }

    public ArrayList<TemplateModel> getCompletedTemplateModels() {
        return completedTemplateModels;
    }

    public void setCompletedTemplateModels(ArrayList<TemplateModel> completedTemplateModels) {
        this.completedTemplateModels = completedTemplateModels;
    }
}
