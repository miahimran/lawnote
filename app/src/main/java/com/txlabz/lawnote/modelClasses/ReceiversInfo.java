package com.txlabz.lawnote.modelClasses;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali Sabir on 8/24/2017.
 */

public class ReceiversInfo implements Serializable {


    @SerializedName("receiver_first_name")
    private String firstName;
    @SerializedName("receiver_last_name")
    private String lastName;
    @SerializedName("receiver_email")
    private String email;
    @SerializedName("receiver_signature_image")
    private String signatureImage;
    private Bitmap signatureBitmap;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(String signatureImage) {
        this.signatureImage = signatureImage;
    }

    public String getFullName() {

        return firstName+" "+lastName;
    }

    public void setSignatureBitmap(Bitmap signatureBitmap) {
        this.signatureBitmap = signatureBitmap;
    }

    public Bitmap getSignatureBitmap() {
        return signatureBitmap;
    }
}
