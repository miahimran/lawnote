package com.txlabz.lawnote.modelClasses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Fatima Siddique on 10/21/2016.
 */
public class OptionsModel  implements Serializable {

    public String toString()
    {
        return optionText;
    }
    @SerializedName("option_id")
    String optionID;

    @SerializedName("option_text")
    String optionText;

    @SerializedName("option_value")
    String optionValue;

    public String getOptionID() {
        return optionID;
    }

    public void setOptionID(String optionID) {
        this.optionID = optionID;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }
}
