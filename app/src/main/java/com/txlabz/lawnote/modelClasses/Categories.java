package com.txlabz.lawnote.modelClasses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 9/9/2016.
 */
public class Categories implements Serializable {

    @SerializedName("category_id")
    String categoryId;

    @SerializedName("category_title")
    String categoryTitle;

    @SerializedName("category_description")
    String categoryDescryption;

    @SerializedName("category_icon")
    String categoryIcon;

    @SerializedName("category_image")
    String categoryImage;

    @SerializedName("templates")
    public ArrayList<TemplateModel> templateModels;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<TemplateModel> getTemplateModels() {
        return templateModels;
    }

    public void setTemplateModels(ArrayList<TemplateModel> templateModels) {
        this.templateModels = templateModels;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryDescryption() {
        return categoryDescryption;
    }

    public void setCategoryDescryption(String categoryDescryption) {
        this.categoryDescryption = categoryDescryption;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }
}
