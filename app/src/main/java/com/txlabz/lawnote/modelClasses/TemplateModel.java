package com.txlabz.lawnote.modelClasses;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 9/9/2016.
 */
public class TemplateModel implements Serializable {

    @SerializedName("template_id")
    String templateId;

    @SerializedName("template_title")
    String templateTitle;

    @SerializedName("template_description")
    String templateDescription;

    @SerializedName("is_created_by_user")
    String isCreatedByUser;

    @SerializedName("template_url")
    String templateURL;

    @SerializedName("template_url_view")
    private
    String templatePreviewUrl;

    @SerializedName("answers_count")
    String answerCount;




    @SerializedName("sender_signature_image")
    String senderSignatureImage;

    @SerializedName("sender_first_name")
    private
    String senderFirstName;

    @SerializedName("sender_last_name")
    private
    String senderLastName;

    @SerializedName("sender_email")
    private
    String senderEmail;

    @SerializedName("receivers")
    private
    ArrayList<ReceiversInfo> receivers;

    @SerializedName("date_time")
    String dateTime;

    @SerializedName("questions")
    public ArrayList<QuestionModel> questionModels;


    public boolean isCreatedByUser() {
        return !TextUtils.isEmpty(isCreatedByUser) && isCreatedByUser.equals("1");
    }

    public void setIsCreatedByUser(String isCreatedByUser) {
        this.isCreatedByUser = isCreatedByUser;
    }

    public String getSenderSignatureImage() {
        return senderSignatureImage;
    }

    public void setSenderSignatureImage(String senderSignatureImage) {
        this.senderSignatureImage = senderSignatureImage;
    }



    public ArrayList<QuestionModel> getQuestionModels() {
        return questionModels;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(String answerCount) {
        this.answerCount = answerCount;
    }

    public void setQuestionModels(ArrayList<QuestionModel> questionModels) {
        this.questionModels = questionModels;
    }

    public String getTemplateURL() {
        return templateURL;
    }

    public void setTemplateURL(String templateURL) {
        this.templateURL = templateURL;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }


    public String getTemplateDescription() {
        return templateDescription;
    }

    public void setTemplateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
    }

    public String getTemplateTitle() {
        return templateTitle;
    }

    public void setTemplateTitle(String templateTitle) {
        this.templateTitle = templateTitle;
    }

    public String getTemplatePreviewUrl() {
        return templatePreviewUrl;
    }

    public void setTemplatePreviewUrl(String templatePreviewUrl) {
        this.templatePreviewUrl = templatePreviewUrl;
    }

    public ArrayList<AnswersInfo> getAnswersList() {

        ArrayList<AnswersInfo> answersList=new ArrayList<>();

        for(int i=0;i<questionModels.size();i++)
        {
            AnswersInfo answersInfo=new AnswersInfo();
            answersInfo.setQuestinId(questionModels.get(i).getQuestionID());
            answersInfo.setAnswwer(questionModels.get(i).getAnswer());
            answersInfo.setQuestionType(questionModels.get(i).getQuestionType());

            answersList.add(answersInfo);
        }

        return answersList;
    }

    public ArrayList<ReceiversInfo> getReceivers() {
        return receivers;
    }

    public void setReceivers(ArrayList<ReceiversInfo> receivers) {
        this.receivers = receivers;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderLastName() {
        return senderLastName;
    }

    public void setSenderLastName(String senderLastName) {
        this.senderLastName = senderLastName;
    }

    public String getSenderFirstName() {
        return senderFirstName;
    }

    public void setSenderFirstName(String senderFirstName) {
        this.senderFirstName = senderFirstName;
    }
}
