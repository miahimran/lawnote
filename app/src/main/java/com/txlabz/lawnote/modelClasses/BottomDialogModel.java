package com.txlabz.lawnote.modelClasses;

/**
 * Created by Ali Sabir on 8/31/2017.
 */

public class BottomDialogModel {

    private String title;
    private int resource;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }
}
