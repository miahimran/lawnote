package com.txlabz.lawnote.modelClasses;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class TemplateFilledData {

    private String elementId;
    private String value;

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
