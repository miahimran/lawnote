package com.txlabz.lawnote.modelClasses.server_responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ali Sabir on 8/25/2017.
 */

public class GeneralResponse {

    @SerializedName("status")
   private int  status;
    @SerializedName("message")
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
