package com.txlabz.lawnote.modelClasses.server_responses;

import com.google.gson.annotations.SerializedName;
import com.txlabz.lawnote.modelClasses.TemplateModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/29/2017.
 */

public class ReceivedTemplates implements Serializable{

    @SerializedName("status")
    private Integer status;
    @SerializedName("received")
    private ArrayList<TemplateModel> received = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<TemplateModel> getReceived() {
        return received;
    }

    public void setReceived(ArrayList<TemplateModel> received) {
        this.received = received;
    }
}
