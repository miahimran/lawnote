package com.txlabz.lawnote.modelClasses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/25/2017.
 */

public class TemplateToSaveInfo {

    @SerializedName("user_id")
    private
    String userId;

    @SerializedName("template_id")
    private
    String templateId;

    @SerializedName("answers_array")
    private
    ArrayList<AnswersInfo> answersList;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public ArrayList<AnswersInfo> getAnswersList() {
        return answersList;
    }

    public void setAnswersList(ArrayList<AnswersInfo> answersList) {
        this.answersList = answersList;
    }
}
