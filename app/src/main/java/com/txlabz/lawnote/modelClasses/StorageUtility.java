package com.txlabz.lawnote.modelClasses;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 7/25/2016.
 */
public class StorageUtility {



    private static StorageUtility storageUtility =new StorageUtility();

     public StorageUtility(){}

    public static StorageUtility getInstance()
    {
        return storageUtility;
    }

    ArrayList<Category> arrayList=new ArrayList<>();

    public void setArrayList(ArrayList<Category> arrayList) {
        this.arrayList = arrayList;
    }

    public ArrayList<Category> getArrayList() {
        return arrayList;
    }
}
