package com.txlabz.lawnote.modelClasses;

/**
 * Created by Fatima Siddique on 7/15/2016.
 */
public class Catalog {
    int resource;
    String name;

    public Catalog(int resource, String name) {
        this.resource = resource;
        this.name = name;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
