package com.txlabz.lawnote.modelClasses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 9/9/2016.
 */
public class HomeDataModel implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("categories")
    public ArrayList<Categories> categories;
    @SerializedName("notifications_data")
    private NotificationDataModel notificationDataModel;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Categories> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Categories> categories) {
        this.categories = categories;
    }

    public NotificationDataModel getNotificationDataModel() {
        return notificationDataModel;
    }
}
