package com.txlabz.lawnote.modelClasses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fatima Siddique on 12/08/2017.
 */

public class NotificationModel {
    @SerializedName("notification_id")
    private String notificationId;
    @SerializedName("template_id")
    private String templateId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("flag")
    private String flag;
    @SerializedName("message")
    private String message;
    @SerializedName("seen")
    private String seen;
    @SerializedName("template_title")
    private String templateTitle;
    @SerializedName("template_description")
    private String templeteDescription;
    @SerializedName("category_icon")
    private String categoryIcon;
    @SerializedName("template_url_view")
    private String templateUrlView;

    public NotificationModel(String message) {
        this.message = message;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public String getUserId() {
        return userId;
    }

    public String getFlag() {
        return flag;
    }

    public String getMessage() {
        return message;
    }

    public String getSeen() {
        return seen;
    }

    public String getTemplateTitle() {
        return templateTitle;
    }

    public String getTempleteDescription() {
        return templeteDescription;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public String getTemplateUrlView() {
        return templateUrlView;
    }
}
