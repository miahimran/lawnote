package com.txlabz.lawnote.modelClasses.server_responses;

import com.google.gson.annotations.SerializedName;
import com.txlabz.lawnote.modelClasses.TemplateModel;

/**
 * Created by Ali Sabir on 9/7/2017.
 */

public class TemplateDetailResponse {

    @SerializedName("status")
    private
    int status;

    @SerializedName("message")
    private
    String message;

    @SerializedName("template")
    private
    TemplateModel template;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TemplateModel getTemplate() {
        return template;
    }

    public void setTemplate(TemplateModel template) {
        this.template = template;
    }
}
