package com.txlabz.lawnote.modelClasses;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 10/6/2016.
 */
public class QuestionModel implements Serializable{


    private static final String TAG=AppConstants.TAG;
    @SerializedName("question_id")
    String questionID;

    @SerializedName("template_id")
    String templateId;

    @SerializedName("question_text")
    String questionText;

    @SerializedName("question_type")
    String questionType;

    @SerializedName("tag_id")
    String tagId;

    @SerializedName("answer")
    String answer;

    @SerializedName("description")
    private
    String description;

    @SerializedName("position")
    String position;

    @SerializedName("options")
    public ArrayList<OptionsModel> optionsModel;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAnswer() {

        Log.d(TAG,"get  answer "+answer+" to object "+ Utils.getUniqueCode(answer));


        return answer;
    }

    public void setAnswer(String answer) {
        Log.d(TAG,"set  answer "+answer+" to object "+ Utils.getUniqueCode(answer));
        this.answer = answer;
    }

    public ArrayList<OptionsModel> getOptionsModel() {
        return optionsModel;
    }

    public void setOptionsModel(ArrayList<OptionsModel> optionsModel) {
        this.optionsModel = optionsModel;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public String toString()
    {
        Gson gson=new Gson();
        return gson.toJson(this);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
