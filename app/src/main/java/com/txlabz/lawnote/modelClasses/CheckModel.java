package com.txlabz.lawnote.modelClasses;

/**
 * Created by Fatima Siddique on 10/20/2016.
 */
public class CheckModel {

    int position;

    public CheckModel(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
