package com.txlabz.lawnote.modelClasses;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;
import com.koushikdutta.ion.builder.BitmapBuilder;
import com.txlabz.lawnote.utils.Base64Utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/25/2017.
 */

public class ContractInfo implements Serializable{

    @SerializedName("user_id")
    private String  userId;
    @SerializedName("sender_signature_image")
    private String userSignature;
    @SerializedName("template_id")
    private String templateId;
    @SerializedName("content")
    private String content;
    @SerializedName("template_title")
    private String templateTitle;
    @SerializedName("receivers")
    private ArrayList<ReceiversInfo> receivers;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserSignature() {
        return userSignature;
    }

    public void setUserSignatureBitmap(Bitmap bitmap)
    {
        setUserSignature(Base64Utils.convert(bitmap));
    }
    public void setUserSignature(String userSignature) {
        this.userSignature = userSignature;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public ArrayList<ReceiversInfo> getReceivers() {
        return receivers;
    }

    public void setReceivers(ArrayList<ReceiversInfo> receivers) {
        this.receivers = receivers;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTemplateTitle() {
        return templateTitle;
    }

    public void setTemplateTitle(String templateTitle) {
        this.templateTitle = templateTitle;
    }
}
