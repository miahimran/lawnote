package com.txlabz.lawnote.modelClasses;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 7/25/2016.
 */
public class Category extends ArrayList<String> {
    String title,description;
    public ArrayList<Template> templateArrayList;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setTemplateArrayList(ArrayList<Template> templateArrayList) {
        this.templateArrayList = templateArrayList;
    }

    public ArrayList<Template> getTemplateArrayList() {
        return templateArrayList;
    }
}
