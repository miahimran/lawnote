package com.txlabz.lawnote.modelClasses;

/**
 * Created by Fatima Siddique on 10/19/2016.
 */
public class TemplateAswerModel {

    int id;
    String userId,templateId,templateName,questionID,questionType,answer;

//    public TemplateAswerModel(int id, String userId, String templateId, String templateName, String questionID, String questionType, String answer) {
//        this.id = id;
//        this.userId = userId;
//        this.templateId = templateId;
//        this.templateName = templateName;
//        this.questionID = questionID;
//        this.questionType = questionType;
//        this.answer = answer;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
