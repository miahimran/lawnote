package com.txlabz.lawnote.modelClasses;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Fatima Siddique on 11/10/2016.
 */
public class PendingTemplateModel implements Serializable {

    @SerializedName("status")
    String status;

    @SerializedName("template")
    TemplateModel templateModel;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TemplateModel getTemplateModel() {
        return templateModel;
    }

    public void setTemplateModel(TemplateModel templateModel) {
        this.templateModel = templateModel;
    }
}
