package com.txlabz.lawnote.modelClasses;

/**
 * Created by Fatima Siddique on 7/15/2016.
 */
public class Template {
    private  String title;
    private  String description;

//    public Template(String title, String description) {
//
//        this.title=title;
//        this.description=description;
//    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
