package com.txlabz.lawnote.dialogUtil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.interfaces.OnResultListener;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ali Sabir on 8/23/2017.
 */


public class GetDescriptionDialog extends DialogFragment implements View.OnClickListener {
    private final OnResultListener onResultListener;

    /** The system calls this to get the DialogFragment's layout, regardless
     of whether it's being displayed as a dialog or an embedded fragment. */

    public GetDescriptionDialog(OnResultListener onResultListener)
    {
        this.onResultListener=onResultListener;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        ButterKnife.bind(getActivity());
        return inflater.inflate(R.layout.get_description_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        initViews();

    }

    /** The system calls this only when creating the layout in a dialog. */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    TextView mQuestion;
    EditText mDescriptionDetails;
    ImageView mCancel;
    TextView mDone;
    private int position;

    private void initViews() {
        Bundle bundle = getArguments();


        mCancel= (ImageView) getView().findViewById(R.id.icon_cancel);
        mDone= (TextView) getView().findViewById(R.id.tv_done);
        mQuestion= (TextView) getView().findViewById(R.id.tv_question);
        mDescriptionDetails= (EditText) getView().findViewById(R.id.et_description_details);

        if (bundle != null) {
            String text = bundle.getString(AppConstants.QUESTION_TEXT, "");
            String desc = bundle.getString(AppConstants.QUESTION_DESC, "");
            position    = bundle.getInt(AppConstants.POSITION);
            mQuestion.setText(text);
            mDescriptionDetails.setText(desc);

            Utils.showSoftKeyboard(getActivity(),mDescriptionDetails);
        }



        mCancel.setOnClickListener(this);
        mDone.setOnClickListener(this);
    }


    private void setResultAndNavigate() {
        Intent data = new Intent();
        data.putExtra(AppConstants.QUESTION_DESC, mDescriptionDetails.getText().toString());
        data.putExtra(AppConstants.POSITION, position);

        onResultListener.onDone(mDescriptionDetails.getText().toString());
        dismiss();

    }

    @Override
    public void onClick(View view) {

        try {
            Utils.hideKeyboard(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (view.getId()) {
            case R.id.icon_cancel:
             dismiss();
                break;
            case R.id.tv_done:
                setResultAndNavigate();
                break;
        }

    }


}
