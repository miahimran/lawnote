package com.txlabz.lawnote;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationController extends Application {


	@Override
	public void onCreate()
	{
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		Branch.getAutoInstance(this);
		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
						.setDefaultFontPath("fonts/Proxima-Regular.ttf")
						.setFontAttrId(R.attr.fontPath)
						.build()
		);

	}


	@Override
	public void onTerminate()
	{
		super.onTerminate();
	}


}
