package com.txlabz.lawnote.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.CreateYourOwnActivity;
import com.txlabz.lawnote.adapters.SignatureRecyclerviewAdapter;
import com.txlabz.lawnote.handler.TemplateDetailHandler;
import com.txlabz.lawnote.interfaces.SaveCompleteListener;
import com.txlabz.lawnote.interfaces.TemplateDetailListener;
import com.txlabz.lawnote.modelClasses.ContractInfo;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.modelClasses.ReceiversInfo;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.StorageUtility;
import com.txlabz.lawnote.utils.Utils;
import com.txlabz.lawnote.utils.WebViewUtils;

import java.util.ArrayList;


public class HtmlTemplateFragment extends BaseTemplateQuestionsFragment implements View.OnClickListener {


    private WebView webView;

    private TextView tvUserName, tvUserEmail;
    private ImageView ivUserSignature;

    private LinearLayout lytContainer;


    private RecyclerView receiversRecyclerView;
    private TextView tvReceiversInfo;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public static HtmlTemplateFragment newInstance(int htmlPageTypePreview) {
        HtmlTemplateFragment fragment = new HtmlTemplateFragment();
        fragment.htmlPageType = htmlPageTypePreview;
        return fragment;
    }

    public static HtmlTemplateFragment newInstance(int htmlPageTypePreview,TemplateModel templateToShow) {
        HtmlTemplateFragment fragment = new HtmlTemplateFragment();
        fragment.htmlPageType = htmlPageTypePreview;
        fragment.selectedTemplate=templateToShow;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.preview_template_fragment, container, false);

        if(selectedTemplate==null){
            selectedTemplate = ((CreateEditTemplateActivity) context).getSelectedTemplate();}


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.showProgressDialog(context);

        if (selectedTemplate == null) {
            Log.d(AppConstants.TAG, "Selected template is null returning...");
            return;
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        try {
            ((CreateEditTemplateActivity) context).setPreviewColor();
        } catch (Exception e) {
            e.printStackTrace();
        }

        initViews(view);

        ////////////////////////Page type / oriented settings...//////////////////////////
        if (htmlPageType == HTML_PAGE_TYPE_COMPLETED) {

            submitBtn.setVisibility(View.GONE);
        } else if (htmlPageType == HTML_PAGE_TYPE_RECEIVED) {

            if (TextUtils.isEmpty(selectedTemplate.getSenderSignatureImage())) {

                submitBtn.setVisibility(View.GONE);
            } else {

                tvReceiversInfo.setText(getString(R.string.tap_to_sign));
                tvReceiversInfo.setOnClickListener(this);
                submitBtn.setText(R.string.send_aggrement);
                submitBtn.setVisibility(View.VISIBLE);
            }
        } else {
            submitBtn.setVisibility(View.VISIBLE);
        }


        if(htmlPageType==HTML_PAGE_TYPE_RECEIVED||htmlPageType==HTML_PAGE_TYPE_COMPLETED){

            String  userId=StorageUtility.getDataFromPreferences(context,AppConstants.PREF_KEY_USER_ID,"0");

            TemplateDetailHandler.getTemplateDetail(context, userId, selectedTemplate.getTemplateId(), new TemplateDetailListener() {
                @Override
                public void onTemplateDetailReceived(TemplateModel templateModel) {

                    selectedTemplate=templateModel;

                    initWebView();
                }
            });
        }
        else initWebView();

        ///////////////////////End page oriendted settings...////////////////////////////////

        populdateData();

    }

    private void initWebView() {

        View view =getView();
        if(view==null) return;


        String templateUrlToLoad;
        if (htmlPageType == HTML_PAGE_TYPE_PREVIEW || htmlPageType == HTML_PAGE_TYPE_COMPLETED || htmlPageType == HTML_PAGE_TYPE_RECEIVED) {
            templateUrlToLoad = selectedTemplate.getTemplatePreviewUrl();
        } else {
            templateUrlToLoad = selectedTemplate.getTemplateURL();

        }


        webView = (WebView) view.findViewById(R.id.mainLayout);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);


                WebViewUtils.populateUserFilledData(webView, selectedTemplate);

                showPageContent();

            }
        });

        if (TextUtils.isEmpty(templateUrlToLoad)) {
            Utils.hideProgressDialog();
            Utils.showErrorMessage(context, getString(R.string.template_url_is_null));
        }
        Log.d(AppConstants.TAG, "Url to load -> preview template url: " + templateUrlToLoad);


        if (Utils.IsNetworkConnected(getContext())) {
            webView.loadUrl(templateUrlToLoad);
        } else {

            Utils.showErrorMessage(context, AppConstants.CHECK_CONNECTION);

        }
    }

    private void populdateData() {

        if (!TextUtils.isEmpty(selectedTemplate.getSenderSignatureImage())) {

            ivUserSignature.setVisibility(View.VISIBLE);
            Utils.loadImage(context, ivUserSignature, selectedTemplate.getSenderSignatureImage());
        } else {

            ivUserSignature.setImageResource(R.drawable.ic_no_sign);
            // ivUserSignature.setVisibility(View.GONE);
        }

        if (htmlPageType==HTML_PAGE_TYPE_RECEIVED) {

            tvUserEmail.setText(selectedTemplate.getSenderEmail());
            tvUserName.setText(selectedTemplate.getSenderFirstName() + " " + selectedTemplate.getSenderLastName());
        } else {
            tvUserName.setText(userName);
            tvUserEmail.setText(userEmail);
        }

        submitBtn.setOnClickListener(this);

        showReceiversInfo();

    }

    private void initViews(View view) {



        try {
            userName = ((CreateEditTemplateActivity) context).getUserName();
            userEmail = ((CreateEditTemplateActivity) context).getEmail();
        } catch (Exception e) {
            e.printStackTrace();
            userName=StorageUtility.getDataFromPreferences(context,AppConstants.PREF_KEY_FIRST_NAME,"0")+" "+
                    StorageUtility.getDataFromPreferences(context,AppConstants.PREF_KEY_LAST_NAME,"0");
            userEmail=StorageUtility.getDataFromPreferences(context,AppConstants.PREF_KEY_EMAIL,"0");

        }


        tvUserName = (TextView) view.findViewById(R.id.freelancerName);
        tvUserEmail = (TextView) view.findViewById(R.id.freelancerEmail);
        ivUserSignature = (ImageView) view.findViewById(R.id.ivAutSignature);
        tvReceiversInfo = (TextView) view.findViewById(R.id.tvReceiversInfo);
        receiversRecyclerView = (RecyclerView) view.findViewById(R.id.receiversRecyclerView);
        lytContainer = (LinearLayout) view.findViewById(R.id.lytContainer);
        submitBtn = (Button) view.findViewById(R.id.submit);

    if(tvUserName!=null)        tvUserName.setText(userName);
    if(tvUserEmail!=null)    tvUserEmail.setText(userEmail);

    }

    private void showReceiversInfo() {

        String userEmail = StorageUtility.getDataFromPreferences(context, AppConstants.PREF_KEY_EMAIL, "");

        if (!TextUtils.isEmpty(userEmail)) {

            sortForUserSignatureOnTop(userEmail);
        }


        receiversRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        if (selectedTemplate.getReceivers() != null && selectedTemplate.getReceivers().size() > 0)
            receiversRecyclerView.setAdapter(new SignatureRecyclerviewAdapter(context, selectedTemplate.getReceivers()));
    }

    private void sortForUserSignatureOnTop(String userEmail) {

        ArrayList<ReceiversInfo> oldReceivers = selectedTemplate.getReceivers();

        if (oldReceivers != null && oldReceivers.size() > 0) {
            int foundIndex = -1;
            for (int i = 0; i < oldReceivers.size(); i++) {
                if (oldReceivers.get(i).getEmail().equals(userEmail)) {
                    foundIndex = i;
                    break;
                }
            }

            if (foundIndex >= 0) {
                ReceiversInfo tempItem = oldReceivers.get(foundIndex);

                if (TextUtils.isEmpty(tempItem.getSignatureImage())) {
                   // hideSubmitButton();
                } else {
                    showSubmitButton();
                }
                oldReceivers.remove(foundIndex);
                oldReceivers.add(0, tempItem);
            }


        }

    }

    private void showSubmitButton() {

        if (htmlPageType != HTML_PAGE_TYPE_COMPLETED)
            submitBtn.setVisibility(View.VISIBLE);
    }



    private void showPageContent() {

        Utils.hideProgressDialog();
        lytContainer.setVisibility(View.VISIBLE);

    }

    public void saveUserInput(final SaveCompleteListener saveCompleteListener) {


        Log.d(AppConstants.TAG, "SAving user data ");
        if (htmlPageType == HtmlTemplateFragment.HTML_PAGE_TYPE_PREVIEW) {
            Log.d(AppConstants.TAG, "Current page type is read only not saving input  returning....");
            saveCompleteListener.onDataSavedCompleted(selectedTemplate);
            return;
        }

        readInput(0, saveCompleteListener);
    }

    @Override
    public void refreshReceivers() {

        showReceiversInfo();

    }


    private void readInput(final int index, final SaveCompleteListener saveCompleteListener) {

        if(selectedTemplate==null) {
            Log.d(AppConstants.TAG,"Trying to read input but selected template is null");
            return;
        }
        if (index < selectedTemplate.getQuestionModels().size()) {

            final QuestionModel question;
            question = selectedTemplate.getQuestionModels().get(index);
            final ArrayList<String> urls = WebViewUtils.getFetchValueUrlForHtmlElements(question);

            if(urls==null) return;
            for (int j = 0; j < urls.size(); j++) {

                Log.d(AppConstants.TAG, "Exceuting ...." + urls.get(j));


                final int finalJ = j;
                webView.evaluateJavascript(urls.get(j),
                        new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String answer) {

                                answer = Utils.cleanUpAnswer(answer);

                                Log.d(AppConstants.TAG, "Found value for " + question.getQuestionText() + "  :  " + answer + " setting at index " + index);
                                question.setAnswer(answer);
                                if (finalJ == urls.size() - 1) {
                                    readInput(index + 1, saveCompleteListener);
                                }

                            }
                        });
            }


        } else if (saveCompleteListener != null) {
            saveCompleteListener.onDataSavedCompleted(selectedTemplate);
        }


    }

    @Override
    public void repopulateData(TemplateModel selectedTemplate) {

        Log.d(AppConstants.TAG, "Populating user data Object code ");

        if (this.selectedTemplate != null && webView != null) {
            WebViewUtils.populateUserFilledData(webView, this.selectedTemplate);
            Log.d(AppConstants.TAG, "Populating user data completed...");
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tvReceiversInfo: {
                showSignatureDialog(REQUEST_CODE_SIGNATURE_FOR_RECEIVED_TEMPLATE);
            }
            break;
            case R.id.submit: {

                saveUserInput(new SaveCompleteListener() {
                    @Override
                    public void onDataSavedCompleted(TemplateModel selectedTemplate) {

                        submitTemplate();
                    }
                });
            }
            break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == REQUEST_CODE_SIGNATURE_FOR_RECEIVED_TEMPLATE&&resultCode==Activity.RESULT_OK) {
            String signaturePath = data.getStringExtra(AppConstants.KEY_SIGNATURE);

            setUserSignature(signaturePath);
            showReceiversInfo();
        } if(requestCode== REQUEST_CODE_SIGNATURE_FOR_SEND_CONTRACT &&resultCode== Activity.RESULT_OK) {
            String signaturePath=  data.getStringExtra(AppConstants.KEY_SIGNATURE);
            Bitmap bitmap = Utils.getBitmapFromPath(signaturePath);
            if (!TextUtils.isEmpty(signaturePath)) {
                ivUserSignature.setVisibility(View.VISIBLE);
                ivUserSignature.setImageBitmap(bitmap);
            }
            inputReceiversInfo(selectedTemplate, bitmap);
        } else if (requestCode == AppConstants.RC_ADD_RECEIVERS) {
            ArrayList<ReceiversInfo> receiversInfo = (ArrayList<ReceiversInfo>) data.getSerializableExtra(AppConstants.INTENT_KEY_RECEIVERS_LIST);
            ContractInfo contractInfo = (ContractInfo) data.getSerializableExtra(AppConstants.INTENT_KEY_CONTRACT_INFO);
            if (receiversInfo != null && receiversInfo.size() > 0) {
                receiversRecyclerView.setVisibility(View.VISIBLE);
                receiversRecyclerView.setAdapter(new SignatureRecyclerviewAdapter(context, contractInfo.getReceivers()));
                boolean shouldSendContract = data.getBooleanExtra(AppConstants.INTENT_KEY_SHOULD_SEND_CALL, true);
                if (shouldSendContract)
                    sendContract(contractInfo);
            }
        }
        else
            super.onActivityResult(requestCode, resultCode, data);
    }

    private void setUserSignature(String signaturePath) {
        if (selectedTemplate != null) {
            ArrayList<ReceiversInfo> receivers = selectedTemplate.getReceivers();

            if (receivers != null) {
                for (int i = 0; i < receivers.size(); i++) {
                    if (receivers.get(i).getEmail().equals(userEmail)) {
                        receivers.get(i).setSignatureBitmap(Utils.getBitmapFromPath(signaturePath));
                        receivers.get(i).setSignatureImage(signaturePath);
                        break;
                    }
                }
            }
        }
    }

}
