package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.interfaces.SaveCompleteListener;
import com.txlabz.lawnote.modelClasses.TemplateModel;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CreateTemplateIntroFragment extends BaseTemplateQuestionsFragment implements View.OnClickListener{

    Context context;
    @BindView(R.id.continue_container)
    View mContinueContainer;
    @BindView(R.id.tv_template_title)
    TextView mTemplateTitle;
    @BindView(R.id.tv_template_desc)
    TextView mTemplateDescription;
    @BindView(R.id.icon_category)
    ImageView iconCategory;
    private String categoryImage;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public static CreateTemplateIntroFragment newInstance(String categoryImage)
    {
        CreateTemplateIntroFragment fragment = new CreateTemplateIntroFragment();
        fragment.categoryImage = categoryImage;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_template_intro, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        initViews();
    }

    private void initViews() {
        Picasso.with(context).load(categoryImage).placeholder(R.drawable.ic_place_holder_category).into(iconCategory);
        TemplateModel selectedTemplate = ((CreateEditTemplateActivity) context).getSelectedTemplate();
        mTemplateTitle.setText(selectedTemplate.getTemplateTitle());
        mTemplateDescription.setText(selectedTemplate.getTemplateDescription());
        mContinueContainer.setOnClickListener(this);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    @Override
    public void onClick(View view) {
            switch (view.getId()) {
                case R.id.continue_container:
                    ((CreateEditTemplateActivity)context).continueToCreateTemplate();
                    break;
            }
    }

    @Override
    public void repopulateData(TemplateModel selectedTemplate) {

    }

    @Override
    public void saveUserInput(SaveCompleteListener onDataSaveCompleted) {

        onDataSaveCompleted.onDataSavedCompleted(selectedTemplate);
    }

    @Override
    public void refreshReceivers() {

    }


}
