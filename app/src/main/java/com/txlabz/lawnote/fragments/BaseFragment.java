package com.txlabz.lawnote.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.txlabz.lawnote.activities.SignatureInputActivity;
import com.txlabz.lawnote.utils.AppConstants;

/**
 * Created by Ali Sabir on 8/22/2017.
 */

public class BaseFragment extends Fragment {

    protected Context context;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        Log.d(AppConstants.TAG,getClass().getSimpleName()+" is attached.");

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getContext();
    }

    protected void showSignatureDialog(int requestCode) {

        Intent intent=new Intent(context, SignatureInputActivity.class);
        startActivityForResult(intent, requestCode);

    }

}
