package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.txlabz.lawnote.activities.ProfileQuestionActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.OptionsModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;

import java.util.ArrayList;


public class RadioQuestionFragment extends BaseQuestionFragment {

    Context context;
    TextView counter;
    int currentPosition;
    Boolean filledAmountUpdated=false;
    String question;
    String questionTag;
    ArrayList<OptionsModel> optionsModelArrayList=new ArrayList<>();
    TextView firstEditText;
    String questionID;
    RadioGroup radioGroup;
    int selectedRadio=0;
    int checked=0;
    String optionAnswer;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.radio_question_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        counter=(TextView)view.findViewById(R.id.counter);
        final int totalSize=((ProfileQuestionActivity)getContext()).getQuestionArrayListSize();
        final CreateTemplateFragment parentFragment=(CreateTemplateFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        firstEditText=(TextView) view.findViewById(R.id.firstEditText);
        radioGroup=(RadioGroup)view.findViewById(R.id.radioGroup);

        firstEditText.setText(question);

        getQuestionID=questionID;

        for (int i=0; i<optionsModelArrayList.size(); i++)
        {
            RadioButton radioButton=new RadioButton(context);
            LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,20,0,0);
            radioButton.setLayoutParams(layoutParams);
            Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/AvenirNextLTPro-Regular.otf");
            radioButton.setTypeface(face);
            radioButton.setText(optionsModelArrayList.get(i).getOptionText());
            radioGroup.addView(radioButton);

        }
        if (checked==1)
        {
            for (int i=0; i<optionsModelArrayList.size();i++)
            {
                RadioButton r=(RadioButton) radioGroup.getChildAt(selectedRadio);
                r.setChecked(true);
            }
        }

        if (optionAnswer.length()>0)
        {
            for (int i=0; i<optionsModelArrayList.size(); i++)
            {
                if (optionAnswer.equals(optionsModelArrayList.get(i).getOptionID()))
                {
                    RadioButton r=(RadioButton) radioGroup.getChildAt(i);
                    ((ProfileQuestionActivity)context).setRadioItem(optionAnswer);
                    r.setChecked(true);
                }
            }
        }


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i=0; i<optionsModelArrayList.size(); i++)
                {
//
                    RadioButton r=(RadioButton) radioGroup.getChildAt(i);
                    if (r.isChecked())
                    {
                        answer=optionsModelArrayList.get(i).getOptionID();
                        getQuestionTag=questionTag;
                        String radioItem=optionsModelArrayList.get(i).getOptionValue();
                        ((ProfileQuestionActivity)context).setRadioItem(radioItem);
//                        optionsModelArrayList.get(i).getOptionID();
                        checked=1;
                        selectedRadio=i;
                    }
                }
            }
        });

        counter.setText(currentPosition+"/"+totalSize);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    public static BaseQuestionFragment getInstance(int i, QuestionModel questionModel, ArrayList<OptionsModel> optionsModel) {
        RadioQuestionFragment object=new RadioQuestionFragment();
        object.currentPosition=i+1;
        object.optionAnswer=questionModel.getAnswer();
        object.questionID=questionModel.getQuestionID();
        object.question=questionModel.getQuestionText();
        object.questionTag=questionModel.getTagId();
        object.optionsModelArrayList=optionsModel;
        return object;
    }
}
