package com.txlabz.lawnote.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.txlabz.lawnote.activities.MainActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.adapters.TemplatesListAdapter;
import com.txlabz.lawnote.modelClasses.TemplateModel;

import java.util.ArrayList;


public class TemplatesListFragment extends BaseFragment implements View.OnClickListener {


    public static final   int  TEMPLATES_TYPE_DRAFT=1;
    public static  final int  TEMPLATES_TYPE_PENDING=2;
    public static final int  TEMPLATES_TYPE_COMPLETED=3;
    public static final int  TEMPLATES_TYPE_RECEIVED=4;

    private int templatesType;


    private ArrayList<TemplateModel> templatesList;
    private  RecyclerView recyclerView;
    private RelativeLayout rel_main;
    private TextView tvMessage;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.templates_list_fragment, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        rel_main=(RelativeLayout)view.findViewById(R.id.rel_main);
        tvMessage= (TextView) view.findViewById(R.id.tvMessage);
        view.findViewById(R.id.submit).setOnClickListener(this);


        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        TemplatesListAdapter templatesListAdapter = new TemplatesListAdapter(context, templatesList,templatesType);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(templatesListAdapter);


        if(templatesList ==null|| templatesList.size()==0)
        {
            rel_main.setVisibility(View.VISIBLE);

            switch (templatesType)
            {
                case TEMPLATES_TYPE_DRAFT:tvMessage.setText(getString(R.string.msg_no_draft_template));break;
                case TEMPLATES_TYPE_PENDING:tvMessage.setText(getString(R.string.msg_no_pending_template));break;
                case TEMPLATES_TYPE_COMPLETED:tvMessage.setText(getString(R.string.msg_no_completed_templates));break;
            }

            rel_main.setVisibility(View.VISIBLE);

        }
    }



    public static Fragment newInstence(ArrayList<TemplateModel> templatesList, int templatesType) {
        TemplatesListFragment fragment = new TemplatesListFragment();
        fragment.templatesList = templatesList;
        fragment.templatesType=templatesType;
        return fragment;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.submit: createNewTemplate();break;
        }
    }

    private void createNewTemplate() {

        Intent intent=new Intent(getContext(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();

    }
}
