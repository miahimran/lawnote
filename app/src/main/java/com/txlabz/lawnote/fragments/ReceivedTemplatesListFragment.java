package com.txlabz.lawnote.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.MainActivity;
import com.txlabz.lawnote.handler.ReceivedTemplatesHandler;
import com.txlabz.lawnote.interfaces.ReceivedTemplatesResponseListener;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.modelClasses.server_responses.ReceivedTemplates;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;


public class ReceivedTemplatesListFragment extends BaseFragment {


    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeLayout;
    private TextView noData;

    public ReceivedTemplatesListFragment() {
    }


    public static ReceivedTemplatesListFragment newInstance() {
        return new ReceivedTemplatesListFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)context).setTitle(AppConstants.RECEIVED);
        ((MainActivity)context).changeAddIcon(0);
        loadData();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void loadData() {

        Utils.showProgressDialog(context);

        String  userId= com.txlabz.lawnote.utils.StorageUtility.getDataFromPreferences(context, AppConstants.PREF_KEY_USER_ID,"0");
        ReceivedTemplatesHandler.getReceivedTemplates(context, userId+"", new ReceivedTemplatesResponseListener() {
            @Override
            public void onServerResponse(ReceivedTemplates receivedTemplates) {

                swipeLayout.setRefreshing(false);
                Utils.hideProgressDialog();
                if (receivedTemplates == null) {
                    noData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    return;
                }
                ArrayList<TemplateModel> templatesArrayList = receivedTemplates.getReceived();
                if(templatesArrayList != null && templatesArrayList.size() > 0) {
                    noData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView.setAdapter(new MyReceivedTemplatesRecyclerViewAdapter(context,receivedTemplates.getReceived()));
                }
                else {
                    noData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receivedtemplates_list, container, false);

        noData = (TextView) view.findViewById(R.id.no_data);
        swipeLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipeLayout);
        swipeLayout.setColorSchemeResources(R.color.mainPrimaryLight, R.color.colorAccentMain, R.color.mainPrimaryDark);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            swipeLayout.setProgressViewOffset(false, 0, Utils.getToolbarHeight(context));
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));


        return view;
    }
}
