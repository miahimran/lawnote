package com.txlabz.lawnote.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.interfaces.SaveCompleteListener;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;


public class TemplateEditPromptFragment extends BaseTemplateQuestionsFragment implements View.OnClickListener {


    public TemplateEditPromptFragment() {
        // Required empty public constructor
    }


    public static TemplateEditPromptFragment newInstance() {
        TemplateEditPromptFragment fragment = new TemplateEditPromptFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_template_edit_prompt, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().findViewById(R.id.btnYes).setOnClickListener(this);
        getView().findViewById(R.id.btnCancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnYes: goForEditTemplate();break;
            case R.id.btnCancel: cancel();break;
        }
    }

    private void cancel() {

        CreateEditTemplateActivity templateDetailActivity= (CreateEditTemplateActivity) getActivity();
        templateDetailActivity.loadFragment(CreateEditTemplateActivity.TAB_INDEX_CREATE_TEMPLATE);

    }

    private void goForEditTemplate() {

        selectedTemplate=((CreateEditTemplateActivity)context).getSelectedTemplate();

        Intent intent=new Intent(context,CreateEditTemplateActivity.class);
        intent.putExtra(AppConstants.SELECTED_TEMPLATE,selectedTemplate);
        intent.putExtra(CreateEditTemplateActivity.INIT_TABS_FOR,CreateEditTemplateActivity.INIT_TABS_FOR_EDIT_TEMPLATE);
        startActivity(intent);

        getActivity().finish();
    }

    @Override
    public void repopulateData(TemplateModel selectedTemplate) {

    }

    @Override
    public void saveUserInput(SaveCompleteListener onDataSaveCompleted) {

        onDataSaveCompleted.onDataSavedCompleted(selectedTemplate);
    }

    @Override
    public void refreshReceivers() {


    }

}
