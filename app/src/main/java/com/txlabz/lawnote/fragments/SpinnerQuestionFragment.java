package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.txlabz.lawnote.activities.ProfileQuestionActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.OptionsModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;


public class SpinnerQuestionFragment extends BaseQuestionFragment {

    Context context;
    Spinner spinner;
    int currentPosition;
    TextView counter,firstEditText;
    String question;
    String questionID;
    String questionTag;
    ArrayList<OptionsModel> optionsModelArrayList=new ArrayList<>();
    int selectedPosition=0;
    String optionAnswer;
    int getSelection=0;
    int firstTimeEntry=0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.spinner_question_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner=(Spinner)view.findViewById(R.id.spinner);
        final int totalSize=((ProfileQuestionActivity)getContext()).getQuestionArrayListSize();
        counter=(TextView)view.findViewById(R.id.counter);
        firstEditText=(TextView)view.findViewById(R.id.firstEditText);

        firstEditText.setText(question);

        counter.setText(currentPosition+"/"+totalSize);


        List<String> categories = new ArrayList<String>();
        categories.add("Default");

        for (int i=0; i<optionsModelArrayList.size(); i++)
        {
            categories.add(optionsModelArrayList.get(i).getOptionText());
        }


        ArrayAdapter<String> dataAdapter=new ArrayAdapter<String>(context,android.R.layout.simple_spinner_dropdown_item,categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);

        spinner.setSelection(selectedPosition);

        getQuestionID=questionID;

        if (optionAnswer.length()>0)
        {
            for (int i=0; i<optionsModelArrayList.size(); i++) {
            if (optionAnswer.equals(optionsModelArrayList.get(i).getOptionID()))
            {
                spinner.setSelection(i+1);
                ((ProfileQuestionActivity)context).setSpinnerItem(optionsModelArrayList.get(i).getOptionValue());
                getSelection=1;
            }
            }
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String selectedItem=spinner.getSelectedItem().toString();

                if (firstTimeEntry==0)
                {
                    firstTimeEntry++;
                }else {
                    firstTimeEntry=1;
                    if (selectedItem.equals("Default"))
                    {
                        answer="";
                    }else {

                        for (int i=0; i<optionsModelArrayList.size(); i++)
                        {
                            if (selectedItem.equals(optionsModelArrayList.get(i).getOptionText()))
                            {
                                String value=optionsModelArrayList.get(position-1).getOptionValue();
                                ((ProfileQuestionActivity)context).setSpinnerItem(value);
                                answer=optionsModelArrayList.get(position-1).getOptionID();
                                getQuestionTag=questionTag;
                                selectedPosition=position;
                                getSelection=0;
                            }
                        }
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    public static BaseQuestionFragment getInstance(int i, QuestionModel questionModel, ArrayList<OptionsModel> optionsModel) {
        SpinnerQuestionFragment object=new SpinnerQuestionFragment();
        object.currentPosition=i+1;
        object.questionID=questionModel.getQuestionID();
        object.optionAnswer=questionModel.getAnswer();
        object.question=questionModel.getQuestionText();
        object.questionTag=questionModel.getTagId();
        object.optionsModelArrayList=optionsModel;
        return object;
    }
}
