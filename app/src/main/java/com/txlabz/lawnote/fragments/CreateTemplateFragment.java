package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.natasa.progressviews.CircleProgressBar;
import com.txlabz.lawnote.activities.ProfileQuestionActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.CheckModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CreateTemplateFragment extends BaseFragment{

    Context context;
    ViewPager viewPager;
    ArrayList<QuestionModel> questionModelArrayList;
    CircleProgressBar circleProgressBar;
    int filledAmount=0;
    ViewPagerAdapter adapter;
    ArrayList<CheckModel> myList=new ArrayList<>();
    TextView firstTextView;
    int toastCounter=0;
    JSONArray jsonArray;
    String getAnswerCOunt;
    int firstTime=0;
    int jsonFirstTime=0;




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.create_template_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        circleProgressBar=(CircleProgressBar)view.findViewById(R.id.circle_progress);
        ((ProfileQuestionActivity)context).setCreateColor();
        questionModelArrayList=new ArrayList<>();
        questionModelArrayList=((ProfileQuestionActivity)context).getArrayList();
        viewPager=(ViewPager)view.findViewById(R.id.viewPager);
        firstTextView=(TextView)view.findViewById(R.id.firstTextView);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin(25);
        setupViewPager(viewPager);




        if (jsonFirstTime==0)
        {
            jsonArray=new JSONArray();
            ((ProfileQuestionActivity)context).setNewJsonArray(jsonArray);
            jsonFirstTime++;
        }


        firstTextView.setText(((ProfileQuestionActivity)context).getTemplateDescription());

        String templateName=((ProfileQuestionActivity)context).getTemplateName();

        circleProgressBar.setTextSize(70);

        getAnswerCOunt=((ProfileQuestionActivity)context).getAnswerCount();

       // filledAmount=Integer.parseInt(getAnswerCOunt);

        if (((ProfileQuestionActivity)getContext()).isFromManage())
        {
            if (firstTime==0)
            {
                filledAmount=Integer.parseInt(getAnswerCOunt);
                firstTime++;
            }
        }

        int progress= Utils.getProgress(getFilledAmount(),questionModelArrayList.size());
        setDonutProgress(progress);

        viewPager.setOffscreenPageLimit(questionModelArrayList.size());

        for (int i=0; i<questionModelArrayList.size(); i++)
        {
            if (questionModelArrayList.get(i).getPosition().equals("0"))
            {
                myList.add(new CheckModel(0));
            }else {
                myList.add(new CheckModel(1));
            }

        }

        for (int i=0; i<questionModelArrayList.size(); i++)
        {
            if (questionModelArrayList.size()>0)
            {
                if (!questionModelArrayList.get(i).getPosition().equals("0"))
                {
                    try {
                        JSONObject jObjectData = new JSONObject();
                        jObjectData.put("question_id",questionModelArrayList.get(i).getQuestionID());
                        jObjectData.put("answer",questionModelArrayList.get(i).getAnswer());
                        jObjectData.put("position",questionModelArrayList.get(i).getPosition());
                        jsonArray.put(jObjectData);
                        ((ProfileQuestionActivity)context).setNewJsonArray(jsonArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

      //  Toast.makeText(getContext(),String.valueOf(jsonArray.length()),Toast.LENGTH_LONG).show();

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) getView().findViewById(R.id.layout_main));
        if (toastCounter==0)
        {
            Toast toast = new Toast(getContext());
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
            toastCounter++;
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int oldPos = 0;
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position>oldPos)
                {
                                    if (position>0 && position<adapter.getCount())
                {
                    BaseQuestionFragment fragment=adapter.getItem(position-1);

                    if (fragment.getAnswer()!=null)
                    {
                        if (fragment.getAnswer().equals(""))
                        {
                            for (int i=0; i<myList.size(); i++)
                            {
                                if (myList.get(position-1).getPosition()==1)
                                {
                                    myList.get(position-1).setPosition(0);
                                    deacreaseFilledAmount();

                                    for (int x=0; x<jsonArray.length(); x++)
                                    {
                                        try {
                                            JSONObject jsonObject=jsonArray.getJSONObject(x);
                                            if (jsonObject.getString("question_id").equals(fragment.getQuestionID()))
                                            {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                                    jsonArray.remove(x);
                                                    ((ProfileQuestionActivity)context).setNewJsonArray(jsonArray);
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    int filedAmount=getFilledAmount();
                                    int progress= Utils.getProgress(filedAmount,questionModelArrayList.size());
                                    setDonutProgress(progress);
                                }
                            }
                        }else {

                            JSONObject jObjectData = new JSONObject();
                            for (int i=0; i<myList.size(); i++)
                            {
                                if (myList.get(position-1).getPosition()==0)
                                {
                                    myList.get(position-1).setPosition(1);
                                    increaseFilledAmount();
                                    int filedAmount=getFilledAmount();
                                    try {
                                        jObjectData.put("question_id",fragment.getQuestionID());
                                        jObjectData.put("answer",fragment.getAnswer());
                                        jObjectData.put("position",position);
                                        jsonArray.put(jObjectData);
                                        ((ProfileQuestionActivity)context).setNewJsonArray(jsonArray);

                                    } catch (JSONException e) {

                                    }
                                    int progress= Utils.getProgress(filedAmount,questionModelArrayList.size());
                                    setDonutProgress(progress);
                                }
                            }
                        }

                    }

                }else if (position==0)
                {

                }else if (position==adapter.getCount()-1)
                {

                }
                    oldPos=position;
                }


                else if (position<oldPos){
                                    if (position>0 && position<adapter.getCount())
                {
                    BaseQuestionFragment fragment=adapter.getItem(position+1);

                    if (fragment.getAnswer()!=null)
                    {
                        if (fragment.getAnswer().equals(""))
                        {
                            for (int i=0; i<myList.size(); i++)
                            {
                                if (myList.get(position+1).getPosition()==1)
                                {
                                    myList.get(position+1).setPosition(0);
                                    deacreaseFilledAmount();

                                    for (int x=0; x<jsonArray.length(); x++)
                                    {
                                        try {
                                            JSONObject jsonObject=jsonArray.getJSONObject(x);
                                            if (jsonObject.getString("question_id").equals(fragment.getQuestionID()))
                                            {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                                    jsonArray.remove(x);
                                                    ((ProfileQuestionActivity)context).setNewJsonArray(jsonArray);
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }


                                    int filedAmount=getFilledAmount();
                                    int progress= Utils.getProgress(filedAmount,questionModelArrayList.size());
                                    setDonutProgress(progress);
                                }
                            }
                        }else {
                            JSONObject jObjectData = new JSONObject();
                            for (int i=0; i<myList.size(); i++)
                            {
                                if (myList.get(position+1).getPosition()==0)
                                {
                                    myList.get(position+1).setPosition(1);
                                    increaseFilledAmount();
                                    try {
                                        jObjectData.put("question_id",fragment.getQuestionID());
                                        jObjectData.put("answer",fragment.getAnswer());
                                        jObjectData.put("position",position+2);
                                        jsonArray.put(jObjectData);
                                        ((ProfileQuestionActivity)context).setNewJsonArray(jsonArray);

                                    } catch (JSONException e) {

                                    }
                                    int filedAmount=getFilledAmount();
                                    int progress= Utils.getProgress(filedAmount,questionModelArrayList.size());
                                    setDonutProgress(progress);
                                }
                            }
                        }
                    }

                }else if (position==0)
                {

                    BaseQuestionFragment fragment=adapter.getItem(position+1);

                    if (fragment.getAnswer()!=null)
                    {
                        if (fragment.getAnswer().equals(""))
                        {
                            {
                            for (int i=0; i<myList.size(); i++)
                                if (myList.get(position+1).getPosition()==1)
                                {
                                    myList.get(position+1).setPosition(0);
                                    deacreaseFilledAmount();

                                    for (int x=0; x<jsonArray.length(); x++)
                                    {
                                        try {
                                            JSONObject jsonObject=jsonArray.getJSONObject(x);
                                            if (jsonObject.getString("question_id").equals(fragment.getQuestionID()))
                                            {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                                    jsonArray.remove(x);
                                                    ((ProfileQuestionActivity)context).setNewJsonArray(jsonArray);
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }


                                    int filedAmount=getFilledAmount();
                                    int progress= Utils.getProgress(filedAmount,questionModelArrayList.size());
                                    setDonutProgress(progress);
                                }
                            }
                        }else {
                            JSONObject jObjectData = new JSONObject();
                            for (int i=0; i<myList.size(); i++)
                            {
                                if (myList.get(position+1).getPosition()==0)
                                {
                                    myList.get(position+1).setPosition(1);
                                    increaseFilledAmount();
                                    try {
                                        jObjectData.put("question_id",fragment.getQuestionID());
                                        jObjectData.put("answer",fragment.getAnswer());
                                        jObjectData.put("position",position+2);
                                        jsonArray.put(jObjectData);
                                        ((ProfileQuestionActivity)context).setNewJsonArray(jsonArray);

                                    } catch (JSONException e) {

                                    }
                                    int filedAmount=getFilledAmount();
                                    int progress= Utils.getProgress(filedAmount,questionModelArrayList.size());
                                    setDonutProgress(progress);
                                }
                            }
                        }
                    }
                }else if (position==adapter.getCount())
                {

                }
                    oldPos=position;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }



    private void setupViewPager(ViewPager viewPager) {


        adapter = new ViewPagerAdapter(getChildFragmentManager());

        for (int i=0; i<questionModelArrayList.size(); i++)
        {
            if (questionModelArrayList.get(i).getQuestionType().equals("editText"))
            {
//                adapter.addFrag(new NameQuestionFragment(), "Name");
                adapter.addFrag(NameQuestionFragment.getInstance(i,questionModelArrayList.get(i)),"Name");

            } else if (questionModelArrayList.get(i).getQuestionType().equals("date"))
            {
                adapter.addFrag(DateFragment.getInstance(i, questionModelArrayList.get(i)), "Date");
            }else if (questionModelArrayList.get(i).getQuestionType().equals("radio"))
            {
                adapter.addFrag(RadioQuestionFragment.getInstance(i, questionModelArrayList.get(i),questionModelArrayList.get(i).getOptionsModel()),"Radio");
            }else if (questionModelArrayList.get(i).getQuestionType().equals("checkbox"))
            {
                adapter.addFrag(CheckBoxQuestionFragment.getInstance(i, questionModelArrayList.get(i),questionModelArrayList.get(i).getOptionsModel()),"CheckBox");

            }else if (questionModelArrayList.get(i).getQuestionType().equals("spinner"))
            {
                adapter.addFrag(SpinnerQuestionFragment.getInstance(i, questionModelArrayList.get(i),questionModelArrayList.get(i).getOptionsModel()),"Spinner");
            }

        }
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<BaseQuestionFragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }


        @Override
        public BaseQuestionFragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(BaseQuestionFragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public float getPageWidth (int position) {
            return 0.93f;
        }
    }



    public void setDonutProgress(int progress)
    {
        circleProgressBar.setProgress(progress);
        circleProgressBar.setText(String.valueOf(progress)+"%");
    }

    public void increaseFilledAmount()
    {
        filledAmount=filledAmount+1;
    }

    public void deacreaseFilledAmount()
    {
            filledAmount=filledAmount-1;
    }

    public int getFilledAmount()
    {
        return filledAmount;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }
}


