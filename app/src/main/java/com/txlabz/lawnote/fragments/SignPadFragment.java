package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;


public class SignPadFragment extends BaseFragment{

    Context context;
    int currentPosition;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_sign_pad, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    public static Fragment getInstance(int i) {
        SignPadFragment object=new SignPadFragment();
        object.currentPosition=i;
        return object;
    }
}
