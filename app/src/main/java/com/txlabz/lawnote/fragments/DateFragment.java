package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CalendarView;
import android.widget.TextView;

import com.txlabz.lawnote.activities.ProfileQuestionActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;

import java.util.Calendar;


public class DateFragment extends BaseQuestionFragment {

    Context context;
    CalendarView calendar;
    TextView counter,firstEditText;
    String question;
    String questionTag;
    int currentPosition;
    int DateCounter=0;
    String TodaysDate="";
    String questionID;
    String optionAnswer;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.date_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        calendar=(CalendarView)view.findViewById(R.id.calendar);
        final int totalSize=((ProfileQuestionActivity)getContext()).getQuestionArrayListSize();
        counter=(TextView)view.findViewById(R.id.counter);
        firstEditText=(TextView)view.findViewById(R.id.firstEditText);

        if (optionAnswer.length()>0)
        {
            String date = optionAnswer;
            String parts[] = date.split("-");

            int year = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            int day = Integer.parseInt(parts[2]);

            Calendar myCalender = Calendar.getInstance();
            myCalender.set(Calendar.YEAR, year);
            myCalender.set(Calendar.MONTH, month);
            myCalender.set(Calendar.DAY_OF_MONTH, day);

            long milliTime = myCalender.getTimeInMillis();

            calendar.setDate(milliTime, true, true);

            ((ProfileQuestionActivity)context).setDeadLine(optionAnswer);
        }

        getQuestionID=questionID;

        firstEditText.setText(question);

        calendar.setShowWeekNumber(false);

        calendar.setWeekSeparatorLineColor(getResources().getColor(R.color.transparent));

        calendar.setSelectedDateVerticalBar(R.color.white);


        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {


//                if (DateCounter==0)
//                {
//                    TodaysDate=year+"-"+month+"-"+dayOfMonth;
//                    DateCounter++;
//                }

                String SelectedDate=year+"-"+month+"-"+dayOfMonth;

                if (SelectedDate.equals(TodaysDate))
                {

                }else {
                    ((ProfileQuestionActivity)context).setDeadLine(SelectedDate);
                   answer=SelectedDate;
                    getQuestionTag=questionTag;
                }

            }
        });


        counter.setText(currentPosition+"/"+totalSize);

    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    public static BaseQuestionFragment getInstance(int i, QuestionModel questionModel) {
        DateFragment object=new DateFragment();
        object.currentPosition=i+1;
        object.optionAnswer=questionModel.getAnswer();
        object.questionID=questionModel.getQuestionID();
        object.question=questionModel.getQuestionText();
        object.questionTag=questionModel.getTagId();
        return object;
    }
}
