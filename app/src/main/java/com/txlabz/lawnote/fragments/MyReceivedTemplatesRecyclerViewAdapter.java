package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.ViewHolder.TemplateListItemViewHolder;
import com.txlabz.lawnote.modelClasses.TemplateModel;

import java.util.ArrayList;
import java.util.List;


public class MyReceivedTemplatesRecyclerViewAdapter extends RecyclerView.Adapter<TemplateListItemViewHolder> {

    private final List<TemplateModel> mValues;
    private final Context context;

    public MyReceivedTemplatesRecyclerViewAdapter(Context context, ArrayList<TemplateModel> items) {
        mValues = items;
        this.context=context;

    }

    @Override
    public TemplateListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_list_item, parent, false);
        return new TemplateListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TemplateListItemViewHolder holder, int position) {

        holder.setData(context,mValues.get(position), HtmlTemplateFragment.HTML_PAGE_TYPE_RECEIVED);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }



}
