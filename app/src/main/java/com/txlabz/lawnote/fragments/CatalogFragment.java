package com.txlabz.lawnote.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.GsonBuilder;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.activities.MainActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.adapters.CatalogAdapter;
import com.txlabz.lawnote.modelClasses.Categories;
import com.txlabz.lawnote.modelClasses.HomeDataModel;
import com.txlabz.lawnote.utils.AlertDialogBox;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.LocalCacheUtility;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CatalogFragment extends BaseFragment {

    Context context;
    public static String FRAGMENT_TAG = CatalogFragment.class.getName();
    List<Categories> dummyData;
    RecyclerView recyclerView;
    int counter,swipeCounter=0;
    SwipeRefreshLayout swipeLayout;
    ProgressDialog progressDialog;
    AlertDialogBox alertDialogBox;
    public String fileName="myDataFile";
    SharedPreferences myData;
    ArrayList<Categories> categoriesArrayList;
    String userID;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_freelance, container, false);
        ((MainActivity)context).setTitle(AppConstants.CATALOG);
        ((MainActivity)context).changeAddIcon(R.drawable.search);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        progressDialog=new ProgressDialog(getContext());
        alertDialogBox=new AlertDialogBox();
        myData=getContext().getSharedPreferences(fileName,0);
        userID=myData.getString(AppConstants.PREF_KEY_USER_ID,AppConstants.NOT_FOUND);
        categoriesArrayList=new ArrayList<>();
        init(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipeLayout);
        swipeLayout.setColorSchemeResources(R.color.mainPrimaryLight, R.color.colorAccentMain, R.color.mainPrimaryDark);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            swipeLayout.setProgressViewOffset(false, 0, Utils.getToolbarHeight(context));

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshCall();
            }
        });
    }

    private void init(View view) {

        Bundle bundle = getArguments();
        if (bundle != null) {
            ((MainActivity) context).setTitle(bundle.getString(AppConstants.NAME));
            ((MainActivity) context).changeAddIcon(R.drawable.search);
            LocalCacheUtility localCacheUtility = LocalCacheUtility.getInstance();
            dummyData = localCacheUtility.getCategoriesArrayList();

            for (int i=0; i<dummyData.size(); i++)
            {
                if (dummyData.get(i).getCategoryTitle().equals("Create Your Own"))
                {
                    counter++;
                }
            }

            if (counter==0)
            {
                Categories categories=new Categories();
                categories.setCategoryTitle("Create Your Own");
                categories.setCategoryDescryption("Create agreement as you need");
                categories.setCategoryImage(String.valueOf(R.drawable.create));

                dummyData.add(categories);

                counter++;
            }


            LinearLayoutManager manager = new LinearLayoutManager(context);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            CatalogAdapter adapter = new CatalogAdapter(context, dummyData);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(manager);
        }

    }

    public void searchCategoryTemplate(String categoryName)
    {
        List<Categories> catalogList=new ArrayList<>();

        for (int i=0; i<dummyData.size();i++)

        {
            String getDataTitle=dummyData.get(i).getCategoryTitle().toLowerCase();
            if (getDataTitle.contains(categoryName.toLowerCase()))
            {
                catalogList.add(dummyData.get(i));

            }
        }
        LinearLayoutManager manager = new LinearLayoutManager(context);
        CatalogAdapter adapter = new CatalogAdapter(context, catalogList);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    public void swipeRefreshCall()
    {
        swipeLayout.setRefreshing(true);
        AlertDialogBox alertDialogBox=new AlertDialogBox();
        if (Utils.IsNetworkConnected(getContext()))
        {

            Ion.with(this)
                    .load(AppConstants.API_GET_HOME_CONTENT+"user_id="+userID)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            swipeLayout.setRefreshing(false);

                            if (e==null)
                            {
                                if (result!=null)
                                {
                                    try {

                                        HomeDataModel array = new GsonBuilder().create().fromJson(result, HomeDataModel.class);
                                        categoriesArrayList=array.getCategories();

                                        LinearLayoutManager manager = new LinearLayoutManager(context);
                                        CatalogAdapter adapter = new CatalogAdapter(context, categoriesArrayList);
                                        recyclerView.setLayoutManager(manager);
                                        recyclerView.setAdapter(adapter);

//                                        for (int i=0; i<categoriesArrayList.size(); i++)
//                                        {
//                                            if (categoriesArrayList.get(i).getCategoryTitle().equals("Create Your Own"))
//                                            {
//                                                swipeCounter++;
//                                            }
//                                        }
//
//                                        if (swipeCounter==0)
//                                        {
                                            Categories categories=new Categories();
                                            categories.setCategoryTitle("Create Your Own");
                                            categories.setCategoryDescryption("Create agreement as you need");
                                            categories.setCategoryImage(String.valueOf(R.drawable.create));

                                            categoriesArrayList.add(categories);
//
//                                            swipeCounter++;
//                                        }
                                    }catch (Exception ex)
                                    {
                                        ex.printStackTrace();
                                    }
                                }
                            }else {
                                e.printStackTrace();
                            }
                        }
                    });
        }
        else {
            alertDialogBox.loadAlert(AppConstants.CHECK_CONNECTION,getContext());
        }
    }
}
