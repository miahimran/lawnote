package com.txlabz.lawnote.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.txlabz.lawnote.activities.MainActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.TemplateDetailActivity;
import com.txlabz.lawnote.adapters.CardArrayAdapter;
import com.txlabz.lawnote.modelClasses.Categories;
import com.txlabz.lawnote.modelClasses.NotificationDataModel;
import com.txlabz.lawnote.modelClasses.NotificationModel;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.LocalCacheUtility;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;


public class HomeFragment extends BaseFragment implements View.OnClickListener{

    public static String FRAGMENT_TAG = HomeFragment.class.getName();
    Context context;
    ArrayList<TemplateModel> arrayList;
    private CardArrayAdapter cardsAdapter;
    SwipeFlingAdapterView flingContainer;
    TextView title_1,title_2,title_3,description_1,description_2,description_3;
    ProgressDialog progressDialog;

    ArrayList<Categories> categoriesArrayList;
    private ArrayList<NotificationModel> notificationsList;
    private LocalCacheUtility localCacheUtility;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        ((MainActivity)context).setTitle(context.getResources().getString(R.string.lawnote));
        ((MainActivity)context).changeAddIcon(R.drawable.add);
        View personal = view.findViewById(R.id.personalContainer);
        View freelance = view.findViewById(R.id.freelanceContainer);
        View digital = view.findViewById(R.id.digitalContainer);
        View more = view.findViewById(R.id.moreContainer);
        title_1=(TextView)view.findViewById(R.id.title_1);
        title_2=(TextView)view.findViewById(R.id.title_2);
        title_3=(TextView)view.findViewById(R.id.title_3);
        description_1=(TextView)view.findViewById(R.id.description_1);
        description_2=(TextView)view.findViewById(R.id.description_2);
        description_3=(TextView)view.findViewById(R.id.description_3);
        arrayList=new ArrayList<TemplateModel>();
        categoriesArrayList=new ArrayList<>();
        progressDialog=new ProgressDialog(context);

        ((MainActivity)getContext()).headerSettingForHome();
        localCacheUtility = LocalCacheUtility.getInstance();
        categoriesArrayList = localCacheUtility.getCategoriesArrayList();

        if(categoriesArrayList!=null&&categoriesArrayList.size()>3) {

            title_1.setText(categoriesArrayList.get(0).getCategoryTitle());
            description_1.setText(categoriesArrayList.get(0).getCategoryDescryption());


            title_2.setText(categoriesArrayList.get(1).getCategoryTitle());
            description_2.setText(categoriesArrayList.get(1).getCategoryDescryption());

            title_3.setText(categoriesArrayList.get(2).getCategoryTitle());
            description_3.setText(categoriesArrayList.get(2).getCategoryDescryption());

        }


        personal.setOnClickListener(this);
        freelance.setOnClickListener(this);
        digital.setOnClickListener(this);
        more.setOnClickListener(this);

        notificationsList = localCacheUtility.getNotificationDataModel();



        if (notificationsList == null || notificationsList.size() == 0) {
            notificationsList = new ArrayList<>();
            notificationsList.add(new NotificationModel(getString(R.string.no_notifications)));
        }

        cardsAdapter = new CardArrayAdapter(context, notificationsList);

        flingContainer = (SwipeFlingAdapterView) view.findViewById(R.id.frame);
        flingContainer.setAdapter(cardsAdapter);
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int i, Object o) {

                Log.d(AppConstants.TAG,"Card item clicked index "+i);

                NotificationModel notificationModel = notificationsList.get(i);

                Gson gson=new Gson();
                Log.d(AppConstants.TAG,"Selected notifications  "+gson.toJson(notificationModel));

                showSelectedCardDetail(notificationModel);

                notificationsList.remove(i);
                cardsAdapter.notifyDataSetChanged();
                localCacheUtility.removeNotification(notificationsList.remove(i));


            }
        });
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                //  if(cardsAdapter.getCount() > 2)
                {
                    Log.d("LIST", "removed object!");
                    notificationsList.remove(0);

                    if(notificationsList.size()==0)
                    {
                        if(localCacheUtility==null){
                            localCacheUtility=LocalCacheUtility.getInstance();
                        }
                        notificationsList.addAll(localCacheUtility.getNotificationDataModel());
                    }
                    cardsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
            }

            @Override
            public void onRightCardExit(Object dataObject) {
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                Log.i("HPC","items : " + itemsInAdapter);
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
            }
        });
    }

    private void showSelectedCardDetail(NotificationModel notificationModel) {

        int   flag;
        String  templateId=notificationModel.getTemplateId();
        String  templatetTitle=notificationModel.getTemplateTitle();
        if(TextUtils.isEmpty(notificationModel.getFlag())){
            loadCategories();return;
        }

        flag = Integer.parseInt(notificationModel.getFlag());


        if(flag== AppConstants.NOTIFICATION_TYPE_RECEIVED||flag==AppConstants.NOTIFICATION_TYPE_SIGNED_BY_SENDER)
        {

            Intent intent=new Intent(context , TemplateDetailActivity.class);
            intent.putExtra(AppConstants.PUT_EXTRA_KEY_TEMPLATE_ID,templateId);
            intent.putExtra(AppConstants.PUT_EXTRA_KEY_HTML_PAGE_TYPE,HtmlTemplateFragment.HTML_PAGE_TYPE_RECEIVED);
            intent.putExtra(AppConstants.PUT_EXTRA_KEY_TEMPLATE_TITLE,templatetTitle);
            context.startActivity(intent);
            //  loadReceivedFragment();

        }
        else if(flag==AppConstants.NOTIFICATION_TYPE_COMPLETED||flag==AppConstants.NOTIFICATION_TYPE_SIGNED_BY_OTHER_PENDING){

            Intent intent=new Intent(context , TemplateDetailActivity.class);
            intent.putExtra(AppConstants.PUT_EXTRA_KEY_TEMPLATE_ID,templateId);
            intent.putExtra(AppConstants.PUT_EXTRA_KEY_HTML_PAGE_TYPE,HtmlTemplateFragment.HTML_PAGE_TYPE_COMPLETED);
            intent.putExtra(AppConstants.PUT_EXTRA_KEY_TEMPLATE_TITLE,templatetTitle);
            context.startActivity(intent);
        }


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //loadUserTemplates();
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        Bundle bundle = null;
        switch (v.getId()){
            case R.id.personalContainer:
                loadTemplateFragment(0);
                break;
            case R.id.freelanceContainer:
                loadTemplateFragment(1);
                break;
            case R.id.digitalContainer:
                loadTemplateFragment(2);
                break;
            case R.id.moreContainer:
                fragment = new CatalogFragment();
                bundle = new Bundle();
                bundle.putString(AppConstants.NAME,AppConstants.CATALOG);
                bundle.putSerializable("catalog_array",categoriesArrayList);
                fragment.setArguments(bundle);
                ((MainActivity)context).loadFragment(fragment);
                break;
        }
    }

    private void loadTemplateFragment(int position) {
        if (!Utils.IsNetworkConnected(context)) {
            DialogsUtils.showAlert(context, AppConstants.CHECK_CONNECTION);
            return;
        }
        TemplateFragment fragment = new TemplateFragment();
        Bundle bundle = new Bundle();
        Categories categories = categoriesArrayList.get(position);
        bundle.putString(AppConstants.NAME,AppConstants.PERSONAL);
        bundle.putString(AppConstants.CATEGORY_ID,categories.getCategoryId());
        bundle.putString(AppConstants.CATEGORY_IMAGE, categories.getCategoryImage());
        bundle.putSerializable("template_array",categories.getTemplateModels());
        fragment.setArguments(bundle);
        ((MainActivity)context).loadFragment(fragment);
    }

    @Override
    public void onResume(){
        super.onResume();
        //loadUserTemplates();
        ((MainActivity)context).setTitle(context.getResources().getString(R.string.lawnote));
        ((MainActivity)context).changeAddIcon(R.drawable.add);
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");

        flingContainer.setAdapter(cardsAdapter);
        cardsAdapter.notifyDataSetChanged();
        if(flingContainer != null)
            flingContainer.removeAllViewsInLayout();
        flingContainer = null;
        cardsAdapter = null;
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    public void loadCategories()
    {
        Bundle bundle;
        Fragment fragment = new CatalogFragment();
        bundle = new Bundle();
        bundle.putString(AppConstants.NAME,AppConstants.CATALOG);
        bundle.putSerializable("catalog_array",categoriesArrayList);
        fragment.setArguments(bundle);
        ((MainActivity)context).loadFragment(fragment);
    }

    public void loadReceivedFragment() {

        MainActivity mainActivity= (MainActivity) getActivity();

        mainActivity.loadReceivedFragment();
    }
}
