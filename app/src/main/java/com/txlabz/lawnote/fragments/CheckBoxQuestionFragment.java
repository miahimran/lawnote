package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.txlabz.lawnote.activities.ProfileQuestionActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.OptionsModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;

import java.util.ArrayList;


public class CheckBoxQuestionFragment extends BaseQuestionFragment {

    Context context;
    TextView counter;
    int currentPosition;
    Boolean filledAmountUpdated=false;
    CheckBox firstCheck,secondCheck,thirdCheck;
    String question;
    String questionTag;
    ArrayList<OptionsModel> optionsModelArrayList=new ArrayList<>();
    TextView firstEditText;
    LinearLayout linearLayout;
    int checkBox1,checkBox2,checkBox3=0;
    String questionID;
    String optionId;
    String optionAnswer;
    int SelectedAmount=0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.checkbox_question_layout, container, false);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final CreateTemplateFragment parentFragment=(CreateTemplateFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        counter=(TextView)view.findViewById(R.id.counter);
        final int totalSize=((ProfileQuestionActivity)getContext()).getQuestionArrayListSize();
        linearLayout=(LinearLayout)view.findViewById(R.id.linearLayout);
        firstEditText=(TextView)view.findViewById(R.id.firstEditText);

        firstEditText.setText(question);

        getQuestionID=questionID;


        for (int i=0; i<optionsModelArrayList.size(); i++)
        {
            CheckBox checkBox=new CheckBox(context);
            LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,20,0,0);
            checkBox.setLayoutParams(layoutParams);
            Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/AvenirNextLTPro-Regular.otf");
            checkBox.setTypeface(face);
            checkBox.setText(optionsModelArrayList.get(i).getOptionText());
            linearLayout.addView(checkBox);

        }

        if (checkBox1==1)
        {
            CheckBox ch=(CheckBox) linearLayout.getChildAt(0);
            ch.setChecked(true);
        }
        if (checkBox2==1)
        {
            CheckBox ch=(CheckBox) linearLayout.getChildAt(1);
            ch.setChecked(true);
        }
        if (checkBox3==1)
        {
            CheckBox ch=(CheckBox) linearLayout.getChildAt(2);
            ch.setChecked(true);
        }

        if (optionAnswer.length()>0)
        {
            String[] values=optionAnswer.split(",");
               // Toast.makeText(getContext(),optionAnswer,Toast.LENGTH_LONG).show();

                for(int x=0; x<values.length; x++)
                {
                    for (int i=0; i<optionsModelArrayList.size(); i++)
                    {
                        if (values[x].equals(optionsModelArrayList.get(i).getOptionID()))
                        {
//                            Toast.makeText(getContext(),values[x],Toast.LENGTH_LONG).show();
                            CheckBox ch=(CheckBox) linearLayout.getChildAt(i);
                            ch.setChecked(true);
                            SelectedAmount++;
                            if (i==0)
                            {
                                ((ProfileQuestionActivity)context).setCheckBoxValue1(optionsModelArrayList.get(i).getOptionValue());
                            }else if (i==1)
                            {
                                ((ProfileQuestionActivity)context).setCheckBoxValue2(optionsModelArrayList.get(i).getOptionValue());
                            }else if(i==2)
                            {
                                ((ProfileQuestionActivity)context).setCheckBoxValue3(optionsModelArrayList.get(i).getOptionValue());
                            }
                        }
                    }
                }
        }




        for (int i=0; i<optionsModelArrayList.size(); i++)
        {
            final int finalI = i;
            linearLayout.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox ch=(CheckBox) linearLayout.getChildAt(finalI);
                    if (ch.isChecked())
                    {
                        if (finalI==0)
                        {
                            ((ProfileQuestionActivity)context).setCheckBoxValue1(optionsModelArrayList.get(finalI).getOptionValue());
                            optionId=optionsModelArrayList.get(finalI).getOptionID();
                            checkBox1=1;
                            SelectedAmount++;
                            if (SelectedAmount==1)
                            {
                                answer=optionId;
                            }else {
                                answer=answer+","+optionId;
                            }
                        }else if (finalI ==1)
                        {
                            ((ProfileQuestionActivity)context).setCheckBoxValue2(optionsModelArrayList.get(finalI).getOptionValue());
                            optionId=optionsModelArrayList.get(finalI).getOptionID();
                            checkBox2=1;
                            SelectedAmount++;
                            if (SelectedAmount==1)
                            {
                                answer=optionId;
                            }else {
                                answer=answer+","+optionId;
                            }
                        }else if (finalI==2)
                        {
                            ((ProfileQuestionActivity)context).setCheckBoxValue3(optionsModelArrayList.get(finalI).getOptionValue());
                            optionId=optionsModelArrayList.get(finalI).getOptionID();
                            checkBox3=1;
                            SelectedAmount++;
                            if (SelectedAmount==1)
                            {
                                answer=optionId;
                            }else {
                                answer=answer+","+optionId;
                            }
                        }
                        getQuestionTag=questionTag;

                    }else {
                        SelectedAmount--;

                        StringBuilder builder=new StringBuilder(answer);

                        if (finalI==0)
                        {
                            checkBox1=0;
                            ((ProfileQuestionActivity)context).setCheckBoxValue1("");
                        }else if (finalI==1)
                        {
                            checkBox2=0;
                            ((ProfileQuestionActivity)context).setCheckBoxValue2("");
                        }else if (finalI==2)
                        {
                            checkBox3=0;
                            ((ProfileQuestionActivity)context).setCheckBoxValue3("");
                        }

                        if (SelectedAmount!=0)
                        {
                            builder.delete(finalI,finalI+2);
                        }

                        answer=builder.toString();

                        if (SelectedAmount==0)
                        {
                           answer="";
                        }
                    }
                }
            });
        }

        counter.setText(currentPosition+"/"+totalSize);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    public static BaseQuestionFragment getInstance(int i, QuestionModel questionModel, ArrayList<OptionsModel> optionsModel) {
        CheckBoxQuestionFragment object=new CheckBoxQuestionFragment();
        object.currentPosition=i+1;
        object.questionID=questionModel.getQuestionID();
        object.optionAnswer=questionModel.getAnswer();
        object.question=questionModel.getQuestionText();
        object.questionTag=questionModel.getTagId();
        object.optionsModelArrayList=optionsModel;
        return object;
    }

    public boolean isCheckedBox()
    {
        if (firstCheck.isChecked() || secondCheck.isChecked() || thirdCheck.isChecked())
        {
            return true;
        }
        return false;
    }
}
