package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.txlabz.lawnote.activities.ProfileQuestionActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;


public class NameQuestionFragment extends BaseQuestionFragment {

    Context context;
    EditText name;
    TextView counter;
    int currentPosition;
    int totalSize;
    static int questionNo;
    long idle_min = 1000; // 4 seconds after user stops typing
    long last_text_edit = 0;
    Handler h = new Handler();
    boolean already_queried = false;
    QuestionModel questionModel;
    String question;
    TextView firstEditText;
    String questionTag;
    int startLength;
    Boolean onceDecrease=true;
    String questionID;
    String getAnswer;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.edit_field_question_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
      firstEditText=(TextView)view.findViewById(R.id.firstEditText);
        counter=(TextView)view.findViewById(R.id.counter);
        questionModel=new QuestionModel();

//        int currentPosition=((ProfileQuestionActivity)context).getPosition();
        totalSize=((ProfileQuestionActivity)context).getQuestionArrayListSize();

        getQuestionID=questionID;

        counter.setText(currentPosition+"/"+totalSize);

        name=(EditText)view.findViewById(R.id.name);

        if (getAnswer.length()>0)
        {
            name.setText(getAnswer);

            if (questionTag.equals("client_name"))
            {

                ((ProfileQuestionActivity)context).setClientName(name.getText().toString());
                //answer=name.getText().toString();
                getQuestionTag=questionTag;
            }else if (questionTag.equals("freelance_name"))
            {
                ((ProfileQuestionActivity)context).setFreeLanceName(name.getText().toString());
                //answer=name.getText().toString();
                getQuestionTag=questionTag;
            }else if (questionTag.equals("work_scope"))
            {
                ((ProfileQuestionActivity)context).setWorkScope(name.getText().toString());
                //answer=name.getText().toString();
                getQuestionTag=questionTag;
            }else  if (questionTag.equals("complete_date"))
            {
                ((ProfileQuestionActivity)context).setCompleteData(name.getText().toString());
                //answer=name.getText().toString();
                getQuestionTag=questionTag;
            }else  if (questionTag.equals("flat_fee"))
            {
                ((ProfileQuestionActivity)context).setFlatFee(name.getText().toString());
                //answer=name.getText().toString();
                getQuestionTag=questionTag;
            }else if (questionTag.equals("revision_rounds"))
            {
                ((ProfileQuestionActivity)context).setRevisionRounds(name.getText().toString());
                //answer=name.getText().toString();
                getQuestionTag=questionTag;
            }else if (questionTag.equals("freelancer_fee"))
            {
                ((ProfileQuestionActivity)context).setFreelanceFee(name.getText().toString());
                //answer=name.getText().toString();
                getQuestionTag=questionTag;
            }

        }
        name.clearFocus();




        final TextWatcher textWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ((ProfileQuestionActivity)context).setFreelancerName(name.getText().toString());

                if (questionTag.equals("client_name"))
                {

                    ((ProfileQuestionActivity)context).setClientName(name.getText().toString());
                    answer=name.getText().toString();
                    getQuestionTag=questionTag;
                }else if (questionTag.equals("freelance_name"))
                {
                    ((ProfileQuestionActivity)context).setFreeLanceName(name.getText().toString());
                    answer=name.getText().toString();
                    getQuestionTag=questionTag;
                }else if (questionTag.equals("work_scope"))
                {
                    ((ProfileQuestionActivity)context).setWorkScope(name.getText().toString());
                    answer=name.getText().toString();
                    getQuestionTag=questionTag;
                }else  if (questionTag.equals("complete_date"))
                {
                    ((ProfileQuestionActivity)context).setCompleteData(name.getText().toString());
                    answer=name.getText().toString();
                    getQuestionTag=questionTag;
                }else  if (questionTag.equals("flat_fee"))
                {
                    ((ProfileQuestionActivity)context).setFlatFee(name.getText().toString());
                    answer=name.getText().toString();
                    getQuestionTag=questionTag;
                }else if (questionTag.equals("revision_rounds"))
                {
                    ((ProfileQuestionActivity)context).setRevisionRounds(name.getText().toString());
                    answer=name.getText().toString();
                    getQuestionTag=questionTag;
                }else if (questionTag.equals("freelancer_fee"))
                {
                    ((ProfileQuestionActivity)context).setFreelanceFee(name.getText().toString());
                    answer=name.getText().toString();
                    getQuestionTag=questionTag;
                }
            }
        };

        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    name.addTextChangedListener(textWatcher);
                }else {
                    name.removeTextChangedListener(textWatcher);
                }
            }
        });

        firstEditText.setText(question);



       // Toast.makeText(getContext(),String.valueOf(questionNo),Toast.LENGTH_LONG).show();


    }
    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }
    public static BaseQuestionFragment getInstance(int i, QuestionModel questionModel) {
        NameQuestionFragment object=new NameQuestionFragment();
        object.currentPosition=i+1;
        object.questionID=questionModel.getQuestionID();
        object.getAnswer=questionModel.getAnswer();
        questionNo=i+1;
        object.question=questionModel.getQuestionText();
        object.questionTag=questionModel.getTagId();
        return object;
    }

}
