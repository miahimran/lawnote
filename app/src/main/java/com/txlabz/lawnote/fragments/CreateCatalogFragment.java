package com.txlabz.lawnote.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.activities.CreateYourOwnActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.adapters.SignatureRecyclerviewAdapter;
import com.txlabz.lawnote.interfaces.SaveCompleteListener;
import com.txlabz.lawnote.modelClasses.ContractInfo;
import com.txlabz.lawnote.modelClasses.ReceiversInfo;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;
import java.io.File;
import java.util.ArrayList;


public class CreateCatalogFragment extends BaseTemplateQuestionsFragment{

    public static String FRAGMENT_TAG = CreateCatalogFragment.class.getName();
    Context context;
    TextView freelancerName,freelancerEmail,freelanceName,freelanceEmail;
    RelativeLayout layout1,layout2;
    File getJpgImageFile;
    Button submitBtn;
    String getFreelanceFirstName,getFreelanceLastName,getFreelanceEmail;
    String userName;
    String email;

    EditText templateContent;
    EditText title;
    String userID;
    String content;
    String titleOfAgreement;
    Bitmap senderSignaturebitmap;
    String getSenderSignature;
    Bitmap getSenderImageBitmap;
    RecyclerView recyclerView;
    ImageView userSignature;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_catalog_create, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initViews(view);
    }

    private void initViews(View view) {

        userName = ((CreateYourOwnActivity) getContext()).getUserName();
        email = ((CreateYourOwnActivity) getContext()).getEmail();
        selectedTemplate = ((CreateYourOwnActivity) getContext()).getSelectedTemplate();
        freelancerName = (TextView) view.findViewById(R.id.freelancerName);
        freelancerEmail = (TextView) view.findViewById(R.id.freelancerEmail);
        freelanceName = (TextView) view.findViewById(R.id.freelanceName);
        freelanceEmail = (TextView) view.findViewById(R.id.freelanceEmail);
        layout1 = (RelativeLayout) view.findViewById(R.id.layout1);
        layout2 = (RelativeLayout) view.findViewById(R.id.layout2);
        submitBtn = (Button) view.findViewById(R.id.submit);
        templateContent = (EditText) view.findViewById(R.id.templateContent);
        title = (EditText) view.findViewById(R.id.title);
        userSignature = (ImageView) view.findViewById(R.id.ivAutSignature);
        recyclerView = (RecyclerView) view.findViewById(R.id.receiversRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setNestedScrollingEnabled(false);
        if (selectedTemplate != null)
            recyclerView.setAdapter(new SignatureRecyclerviewAdapter(context, selectedTemplate.getReceivers()));
        freelancerName.setText(userName);
        freelancerEmail.setText(email);


        ((CreateYourOwnActivity) getContext()).setGeneralFreeLancerName(userName);
        ((CreateYourOwnActivity) getContext()).setGeneralFreeLancerEmail(email);

        getSenderSignature = ((CreateYourOwnActivity) getContext()).getSenderSignature();

        if (!TextUtils.isEmpty(getSenderSignature)) {
            showCreatorSignatures();
        }

        title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                titleOfAgreement = title.getText().toString();
                ((CreateYourOwnActivity) getContext()).setTitleOfAgreement(titleOfAgreement);
            }
        });

        templateContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                content = templateContent.getText().toString();
                ((CreateYourOwnActivity) getContext()).setContentOfAgreement(content);
            }
        });


        String agreementTitle = ((CreateYourOwnActivity) getContext()).getTitleOfAgreement();
        String contentText = ((CreateYourOwnActivity) getContext()).getContentOfAgreement();
        File freeLancerFile = ((CreateYourOwnActivity) getContext()).getGetJpgSign();
        File getFreelancFile = ((CreateYourOwnActivity) getContext()).getJpgFreelanceSign();
        String FreelanceName1 = ((CreateYourOwnActivity) getContext()).getGetFreeLanceName();
        String FreelanceEmail1 = ((CreateYourOwnActivity) getContext()).getGetFreeLanceEmail();
        int INIT_TABS_FOR = ((CreateYourOwnActivity) getContext()).getINIT_TABS_FOR();

        if (agreementTitle != null) {
            title.setText(agreementTitle);
        }
        if (contentText != null) {
            templateContent.setText(contentText);
        }
        if (FreelanceName1 != null) {
            freelanceName.setText(FreelanceName1);
        }
        if (FreelanceEmail1 != null) {
            freelanceEmail.setText(FreelanceEmail1);
        }
        templateContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        templateContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askForSign();
            }
        });

        if (INIT_TABS_FOR == CreateEditTemplateActivity.INIT_TABS_FOR_COMPLETED_PREVIEW) {
            submitBtn.setVisibility(View.GONE);
        }
    }
    private void showCreatorSignatures() {
        userSignature.setVisibility(View.VISIBLE);
        Utils.loadImage(context ,userSignature,getSenderSignature);
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

    public String getUserID() {
        return userID;
    }

    public String getGetFreelanceFirstName() {
        return getFreelanceFirstName;
    }

    public String getGetFreelanceLastName() {
        return getFreelanceLastName;
    }

    public String getFreelanceEmail() {
        return getFreelanceEmail;
    }

    public File getGetJpgImageFile() {
        return getJpgImageFile;
    }

    public String getContent() {
        return content;
    }

    public String getTitleOfAgreement() {
        return titleOfAgreement;
    }

    private void postDataToServer(ContractInfo contractInfo) {
        getSenderSignature = ((CreateYourOwnActivity)getContext()).getSenderSignature();
        userID = ((CreateYourOwnActivity)context).getUserID();
        ((CreateYourOwnActivity)getContext()).postDataToServer(contractInfo);
    }

    @Override
    public void repopulateData(TemplateModel selectedTemplate) {

    }

    @Override
    public void saveUserInput(SaveCompleteListener onDataSaveCompleted) {

    }

    @Override
    public void refreshReceivers() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode== REQUEST_CODE_SIGNATURE_FOR_SEND_CONTRACT &&resultCode== Activity.RESULT_OK) {
            String signaturePath =  data.getStringExtra(AppConstants.KEY_SIGNATURE);
            Bitmap bitmap = Utils.getBitmapFromPath(signaturePath);
            userSignature.setVisibility(View.VISIBLE);
            userSignature.setImageBitmap(bitmap);
            inputReceiversInfo(selectedTemplate, bitmap);
        } else if (requestCode == AppConstants.RC_ADD_RECEIVERS) {
            ArrayList<ReceiversInfo> receiversInfo = (ArrayList<ReceiversInfo>) data.getSerializableExtra(AppConstants.INTENT_KEY_RECEIVERS_LIST);
            ContractInfo contractInfo = (ContractInfo) data.getSerializableExtra(AppConstants.INTENT_KEY_CONTRACT_INFO);
            boolean shouldSendContract = data.getBooleanExtra(AppConstants.INTENT_KEY_SHOULD_SEND_CALL, true);
            if (receiversInfo != null && receiversInfo.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setAdapter(new SignatureRecyclerviewAdapter(context, contractInfo.getReceivers()));
                if (shouldSendContract) {
                    postDataToServer(contractInfo);
                }
            }
        }
        else
            super.onActivityResult(requestCode, resultCode, data);

    }

    public static BaseTemplateQuestionsFragment newInstance() {
        return new CreateCatalogFragment();
    }
}
