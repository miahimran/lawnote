package com.txlabz.lawnote.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.BaseActivity;
import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.activities.ManageActivity;
import com.txlabz.lawnote.activities.ReceiversInputActivity;
import com.txlabz.lawnote.handler.SendReceiverSignatureHandler;
import com.txlabz.lawnote.handler.TemplateSaveAnswerHandler;
import com.txlabz.lawnote.handler.TemplateSendContractHandler;
import com.txlabz.lawnote.interfaces.DialogCallBackListener;
import com.txlabz.lawnote.interfaces.ReceiversAddCompleteListener;
import com.txlabz.lawnote.interfaces.SaveCompleteListener;
import com.txlabz.lawnote.interfaces.ServerResponseListener;
import com.txlabz.lawnote.interfaces.SignatureNowOrLaterListener;
import com.txlabz.lawnote.modelClasses.ContractInfo;
import com.txlabz.lawnote.modelClasses.ReceiversInfo;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.modelClasses.TemplateToSaveInfo;
import com.txlabz.lawnote.modelClasses.server_responses.GeneralResponse;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/24/2017.
 */

public abstract class BaseTemplateQuestionsFragment extends BaseFragment implements ReceiversAddCompleteListener {


    public static final int HTML_PAGE_TYPE_PREVIEW = 1;
    public static final int HTML_PAGE_TYPE_EDIT = 2;
    public static final int HTML_PAGE_TYPE_COMPLETED = 3;
    public static final int HTML_PAGE_TYPE_RECEIVED = 4;

    protected static final int REQUEST_CODE_SIGNATURE_FOR_SEND_CONTRACT = 13;
    protected static final int REQUEST_CODE_SIGNATURE_FOR_RECEIVED_TEMPLATE = 14;

    protected   TemplateModel selectedTemplate;
    protected String userName;
    protected String userEmail;
    protected int htmlPageType;
    protected Button submitBtn;

    abstract public void repopulateData(TemplateModel selectedTemplate);
    public abstract void saveUserInput(SaveCompleteListener onDataSaveCompleted);

    public  void submitTemplate() {

        if(htmlPageType==HTML_PAGE_TYPE_RECEIVED)
        {
            sendUserSignature();
        }
        else if( BaseTemplateQuestionsFragment.this instanceof CreateTemplateInputFragment)
        {
            saveTemplateAnswers(true,null);
        }
        else {
            saveTemplateAnswers(false,null);}
    }

    private void sendUserSignature() {

        String templateId=selectedTemplate.getTemplateId();
        String signPath=null;
        ArrayList<ReceiversInfo> receivers = selectedTemplate.getReceivers();

        if(receivers!=null&&receivers.size()>0)
        {
            for(int i=0;i<receivers.size();i++)
            {
                if(receivers.get(i).getEmail().equals(userEmail))
                {
                    signPath=receivers.get(i).getSignatureImage();
                    break;
                }
            }

        }

        if(TextUtils.isEmpty(templateId)||TextUtils.isEmpty(signPath))
        {
            Utils.showErrorMessage(context,getString(R.string.msg_envalid_sign_or_template_id));
        }
        else
        {
            SendReceiverSignatureHandler.sendReceiverSignature(context, templateId, signPath, new ServerResponseListener() {
                @Override
                public void onServerResponse(GeneralResponse generalResponse) {

                    if(generalResponse!=null)
                    {
                        if(generalResponse.getStatus()==AppConstants.SERVER_RESPONSE_CODE_SUCCESS)
                        {
                            Utils.showErrorMessage(context,generalResponse.getMessage());
                            ((Activity)context).onBackPressed();
                        }
                        else {
                            Utils.showErrorMessage(context,getString(R.string.msg_unknown_error));
                        }
                    }
                }
            });
        }

    }

    public  void saveTemplateAnswers(final boolean onlySave, final DialogCallBackListener callBackListener) {

        if(selectedTemplate==null){
            callBackListener.onOk();
        }

        TemplateToSaveInfo templateToSaveInfo=new TemplateToSaveInfo();

        templateToSaveInfo.setTemplateId(selectedTemplate.getTemplateId());
        templateToSaveInfo.setUserId(((CreateEditTemplateActivity)getActivity()).getUserID());
        templateToSaveInfo.setAnswersList(selectedTemplate.getAnswersList());

        TemplateSaveAnswerHandler.saveTemplateAnswers(context, templateToSaveInfo, new ServerResponseListener() {
            @Override
            public void onServerResponse(GeneralResponse generalResponse) {

                if(callBackListener!=null) {callBackListener.onOk(); return;}

                if(generalResponse!=null&&generalResponse.getStatus()==AppConstants.SERVER_RESPONSE_CODE_SUCCESS)
                {


                    if(onlySave) {
                        gotoMangeScreen();
                    }
                    else {
                        askForSign();
                    }
                }
                else {
                    if(generalResponse!=null)
                    {
                        Utils.showErrorMessage(context,generalResponse.getMessage());
                    }
                  else   Utils.showErrorMessage(context,context.getString(R.string.msg_unknown_error));
                }


            }
        });
    }

    public void askForSign() {

        DialogsUtils.askForSignNowOrLater(context, new SignatureNowOrLaterListener() {
            @Override
            public void signatureNow() {
                showSignatureDialog(REQUEST_CODE_SIGNATURE_FOR_SEND_CONTRACT);
            }

            @Override
            public void signatureLater() {
                inputReceiversInfo(selectedTemplate, null);
            }
        });
    }

    public void inputReceiversInfo(TemplateModel selectedTemplate, Bitmap signature) {

        ContractInfo contractInfo = new ContractInfo();
        contractInfo.setUserId(((BaseActivity)getActivity()).getUserID());
        if (selectedTemplate != null) {
            contractInfo.setTemplateId(selectedTemplate.getTemplateId());
            contractInfo.setReceivers(selectedTemplate.getReceivers());
        } if(signature!=null)
            contractInfo.setUserSignatureBitmap(signature);

        Intent intent = new Intent(context, ReceiversInputActivity.class);
        intent.putExtra(AppConstants.INTENT_KEY_CONTRACT_INFO, contractInfo);
        startActivityForResult(intent, AppConstants.RC_ADD_RECEIVERS);
    }

    public void sendContract(ContractInfo contractInfo) {

        TemplateSendContractHandler.sendContract(context, contractInfo, new ServerResponseListener() {
            @Override
            public void onServerResponse(GeneralResponse generalResponse) {

                if(generalResponse!=null)
                {
                    Utils.showErrorMessage(context, generalResponse.getMessage(), new DialogCallBackListener() {
                        @Override
                        public void onOk() {

                            gotoMangeScreen();
                        }

                        @Override
                        public void onDiscard() {

                        }

                        @Override
                        public void onCancel() {

                        }
                    });


                }
            }
        });

    }

    private void gotoMangeScreen() {

        Intent intent=new Intent(context, ManageActivity.class);
        Utils.putExtraForClearLast(intent);
        intent.putExtra(AppConstants.PUT_EXTRA_KEY_REDRICT_TO,ManageActivity.TAB_INDEX_PENDING);
        startActivity(intent);
        try {
            getActivity().finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if(requestCode== REQUEST_CODE_SIGNATURE_FOR_SEND_CONTRACT &&resultCode== Activity.RESULT_OK) {
//            String signaturePath=  data.getStringExtra(AppConstants.KEY_SIGNATURE);
//            inputReceiversInfo(selectedTemplate,Utils.getBitmapFromPath(signaturePath));
//        } else if (requestCode == AppConstants.RC_ADD_RECEIVERS) {
//            ArrayList<ReceiversInfo> receiversInfo = (ArrayList<ReceiversInfo>) data.getSerializableExtra(AppConstants.INTENT_KEY_RECEIVERS_LIST);
//            ContractInfo contractInfo = (ContractInfo) data.getSerializableExtra(AppConstants.INTENT_KEY_CONTRACT_INFO);
//            if (receiversInfo != null && receiversInfo.size() > 0)
//                sendContract(contractInfo);
//        }
//        else
//            super.onActivityResult(requestCode, resultCode, data);
//
//    }

    @Override
    public void onReceiversAdded(ContractInfo contractInfo) {

        sendContract(contractInfo);

    }

    protected void hideSubmitButton() {

        submitBtn.setVisibility(View.GONE);

    }


    public abstract void refreshReceivers();
}
