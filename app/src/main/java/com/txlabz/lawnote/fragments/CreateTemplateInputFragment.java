package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.natasa.progressviews.CircleProgressBar;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.adapters.TemplateCreateQuestionsAdapter;
import com.txlabz.lawnote.interfaces.OnFocusListener;
import com.txlabz.lawnote.interfaces.ProgressUpdateListener;
import com.txlabz.lawnote.interfaces.SaveCompleteListener;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CreateTemplateInputFragment extends BaseTemplateQuestionsFragment implements ProgressUpdateListener, View.OnClickListener, OnFocusListener {


    private static String TAG="LN_QUES_ADPTR";
    @BindView(R.id.circle_progress)
    CircleProgressBar mCircleProgressBar;
    @BindView(R.id.tv_template_desc)
    TextView tvQuestionDescription;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private TemplateCreateQuestionsAdapter mTemplateQuestionsAdapter;
    private int filledAmount = 0;
    ArrayList<QuestionModel> mQuestionsList;
    private NestedScrollView nestedScrollView;
    private LinearLayoutManager layoutManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    public static CreateTemplateInputFragment newInstance()
    {
        return new CreateTemplateInputFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_template_input, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();

    }

    private void initViews() {

        nestedScrollView= (NestedScrollView) getView().findViewById(R.id.nested_scroll);
        selectedTemplate=((CreateEditTemplateActivity)context).getSelectedTemplate();

        mQuestionsList = selectedTemplate.getQuestionModels();

        tvQuestionDescription.setText(selectedTemplate.getTemplateDescription());
        mCircleProgressBar.setTextSize(70);



        int progress = Utils.getProgress(filledAmount, mQuestionsList.size());
        updateProgress(progress);
        mRecyclerView.setNestedScrollingEnabled(false);
        layoutManager=new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(layoutManager);
        mTemplateQuestionsAdapter = new TemplateCreateQuestionsAdapter(context, mQuestionsList,this,CreateTemplateInputFragment.this);
        mRecyclerView.setAdapter(mTemplateQuestionsAdapter);


        submitBtn= (Button) getView().findViewById(R.id.btnSubmitTemplate);
        submitBtn.setOnClickListener(this);

        updateProgress(0);

        if(mQuestionsList.size()==0){

            hideSubmitButton();
        }
    }



    @Override
    public void updateProgress(int progress) {

        if(mTemplateQuestionsAdapter==null ||mCircleProgressBar==null)
            return;


        Log.d(AppConstants.TAG,"Updating create template progress prog: "+progress);
        mCircleProgressBar.setProgress(progress);
        mCircleProgressBar.setText(String.valueOf(progress)+"%");
    }

    @Override
    public void repopulateData(TemplateModel selectedTemplate) {

        mTemplateQuestionsAdapter.notifyDataSetChanged();
    }

    @Override
    public void saveUserInput(SaveCompleteListener onDataSaveCompleted) {

        onDataSaveCompleted.onDataSavedCompleted(selectedTemplate);
    }

    @Override
    public void refreshReceivers() {

    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {

            case R.id.btnSubmitTemplate:   submitTemplate(); break;
        }

    }

    @Override
    public void onFocusReceived(View parentView) {

        Log.d(TAG,"View in focus "+parentView.getId());

        if(mTemplateQuestionsAdapter!=null&&mTemplateQuestionsAdapter.isFirstItem())
        nestedScrollView.smoothScrollTo(0, parentView.getBottom()+20);
        //nestedScrollView.smoothScrollTo(0, submitBtn.getTop());

    }

    @Override
    public void onFocusLost(View parentView) {

    }

    @Override
    public void onListItemSelected(int index) {

        //Log.d(TAG,"Item Scrolled "+index);
    //    layoutManager.scrollToPositionWithOffset(0, 0);
      //  mRecyclerView.scrollBy(index, 0);
//       mRecyclerView. smoothScrollToPosition(index-2);
    }
}
