package com.txlabz.lawnote.fragments;

import com.txlabz.lawnote.interfaces.SaveCompleteListener;
import com.txlabz.lawnote.modelClasses.TemplateModel;

/**
 * Created by Ali Sabir on 8/29/2017.
 */

public class EmptyTemplateFragment extends BaseTemplateQuestionsFragment {
    @Override
    public void repopulateData(TemplateModel selectedTemplate) {

    }

    @Override
    public void saveUserInput(SaveCompleteListener onDataSaveCompleted) {

        onDataSaveCompleted.onDataSavedCompleted(null);
    }

    @Override
    public void refreshReceivers() {

    }

    public static BaseTemplateQuestionsFragment newInstance() {

        EmptyTemplateFragment emptyTemplateFragment=new EmptyTemplateFragment();
        return emptyTemplateFragment;

    }
}
