package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.txlabz.lawnote.activities.CreateYourOwnActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.adapters.SignatureRecyclerviewAdapter;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;

import java.io.File;


public class PreviewCatalogFragment extends BaseFragment{

    public static String FRAGMENT_TAG = PreviewCatalogFragment.class.getName();
    Context context;
    TextView headingAgreement,content,nameHeading2,emaiHeading2;
    ImageView freelancerSignature,recieverSignature;
    RecyclerView recyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_catalog_preview, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        headingAgreement=(TextView)view.findViewById(R.id.headingAgreement);
        content=(TextView)view.findViewById(R.id.content);
        freelancerSignature=(ImageView)view.findViewById(R.id.freelancerSignature);
        nameHeading2=(TextView)view.findViewById(R.id.nameHeading2);
        emaiHeading2=(TextView)view.findViewById(R.id.emaiHeading2);
        recieverSignature=(ImageView)view.findViewById(R.id.recieverSignature);
        recyclerView = (RecyclerView) view.findViewById(R.id.receiversRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        String title=((CreateYourOwnActivity)getContext()).getTitleOfAgreement();
        String contentText=((CreateYourOwnActivity)getContext()).getContentOfAgreement();
        File freeLancerFile=((CreateYourOwnActivity)getContext()).getGetJpgSign();
        String freelancerName=((CreateYourOwnActivity)getContext()).getUserName();
        String freelancerEmail=((CreateYourOwnActivity)getContext()).getEmail();
        File getFreelancFile=((CreateYourOwnActivity)getContext()).getJpgFreelanceSign();
        String getFreelanceName=((CreateYourOwnActivity)getContext()).getGetFreeLanceName();
        String getFreelanceEmail=((CreateYourOwnActivity)getContext()).getGetFreeLanceEmail();
        String senderSignature=((CreateYourOwnActivity)getContext()).getSenderSignature();
        TemplateModel templateModel = ((CreateYourOwnActivity)getContext()).getSelectedTemplate();
        recyclerView.setNestedScrollingEnabled(false);
        if (templateModel != null) {
            recyclerView.setAdapter(new SignatureRecyclerviewAdapter(context, templateModel.getReceivers()));
        }
        if (title!=null)
        {
            headingAgreement.setText(title);
        }
        if (contentText!=null)
        {
            content.setText(contentText);
        }
        if (freeLancerFile!=null)
        {
            Bitmap myBitmap = BitmapFactory.decodeFile(freeLancerFile.getAbsolutePath());
            freelancerSignature.setImageBitmap(myBitmap);
        }
        if (freelancerName!=null)
        {
            nameHeading2.setText(freelancerName);
        }
        if (freelancerEmail!=null)
        {
            emaiHeading2.setText(freelancerEmail);
        }
        if (getFreelancFile!=null) {
            Bitmap myBitmap = BitmapFactory.decodeFile(getFreelancFile.getAbsolutePath());
            recieverSignature.setImageBitmap(myBitmap);
        }
        if (senderSignature!=null && !senderSignature.isEmpty()) {
            Picasso.with(getContext()).load(senderSignature).into(freelancerSignature);
        }
        
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(AppConstants.TAG,"In on destroy view");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }

}
