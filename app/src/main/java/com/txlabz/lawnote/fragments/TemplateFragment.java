package com.txlabz.lawnote.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.GsonBuilder;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.activities.MainActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.adapters.TemplateAdapter;
import com.txlabz.lawnote.modelClasses.Categories;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AlertDialogBox;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;

public class TemplateFragment extends BaseFragment {

    Context context;
    ArrayList<TemplateModel> arrayList;
    public static String FRAGMENT_TAG = TemplateFragment.class.getName();
    RecyclerView recyclerView;
    String categoryId;
    SwipeRefreshLayout swipeLayout;
    AlertDialogBox alertDialogBox;
    SharedPreferences sharedPreference;
    String userID;
    private String categoryImage;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_freelance, container, false);
        sharedPreference =Utils.getSharedPreference(getActivity());
        init(view);
        return view;
    }


    private void init(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            ((MainActivity) context).setTitle(bundle.getString(AppConstants.NAME));
            ((MainActivity) context).changeAddIcon(R.drawable.search);
            arrayList = (ArrayList<TemplateModel>) bundle.getSerializable("template_array");
            categoryId=bundle.getString(AppConstants.CATEGORY_ID);
            categoryImage = bundle.getString(AppConstants.CATEGORY_IMAGE);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            // Category_Name=bundle.getString(AppConstants.NAME);
        }


        LinearLayoutManager manager = new LinearLayoutManager(context);
        TemplateAdapter adapter = new TemplateAdapter(context, categoryImage, arrayList);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipeLayout);
        alertDialogBox=new AlertDialogBox();
        userID= sharedPreference.getString(AppConstants.PREF_KEY_USER_ID,AppConstants.NOT_FOUND);
        swipeLayout.setColorSchemeResources(R.color.mainPrimaryLight, R.color.colorAccentMain, R.color.mainPrimaryDark);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            swipeLayout.setProgressViewOffset(false, 0, Utils.getToolbarHeight(context));

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperRefreshCall();
            }
        });
    }

    public String getFragmentName()
    {
        return "Template";
    }
    public void searchTemplateName(String Name)
    {
        ArrayList<TemplateModel> templateModelArrayList=new ArrayList<>();

        for (int i=0; i<arrayList.size();i++)

        {
            String getDataTitle=arrayList.get(i).getTemplateTitle().toLowerCase();
            if (getDataTitle.contains(Name.toLowerCase()))
            {
                arrayList.get(i);
                templateModelArrayList.add(arrayList.get(i));
            }
        }
        LinearLayoutManager manager = new LinearLayoutManager(context);
        TemplateAdapter adapter = new TemplateAdapter(context, categoryImage, templateModelArrayList);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    public void swiperRefreshCall()
    {
        swipeLayout.setRefreshing(true);
        if (Utils.IsNetworkConnected(getContext()))
        {

            Ion.with(this)
                    .load(AppConstants.API_GET_TEMPLATES_BY_CATEGORY_ID+"user_id="+userID+"&category_id="+categoryId)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            swipeLayout.setRefreshing(false);

                            if (e==null)
                            {
                                if (result!=null)
                                {
                                    try {

                                        Categories array = new GsonBuilder().create().fromJson(result, Categories.class);
                                       // categoriesArrayList=array.getCategories();

                                        arrayList=array.getTemplateModels();

                                        LinearLayoutManager manager = new LinearLayoutManager(context);
                                        TemplateAdapter adapter = new TemplateAdapter(context, categoryImage, arrayList);
                                        recyclerView.setLayoutManager(manager);
                                        recyclerView.setAdapter(adapter);

                                    }catch (Exception ex)
                                    {
                                        ex.printStackTrace();
                                    }
                                }
                            }else {
                                e.printStackTrace();
                            }
                        }
                    });
        }
        else {
            alertDialogBox.loadAlert(AppConstants.CHECK_CONNECTION,getContext());
        }
    }
}