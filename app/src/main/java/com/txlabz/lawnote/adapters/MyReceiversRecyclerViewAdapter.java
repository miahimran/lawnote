package com.txlabz.lawnote.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.ReceiversInfo;
import java.util.ArrayList;


public class MyReceiversRecyclerViewAdapter extends RecyclerView.Adapter<MyReceiversRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<ReceiversInfo> mValues;

    public MyReceiversRecyclerViewAdapter(ArrayList<ReceiversInfo> items) {
        mValues = items;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_receivers_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

        holder.etFirstName.setText(mValues.get(position).getFirstName());
        holder.etLastName.setText(mValues.get(position).getLastName());
        holder.etEmail.setText(mValues.get(position).getEmail());
        holder.ivCross.setVisibility(View.VISIBLE);
        if (position == 0)
            holder.ivCross.setVisibility(View.GONE);
        holder.ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mValues.size()>1)
                { mValues.remove(position);
                    notifyDataSetChanged();
             //   notifyItemRemoved(position);
                }
            }
        });

        holder.etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mValues.size() > position)
                    mValues.get(position).setFirstName(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        holder.etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mValues.size() > position)
                    mValues.get(position).setLastName(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        holder.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                mValues.get(position).setEmail(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final EditText etFirstName,etLastName,etEmail;
        public final ImageView ivCross;
        public ReceiversInfo mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            etFirstName = (EditText) view.findViewById(R.id.etFirstName);
            etLastName = (EditText) view.findViewById(R.id.etLastName);
            etEmail = (EditText) view.findViewById(R.id.etEmail);
            ivCross = (ImageView) view.findViewById(R.id.ivCross);
        }


    }


}
