package com.txlabz.lawnote.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 3/20/2016.
 */
public class TemplateAdapter extends RecyclerView.Adapter<TemplateAdapter.ViewHolder> {

    private ArrayList<TemplateModel> templatesList = null;

    private LayoutInflater inflater;
    private Context context;
    private String categoryImage;

    public TemplateAdapter(Context context, String categoryImage, ArrayList<TemplateModel> arrayList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.categoryImage = categoryImage;
        this.templatesList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_template_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TemplateModel templateModel = templatesList.get(position);
        holder.title.setText(templateModel.getTemplateTitle());
        holder.desc.setText(templateModel.getTemplateDescription());

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<QuestionModel> questionsList;

                if(templateModel.getQuestionModels()==null||
                        templateModel.getQuestionModels().size()==0){

                    Utils.showErrorMessage(context,context.getString(R.string.msg_no_question_in_this_template));
                    return;
                }
                questionsList = templateModel.getQuestionModels();

                Intent intent=new Intent(context, CreateEditTemplateActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString(AppConstants.TEMPLATE_ID, templateModel.getTemplateId());
                bundle.putString(AppConstants.TEMPLATE_TITLE, templateModel.getTemplateTitle());
                bundle.putString(AppConstants.TEMPLATE_URL, templateModel.getTemplateURL());
                bundle.putString(AppConstants.TEMPLATE_DESCRIPTION, templateModel.getTemplateDescription());
                bundle.putString(AppConstants.ANSWER_COUNT, templateModel.getAnswerCount());
                bundle.putSerializable(AppConstants.QUESTION_ARRAY, questionsList);
                bundle.putString(AppConstants.CATEGORY_IMAGE, categoryImage);
                bundle.putSerializable(AppConstants.SELECTED_TEMPLATE, templateModel);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return templatesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View item;

        TextView desc,title;

        public ViewHolder(View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.item);

            desc=(TextView)itemView.findViewById(R.id.desc);
            title=(TextView)itemView.findViewById(R.id.title);


        }
    }

}