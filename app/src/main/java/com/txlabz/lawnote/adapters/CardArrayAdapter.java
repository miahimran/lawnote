package com.txlabz.lawnote.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.NotificationModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;

/***
 * Created by Fatima Siddique on 7/16/2016.
 */
public class CardArrayAdapter extends ArrayAdapter<NotificationModel> {

    private final Context context;
    ArrayList<NotificationModel> notificationsList;

    public CardArrayAdapter(Context context, ArrayList<NotificationModel> arrayList) {
        super(context, 0, arrayList);
        this.notificationsList = arrayList;
        this.context =context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final NotificationModel notificationModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.home_card_content, parent, false);
        }

        ImageView icIcon= (ImageView) convertView.findViewById(R.id.icIcon);

        Utils.loadImage(context,icIcon,notificationModel.getCategoryIcon());

        TextView tvHome = (TextView) convertView.findViewById(R.id.desc);
        tvHome.setText(notificationModel.getMessage());
//        RelativeLayout layout = (RelativeLayout) convertView.findViewById(R.id.cardContainer);
//        layout.setOnClickListener(clickListener);
        return convertView;
    }

    @Override
    public int getCount() {

        Log.d(AppConstants.TAG,"Card notifications count  "+notificationsList.size());

        return notificationsList.size();
    }
}
