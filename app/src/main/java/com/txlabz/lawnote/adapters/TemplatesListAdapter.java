package com.txlabz.lawnote.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.activities.CreateEditTemplateActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.ViewHolder.TemplateListItemViewHolder;
import com.txlabz.lawnote.fragments.TemplatesListFragment;
import com.txlabz.lawnote.interfaces.CallbackListener;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.StorageUtility;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 10/25/2016.
 */
public class TemplatesListAdapter extends RecyclerView.Adapter<TemplateListItemViewHolder> {

    private final int templatesType;
    Context context;
    ArrayList<TemplateModel> homeList;

    public TemplatesListAdapter(Context context, ArrayList<TemplateModel> draftsArrayList, int templatesType) {
        this.context=context;
        this.templatesType=templatesType;
        this.homeList=draftsArrayList;
    }

    @Override
    public TemplateListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_list_item, null);
        TemplateListItemViewHolder manageTemplateViewHolder = new TemplateListItemViewHolder(v);
        return manageTemplateViewHolder;
    }

    @Override
    public void onBindViewHolder(final TemplateListItemViewHolder holder, final int position) {
        final TemplateModel templateModel = homeList.get(position);
        holder.setData(context, templateModel, templatesType);

        if (templatesType == TemplatesListFragment.TEMPLATES_TYPE_DRAFT) {
            holder.rel_main.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final ArrayList<String> data = new ArrayList<>();
                    data.add(context.getString(R.string.delete));
                    data.add(context.getString(R.string.cancel));
                    DialogsUtils.showSheetDialog(context, false, data, new CallbackListener() {
                        @Override
                        public void callback(String s) {
                            if (s.equals(data.get(0))) {
                                deleteTemplate(context, templateModel.getTemplateId(), holder.getAdapterPosition());
                            }
                        }
                    });
                    return false;
                }
            });
        }
    }

    private void deleteTemplate(final Context context, String templateId, final int position) {
        DialogsUtils.showLoading(context);
        String userID = StorageUtility.getDataFromPreferences(context, AppConstants.PREF_KEY_USER_ID, AppConstants.NOT_FOUND);
        Ion.with(context)
                .load(AppConstants.API_BASE_URL + "user/deleteDraft")
                .setBodyParameter("template_id", templateId)
                .setBodyParameter("user_id", userID)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        DialogsUtils.dismiss();
                        if (e == null) {
                            if (result!=null) {
                                try {
                                    JSONObject jsonObject = new JSONObject(result);
                                    if (jsonObject.getInt(AppConstants.STATUS) == 1) {
                                        homeList.remove(position);
                                        notifyItemRemoved(position);
                                    }
                                    DialogsUtils.showAlert(context, jsonObject.getString(AppConstants.MESSAGE));
                                }catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }else {
                            e.printStackTrace();
                        }
                    }
                });
    }


    @Override
    public int getItemCount() {
        if(homeList==null) return 0;

        return homeList.size();
    }
}
