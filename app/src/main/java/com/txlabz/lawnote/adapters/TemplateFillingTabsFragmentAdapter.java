package com.txlabz.lawnote.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.txlabz.lawnote.fragments.BaseFragment;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class TemplateFillingTabsFragmentAdapter extends FragmentStatePagerAdapter {


    private final BaseFragment[] tabFragments;

    public TemplateFillingTabsFragmentAdapter(FragmentManager fm, BaseFragment[] tabFragments) {
        super(fm);
        this.tabFragments=tabFragments;
    }

    @Override
    public Fragment getItem(int position) {
        return tabFragments[position];
    }

    @Override
    public int getCount() {
        return tabFragments.length;
    }
}
