package com.txlabz.lawnote.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;
import com.txlabz.lawnote.activities.CreateYourOwnActivity;
import com.txlabz.lawnote.activities.MainActivity;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.fragments.TemplateFragment;
import com.txlabz.lawnote.modelClasses.Categories;
import com.txlabz.lawnote.utils.AppConstants;

import java.util.List;

/**
 * Created by Fatima Siddique on 3/20/2016.
 */

public class CatalogAdapter extends RecyclerSwipeAdapter<CatalogAdapter.SimpleViewHolder> {

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        ImageView trash;
        ImageView arrow;
        TextView title,desc;
        ImageView icon;
        RelativeLayout relativeLayout;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            title = (TextView) itemView.findViewById(R.id.title);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            arrow = (ImageView) itemView.findViewById(R.id.arrow);
            desc=(TextView)itemView.findViewById(R.id.desc);
            relativeLayout=(RelativeLayout)itemView.findViewById(R.id.relativeLayout);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }
    }

    private Context mContext;
    private List<Categories> mDataset;

    public CatalogAdapter(Context context, List<Categories> objects) {
        this.mContext = context;
        this.mDataset = objects;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_list_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final Categories item = mDataset.get(position);
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                viewHolder.arrow.setVisibility(View.GONE);
                YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.trash));
            }

            @Override
            public void onClose(SwipeLayout layout) {
                super.onClose(layout);
                viewHolder.arrow.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                super.onStartOpen(layout);
                viewHolder.arrow.setVisibility(View.GONE);
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                super.onStartClose(layout);
                viewHolder.arrow.setVisibility(View.VISIBLE);
            }
        });

        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
                mDataset.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, mDataset.size());
                mItemManger.closeAllItems();
            }
        });

        viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (position==mDataset.size()-1)
                {
                    Intent intent=new Intent(mContext, CreateYourOwnActivity.class);
                    mContext.startActivity(intent);
                }else {
                    Fragment fragment = new TemplateFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.CATEGORY_ID, item.getCategoryId());
                    bundle.putString(AppConstants.NAME, item.getCategoryTitle());
                    bundle.putString(AppConstants.CATEGORY_IMAGE, item.getCategoryImage());
                    bundle.putSerializable("template_array", item.getTemplateModels());
                    fragment.setArguments(bundle);
                    ((MainActivity)mContext).loadFragment(fragment);
                }
            }
        });
        viewHolder.title.setText(item.getCategoryTitle());
        if (item.getCategoryTitle().equals("Create Your Own"))
        {
            Picasso.with(mContext).load(R.drawable.create).into(viewHolder.icon);
        }else {
            if(TextUtils.isEmpty(item.getCategoryImage())) {
                viewHolder.icon.setImageResource(R.drawable.ic_place_holder_category);
            }
           else {
                Picasso.with(mContext).load(item.getCategoryImage()).into(viewHolder.icon);}
        }
        viewHolder.desc.setText(item.getCategoryDescryption());
        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }
}