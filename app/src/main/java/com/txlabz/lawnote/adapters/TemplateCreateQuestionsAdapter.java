package com.txlabz.lawnote.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.txlabz.lawnote.ViewHolder.BaseViewHolderTemplateQuestions;
import com.txlabz.lawnote.ViewHolder.ViewHolderTemplateQuestionCheckBox;
import com.txlabz.lawnote.ViewHolder.ViewHolderTemplateQuestionDateInput;
import com.txlabz.lawnote.ViewHolder.ViewHolderTemplateQuestionEditText;
import com.txlabz.lawnote.ViewHolder.ViewHolderTemplateQuestionEditTextArea;
import com.txlabz.lawnote.ViewHolder.ViewHolderTemplateQuestionRadio;
import com.txlabz.lawnote.ViewHolder.ViewHolderTemplateQuestionSpinner;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.interfaces.AnswerListener;
import com.txlabz.lawnote.interfaces.OnFocusListener;
import com.txlabz.lawnote.interfaces.ProgressUpdateListener;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;

import it.sephiroth.android.library.tooltip.Tooltip;


/**
 * Created by Fatima Siddique on 3/20/2016.
 */
public class TemplateCreateQuestionsAdapter extends RecyclerView.Adapter<BaseViewHolderTemplateQuestions>   {

    public  static final int
            VIEW_TYPE_INPUT_EDIT_TEXT=1,
            VIEW_TYPE_INPUT_EDIT_TEXT_AREA=2,
            VIEW_TYPE_INPUT_DATE_INPUT=3,
            VIEW_TYPE_INPUT_RADIO=4,
            VIEW_TYPE_INPUT_SPINNER=5,
            VIEW_TYPE_INPUT_CHECK_BOX=6;
    private static final int VIEW_TYPE_EMPTY_VIEW = -1;
    private final ProgressUpdateListener progressUpdateListener;
    private final OnFocusListener onFocusListener;

    private ArrayList<QuestionModel> templateQuestionsList = null;
    private LayoutInflater inflater;
    private Context context;
    private int expandedPosition = 0;
    private Tooltip.TooltipView toolTip;


    public TemplateCreateQuestionsAdapter(Context context, ArrayList<QuestionModel> arrayList, ProgressUpdateListener progressUpdateListener,OnFocusListener onFocusListener) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.templateQuestionsList = arrayList;
        this.progressUpdateListener=progressUpdateListener;
        this.onFocusListener=onFocusListener;
    }

    @Override
    public BaseViewHolderTemplateQuestions onCreateViewHolder(ViewGroup parent, int viewType) {


        switch (viewType)
        {
            case VIEW_TYPE_INPUT_EDIT_TEXT:{
                View view = inflater.inflate(R.layout.create_template_item_edittext, parent, false);
                return new ViewHolderTemplateQuestionEditText(view,onFocusListener);

            }
            case VIEW_TYPE_INPUT_EDIT_TEXT_AREA:{

                View view = inflater.inflate(R.layout.create_template_item_edittext_area, parent, false);
                return new ViewHolderTemplateQuestionEditTextArea(view);
            }
            case VIEW_TYPE_INPUT_DATE_INPUT:{

                View view = inflater.inflate(R.layout.create_template_item_date_input, parent, false);
                return new ViewHolderTemplateQuestionDateInput(view);
            }
            case VIEW_TYPE_INPUT_RADIO:{

                View view = inflater.inflate(R.layout.create_template_item_radio, parent, false);
                return new ViewHolderTemplateQuestionRadio(view);
            }
            case VIEW_TYPE_INPUT_SPINNER:{

                View view = inflater.inflate(R.layout.create_template_item_spinner, parent, false);
                return new ViewHolderTemplateQuestionSpinner(view);
            }
            case VIEW_TYPE_INPUT_CHECK_BOX:{

                View view = inflater.inflate(R.layout.create_template_item_checkbox, parent, false);
                return new ViewHolderTemplateQuestionCheckBox(view);
            }
            case VIEW_TYPE_EMPTY_VIEW:{

                View view = inflater.inflate(R.layout.create_template_item_empty_view, parent, false);
                return new ViewHolderTemplateQuestionCheckBox(view);
            }
        }


        return null;

    }




    @Override
    public int getItemViewType(int position) {

        QuestionModel questionModel = templateQuestionsList.get(position);
       return findViewType(questionModel.getQuestionType());
    }

    public  int findViewType(String questionType) {



        int  viewType;
        switch (questionType)
        {
            case AppConstants.QUESTION_TYPE_EDIT_TEXT:viewType= VIEW_TYPE_INPUT_EDIT_TEXT;break;
            case AppConstants.QUESTION_TYPE_EDIT_TEXT_AREA:viewType= VIEW_TYPE_INPUT_EDIT_TEXT_AREA;break;
            case AppConstants.QUESTION_TYPE_DATE_INPUT:viewType= VIEW_TYPE_INPUT_DATE_INPUT;break;
            case AppConstants.QUESTION_TYPE_RADIO:viewType= VIEW_TYPE_INPUT_RADIO;break;
            case AppConstants.QUESTION_TYPE_SPINNER:viewType= VIEW_TYPE_INPUT_SPINNER;break;
            case AppConstants.QUESTION_TYPE_CHECK_BOX:viewType= VIEW_TYPE_INPUT_CHECK_BOX;break;
            default: viewType= VIEW_TYPE_EMPTY_VIEW;

        }

        Log.d(AppConstants.TAG,"Found view type for "+questionType+" "+viewType);
        return viewType;
    }

    @Override
    public void onBindViewHolder(final BaseViewHolderTemplateQuestions mViewHolderItem, final int position) {
        final QuestionModel questionModel = templateQuestionsList.get(position);

        if(mViewHolderItem.mContentContainer==null) return;


        if (position == expandedPosition) {

            mViewHolderItem.mContentContainer.setVisibility(View.VISIBLE);
            mViewHolderItem.mCardView.setSelected(true);
        } else {
            mViewHolderItem.mContentContainer.setVisibility(View.GONE);
            mViewHolderItem.mCardView.setSelected(false);
        }

        mViewHolderItem.setAnswerListener(new AnswerListener() {
            @Override
            public void onAnswerSet(String answer) {
                Log.d(AppConstants.TAG,"Setting answer "+answer+" for position "+position+" question "+questionModel.toString());
                templateQuestionsList.get(position).setAnswer(answer);

                if(progressUpdateListener!=null)
                {
                    progressUpdateListener.updateProgress(calculateTemplateProgress());
                }

            }
        });



        if(mViewHolderItem.ivInfo!=null) {

            mViewHolderItem.ivInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInfo(mViewHolderItem.ivInfo,questionModel.getDescription());
                }
            });

        }

        mViewHolderItem.mTitle.setText(questionModel.getQuestionText());
        String questionType = questionModel.getQuestionType();
        if (TextUtils.isEmpty(questionType))
            return;

        Log.i(AppConstants.TAG, "type is : " + questionType + ", position " + position + ", question : " + questionModel.getQuestionText());



        if(mViewHolderItem instanceof ViewHolderTemplateQuestionEditText) {

            ViewHolderTemplateQuestionEditText viewHolderTemplateQuestionEditText= (ViewHolderTemplateQuestionEditText) mViewHolderItem;

            viewHolderTemplateQuestionEditText.etAnswerInput.setVisibility(View.VISIBLE);
            viewHolderTemplateQuestionEditText. setupEditTextView( questionModel);
        }
        else if(mViewHolderItem instanceof ViewHolderTemplateQuestionEditTextArea){

            ViewHolderTemplateQuestionEditTextArea viewHolderTemplateQuestionEditTextArea= (ViewHolderTemplateQuestionEditTextArea) mViewHolderItem;
            viewHolderTemplateQuestionEditTextArea.mEditAreaContainer.setVisibility(View.VISIBLE);
            viewHolderTemplateQuestionEditTextArea.setupEditAreaView(context, questionModel);
        }
        else if(mViewHolderItem instanceof  ViewHolderTemplateQuestionDateInput){

            ViewHolderTemplateQuestionDateInput viewHolderTemplateQuestionDateInput= (ViewHolderTemplateQuestionDateInput) mViewHolderItem;
            viewHolderTemplateQuestionDateInput.tvPickDate.setVisibility(View.VISIBLE);
            viewHolderTemplateQuestionDateInput. setupDateView(context, questionModel);
        }
        else if(mViewHolderItem instanceof  ViewHolderTemplateQuestionRadio){

            ViewHolderTemplateQuestionRadio viewHolderTemplateQuestionRadio= (ViewHolderTemplateQuestionRadio) mViewHolderItem;
            viewHolderTemplateQuestionRadio.mRadioGroup.setVisibility(View.VISIBLE);
            viewHolderTemplateQuestionRadio. setupRadioView(context, questionModel);

        }
        else if(mViewHolderItem instanceof  ViewHolderTemplateQuestionSpinner){

            ViewHolderTemplateQuestionSpinner viewHolderTemplateQuestionSpinner= (ViewHolderTemplateQuestionSpinner) mViewHolderItem;
            viewHolderTemplateQuestionSpinner.mSpinner.setVisibility(View.VISIBLE);
            viewHolderTemplateQuestionSpinner.setupSpinnerOptions(context,questionModel);
        }
        else if(mViewHolderItem instanceof  ViewHolderTemplateQuestionCheckBox){

            ViewHolderTemplateQuestionCheckBox viewHolderTemplateQuestionCheckBox= (ViewHolderTemplateQuestionCheckBox) mViewHolderItem;
            viewHolderTemplateQuestionCheckBox.lytCheckboxHolder.setVisibility(View.VISIBLE);
            viewHolderTemplateQuestionCheckBox.setupCheckOptions(context,questionModel);
        }


        mViewHolderItem.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandedPosition == mViewHolderItem.getAdapterPosition())
                    return;

                boolean isCollapsing = false;
                if (expandedPosition >= 0) {
                    int prev = expandedPosition;
                    if(mViewHolderItem.getPosition() == expandedPosition) {
                        expandedPosition = -1;
                        isCollapsing = true;
                    }
                    notifyItemChanged(prev);
                }
                if(!isCollapsing) {
                    expandedPosition = mViewHolderItem.getAdapterPosition();

                    if(mViewHolderItem.getItemViewType()!=VIEW_TYPE_INPUT_EDIT_TEXT){

                        Utils.hideKeyboard((Activity) context);
                      //  mViewHolderItem.ivInfo.requestFocus();
                    }
                    else {
                        try {
                            ViewHolderTemplateQuestionEditText editTextHolder= (ViewHolderTemplateQuestionEditText) mViewHolderItem;
                            if(editTextHolder!=null){
                                editTextHolder.clearFocus();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    notifyItemChanged(expandedPosition);
                    if(onFocusListener!=null){
                        onFocusListener.onListItemSelected(position);
                    }
                }
            }
        });
    }


    private void showInfo(ImageView ivInfo, String description) {

        if(toolTip!=null && toolTip.isShown()) {
            toolTip.hide();
            toolTip.remove();
            return;
        }
        if (TextUtils.isEmpty(description))
            return;

        toolTip = Tooltip.make(context, new Tooltip.Builder(101)
                .anchor(ivInfo, Tooltip.Gravity.BOTTOM)
                .activateDelay(800)
                .showDelay(200)
                .text(description)
                .maxWidth(560)
                .withArrow(true)
                .withOverlay(true)
                .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
                .build()
        );

        toolTip.show();
        toolTip.setTextColor(Color.WHITE);
    }


    @Override
    public int getItemCount() {
        return templateQuestionsList.size();
    }


    private int calculateTemplateProgress() {

        int filledCount=0;
        for(int i=0;i<templateQuestionsList.size();i++)
        {
            if(!TextUtils.isEmpty(templateQuestionsList.get(i).getAnswer()))
                filledCount++;
        }


        int  totalQuestions=getValidQuestionsCount(templateQuestionsList);
        if(totalQuestions>0&&filledCount>0)
        {
            return (filledCount*100)/totalQuestions;
        }
        return 0;
    }

    private int getValidQuestionsCount(ArrayList<QuestionModel> questionList) {

        int validCount=0;
        for(int i=0;i<questionList.size();i++){


            if(findViewType(questionList.get(i).getQuestionType())>0){
                validCount++;

            }
        }

        return validCount;
    }

    public boolean isFirstItem() {
        return expandedPosition==0;
    }
}