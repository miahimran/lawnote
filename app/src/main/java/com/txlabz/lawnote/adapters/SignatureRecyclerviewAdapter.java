package com.txlabz.lawnote.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.ReceiversInfo;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.StorageUtility;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;

import static android.view.View.GONE;

/**
 * Created by Ali Sabir on 8/29/2017.
 */

public class SignatureRecyclerviewAdapter extends RecyclerView.Adapter<SignatureRecyclerviewAdapter.ViewHolderSignature> {

    private ArrayList<ReceiversInfo> mValues;

    private Context context;

    public SignatureRecyclerviewAdapter(Context context, ArrayList<ReceiversInfo> mValues) {
        this.context=context;
        this.mValues = mValues;


    }

    @Override
    public SignatureRecyclerviewAdapter.ViewHolderSignature onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.signature_item, parent, false);
        return new SignatureRecyclerviewAdapter.ViewHolderSignature(view);
    }

    @Override
    public void onBindViewHolder(final SignatureRecyclerviewAdapter.ViewHolderSignature holder, final int position) {
        holder.signatureInfo = mValues.get(position);

        if(holder.signatureInfo.getSignatureBitmap()!=null) {
            holder.ivSignature.setImageBitmap(holder.signatureInfo.getSignatureBitmap());
        } else  if(!TextUtils.isEmpty(holder.signatureInfo.getSignatureImage())) {
            Utils.loadImage(context ,holder.ivSignature,holder.signatureInfo.getSignatureImage());
        } else {
            holder.ivSignature.setImageResource(R.drawable.ic_no_sign);
        }

        holder.tvEamil.setText(holder.signatureInfo.getEmail());
        holder.tvName.setText(holder.signatureInfo.getFullName());

        if(position<mValues.size()-1){
            holder.vDevider.setVisibility(View.VISIBLE);
        }
        else {holder.vDevider.setVisibility(View.GONE);}

    }

    @Override
    public int getItemCount() {
        return mValues == null ? 0 : mValues.size();
    }

    class ViewHolderSignature extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvName,tvEamil;
        public final ImageView ivSignature;
        public ReceiversInfo signatureInfo;
        public View vDevider;

        public ViewHolderSignature(View view) {
            super(view);
            mView = view;

            tvEamil= (TextView) view.findViewById(R.id.tvEmail);
            tvName= (TextView) view.findViewById(R.id.tvName);
            ivSignature= (ImageView) view.findViewById(R.id.ivSignature);
            vDevider=  view.findViewById(R.id.vDevider);
        }
    }
}
