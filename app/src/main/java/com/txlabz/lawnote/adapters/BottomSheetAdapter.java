package com.txlabz.lawnote.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.BottomDialogModel;

import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/31/2017.
 */

public class BottomSheetAdapter extends BaseAdapter {
    ArrayList<String> data;
    private LayoutInflater layoutInflater;

    public BottomSheetAdapter(Context context, ArrayList<String> data) {
        layoutInflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;

        if (view == null) {

            view = layoutInflater.inflate(R.layout.layout_bottom_sheet, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.txtTitle = (TextView) view.findViewById(R.id.tvBottomSheet);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.txtTitle.setText(data.get(position));
        return view;
    }

    static class ViewHolder {
        TextView txtTitle;
    }
}
