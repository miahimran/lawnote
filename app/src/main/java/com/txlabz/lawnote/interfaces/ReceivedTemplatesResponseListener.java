package com.txlabz.lawnote.interfaces;

import com.txlabz.lawnote.modelClasses.server_responses.ReceivedTemplates;

/**
 * Created by Ali Sabir on 8/29/2017.
 */

public interface ReceivedTemplatesResponseListener {


    void onServerResponse(ReceivedTemplates receivedTemplates);
}
