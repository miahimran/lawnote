package com.txlabz.lawnote.interfaces;

/**
 * Created by Ali Sabir on 8/31/2017.
 */

public interface CallbackListener {
    void callback(String s);
}
