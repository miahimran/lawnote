package com.txlabz.lawnote.interfaces;

/**
 * Created by Ali Sabir on 9/7/2017.
 */

public interface DialogCallBackListener {

    void onOk();
    void onDiscard();
    void onCancel();
}
