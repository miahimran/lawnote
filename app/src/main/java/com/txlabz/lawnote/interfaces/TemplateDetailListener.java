package com.txlabz.lawnote.interfaces;

import com.txlabz.lawnote.modelClasses.TemplateModel;

/**
 * Created by Ali Sabir on 9/7/2017.
 */

public interface TemplateDetailListener {

   void  onTemplateDetailReceived(TemplateModel templateModel);
}
