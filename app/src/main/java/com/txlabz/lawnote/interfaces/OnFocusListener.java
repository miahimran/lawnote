package com.txlabz.lawnote.interfaces;

import android.view.View;

/**
 * Created by Ali Sabir on 9/25/2017.
 */

public interface OnFocusListener {

   void onFocusReceived(View parentView);
    void onFocusLost(View parentView);

    void onListItemSelected(int index);
}
