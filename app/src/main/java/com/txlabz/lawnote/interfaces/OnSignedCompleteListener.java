package com.txlabz.lawnote.interfaces;

import android.graphics.Bitmap;

/**
 * Created by Ali Sabir on 8/24/2017.
 */

public interface OnSignedCompleteListener {

  void   onSignedCompleted(Bitmap signature);
}
