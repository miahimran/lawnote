package com.txlabz.lawnote.interfaces;

/**
 * Created by Ali Sabir on 8/29/2017.
 */

public interface PermissionListener {

   void  onPermissionGranted(boolean granted);
}
