package com.txlabz.lawnote.interfaces;

import com.txlabz.lawnote.modelClasses.ContractInfo;
import com.txlabz.lawnote.modelClasses.ReceiversInfo;

import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/30/2017.
 */

public interface ReceiversAddCompleteListener {

    void onReceiversAdded(ContractInfo addedReceivers);
}
