package com.txlabz.lawnote.interfaces;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public interface ProgressUpdateListener {

    void updateProgress(int progress);
}
