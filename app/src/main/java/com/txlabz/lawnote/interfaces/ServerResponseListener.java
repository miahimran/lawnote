package com.txlabz.lawnote.interfaces;

import com.txlabz.lawnote.modelClasses.server_responses.GeneralResponse;

/**
 * Created by Ali Sabir on 8/25/2017.
 */

public interface ServerResponseListener {

  void  onServerResponse(GeneralResponse generalResponse);
}
