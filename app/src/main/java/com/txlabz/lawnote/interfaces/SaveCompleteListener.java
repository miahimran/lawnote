package com.txlabz.lawnote.interfaces;

import com.txlabz.lawnote.modelClasses.TemplateModel;

/**
 * Created by Ali Sabir on 8/28/2017.
 */

public interface SaveCompleteListener {

    void onDataSavedCompleted(TemplateModel selectedTemplate);
}
