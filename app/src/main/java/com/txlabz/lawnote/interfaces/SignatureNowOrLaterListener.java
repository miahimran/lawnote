package com.txlabz.lawnote.interfaces;

/**
 * Created by Ali Sabir on 8/24/2017.
 */

public interface SignatureNowOrLaterListener {

    void signatureNow();
    void signatureLater();
}
