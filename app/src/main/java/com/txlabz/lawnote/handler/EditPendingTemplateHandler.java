package com.txlabz.lawnote.handler;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.interfaces.ServerResponseListener;
import com.txlabz.lawnote.modelClasses.server_responses.GeneralResponse;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

/**
 * Created by Ali Sabir on 8/29/2017.
 */

public class EditPendingTemplateHandler {

    public static  void sendEditTemplateRequest(final Context context, String templateId,String userId, boolean isCreatedByUser, final ServerResponseListener responseListener) {

        final Gson gson=new Gson();

        Utils.showProgressDialog(context);

        String url = AppConstants.EDIT_PENDING_TEMPLATE_URL;
        if (isCreatedByUser)
            url = AppConstants.SAVE_OWN_TEMPLATE;

        Log.d(AppConstants.TAG," "+url+" \n ");

        Ion.with(context)
                .load(url)
                .setMultipartParameter(AppConstants.SERVER_KEY_USER_ID,userId)
                .setMultipartParameter(AppConstants.SERVER_KEY_TEMPLATE_ID,templateId)

                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Utils.hideProgressDialog();




                        if (e == null) {
                            try {

                                Log.d(AppConstants.TAG,"Result "+result);

                                GeneralResponse generalResponse=gson.fromJson(result,GeneralResponse.class);

                                //    Utils.showErrorMessage(context,generalResponse.getMessage());
                                responseListener.onServerResponse(generalResponse);
                                return;

                            } catch (Exception e1) {
                                e1.printStackTrace();
                                responseListener.onServerResponse(null);
                                return;
                            }
                        }

                        responseListener.onServerResponse(null);
                    }
                });

    }

}
