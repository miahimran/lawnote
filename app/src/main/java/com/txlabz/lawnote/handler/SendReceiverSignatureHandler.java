package com.txlabz.lawnote.handler;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.interfaces.ServerResponseListener;
import com.txlabz.lawnote.modelClasses.server_responses.GeneralResponse;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.io.File;

/**
 * Created by Ali Sabir on 8/30/2017.
 */

public class SendReceiverSignatureHandler {

    public static void sendReceiverSignature(Context context, String userId, String filePath, final ServerResponseListener serverResponseListener)
    {
        final Gson gson=new Gson();
        Utils.showProgressDialog(context);

        String url= AppConstants.SEND_RECEIVER_SIGNATURE;


        Log.d(AppConstants.TAG," "+url+" \n ");

        File sign=new File(filePath);

        Ion.with(context)
                .load(url)
                .setMultipartParameter(AppConstants.SERVER_KEY_TEMPLATE_ID,userId)
                .setMultipartFile(AppConstants.SERVER_KEY_RECEIVER_SIGN,sign)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Utils.hideProgressDialog();

                        if (e == null) {
                            try {

                                Log.d(AppConstants.TAG,"Result: "+result);

                                GeneralResponse response=gson.fromJson(result,GeneralResponse.class);
                                //  Utils.showErrorMessage(context,response.getMessage());
                                serverResponseListener.onServerResponse(response);
                                return;

                            } catch (Exception e1) {
                                e1.printStackTrace();
                                serverResponseListener.onServerResponse(null);
                                return;
                            }
                        }

                        serverResponseListener.onServerResponse(null);
                    }


                });
    }
    }

