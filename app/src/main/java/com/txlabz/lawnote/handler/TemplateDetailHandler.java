package com.txlabz.lawnote.handler;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.interfaces.TemplateDetailListener;
import com.txlabz.lawnote.modelClasses.server_responses.TemplateDetailResponse;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

/**
 * Created by Ali Sabir on 9/7/2017.
 */

public class TemplateDetailHandler {

    public static  void getTemplateDetail(final Context context, String userId, String templateId,final TemplateDetailListener templateDetailListener) {



        Utils.showProgressDialog(context);

        String url= AppConstants.TEMPLATES_DETAIL_URL;

        Log.d(AppConstants.TAG," "+url+" \n ");

        Ion.with(context)
                .load(url)
                .setMultipartParameter(AppConstants.SERVER_KEY_USER_ID,userId)
                .setMultipartParameter(AppConstants.SERVER_KEY_TEMPLATE_ID,templateId)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Utils.hideProgressDialog();




                        if (e == null) {
                            try {

                                Log.d(AppConstants.TAG,"Result "+result);

                                Gson gson=new Gson();


                                TemplateDetailResponse templateDetailResponse=gson.fromJson(result,TemplateDetailResponse.class);

                                //    Utils.showErrorMessage(context,generalResponse.getMessage());

                                if(templateDetailResponse.getStatus()==AppConstants.SERVER_RESPONSE_CODE_SUCCESS){
                                templateDetailListener.onTemplateDetailReceived(templateDetailResponse.getTemplate());}
                                else{
                                    templateDetailListener.onTemplateDetailReceived(null);
                                }
                                return;

                            } catch (Exception e1) {
                                e1.printStackTrace();
                                templateDetailListener.onTemplateDetailReceived(null);
                                return;
                            }
                        }

                        templateDetailListener.onTemplateDetailReceived(null);
                    }
                });

    }
}
