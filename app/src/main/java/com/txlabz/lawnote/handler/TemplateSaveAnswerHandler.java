package com.txlabz.lawnote.handler;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.interfaces.ServerResponseListener;
import com.txlabz.lawnote.modelClasses.TemplateToSaveInfo;
import com.txlabz.lawnote.modelClasses.server_responses.GeneralResponse;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;


/**
 * Created by Ali Sabir on 8/25/2017.
 */

public class TemplateSaveAnswerHandler {

    public static  void saveTemplateAnswers(final Context context, TemplateToSaveInfo templateToSaveInfo, final ServerResponseListener responseListener) {

            final Gson gson=new Gson();

            Utils.showProgressDialog(context);

            String url=AppConstants.SAVE_TEMPLATE_ANSWERS_URL;
        String jsonToSubmit=gson.toJson(templateToSaveInfo);

        Log.d(AppConstants.TAG," "+url+" \n "+jsonToSubmit);

            Ion.with(context)
                    .load(url)
                    .setStringBody(jsonToSubmit)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            Utils.hideProgressDialog();




                            if (e == null) {
                                try {

                                    Log.d(AppConstants.TAG,"Result "+result);

                                    GeneralResponse generalResponse=gson.fromJson(result,GeneralResponse.class);

                                //    Utils.showErrorMessage(context,generalResponse.getMessage());
                                    responseListener.onServerResponse(generalResponse);
                                    return;

                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                    responseListener.onServerResponse(null);
                                    return;
                                }
                            }

                            responseListener.onServerResponse(null);
                        }
                    });

    }

}
