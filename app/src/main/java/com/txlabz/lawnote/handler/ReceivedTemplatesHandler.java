package com.txlabz.lawnote.handler;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.interfaces.ReceivedTemplatesResponseListener;
import com.txlabz.lawnote.modelClasses.server_responses.ReceivedTemplates;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

/**
 * Created by Ali Sabir on 8/29/2017.
 */

public class ReceivedTemplatesHandler {

    public static  void getReceivedTemplates(final Context context, String userId, final ReceivedTemplatesResponseListener responseListener) {

        final Gson gson=new Gson();

        Utils.showProgressDialog(context);

        String url= AppConstants.RECEIVED_TEMPLATES_URL+"?"+AppConstants.SERVER_KEY_USER_ID+"="+userId;

        Log.d(AppConstants.TAG," "+url+" \n ");

        Ion.with(context)
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Utils.hideProgressDialog();




                        if (e == null) {
                            try {

                                Log.d(AppConstants.TAG,"Result "+result);

                                ReceivedTemplates receivedTemplates=gson.fromJson(result,ReceivedTemplates.class);

                                //    Utils.showErrorMessage(context,generalResponse.getMessage());
                                responseListener.onServerResponse(receivedTemplates);
                                return;

                            } catch (Exception e1) {
                                e1.printStackTrace();
                                responseListener.onServerResponse(null);
                                return;
                            }
                        }

                        responseListener.onServerResponse(null);
                    }
                });

    }
}
