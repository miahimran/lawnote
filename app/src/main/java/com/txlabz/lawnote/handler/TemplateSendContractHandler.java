package com.txlabz.lawnote.handler;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.interfaces.ServerResponseListener;
import com.txlabz.lawnote.modelClasses.ContractInfo;
import com.txlabz.lawnote.modelClasses.server_responses.GeneralResponse;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;


/**
 * Created by Ali Sabir on 8/25/2017.
 */

public class TemplateSendContractHandler {

    public static  void sendContract(final Context context, ContractInfo contractInfo, final ServerResponseListener serverResponseListener) {

        final Gson gson=new Gson();


        Utils.showProgressDialog(context);

        String url=AppConstants.SEND_CONTRACT_URL;
        final String requestString=gson.toJson(contractInfo);

        Log.d(AppConstants.TAG," "+url+" \n "+requestString);


        Ion.with(context)
                .load(url)
                .setStringBody(requestString)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Utils.hideProgressDialog();

                        if (e == null) {
                            try {

                                Log.d(AppConstants.TAG,"Result: "+result);

                                GeneralResponse response=gson.fromJson(result,GeneralResponse.class);
                                //  Utils.showErrorMessage(context,response.getMessage());
                                serverResponseListener.onServerResponse(response);
                                return;

                            } catch (Exception e1) {
                                e1.printStackTrace();
                                serverResponseListener.onServerResponse(null);
                                return;
                            }
                        }

                        serverResponseListener.onServerResponse(null);
                    }


                });
    }


}
