package com.txlabz.lawnote.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.adapters.BottomSheetAdapter;
import com.txlabz.lawnote.interfaces.CallbackListener;
import com.txlabz.lawnote.interfaces.DialogCallBackListener;
import com.txlabz.lawnote.interfaces.OnSignedCompleteListener;
import com.txlabz.lawnote.interfaces.PermissionListener;
import com.txlabz.lawnote.interfaces.SignatureNowOrLaterListener;

import java.util.ArrayList;


/**
 * Created by fatima on 12/22/2016.
 */

public class DialogsUtils {
    private static ProgressDialog progressDialog;
    private static AlertDialog alert;
    public static void showLoading(Context context) {
        showLoading(context, context.getString(R.string.loading));
    }

    public static void showLoading(Context context, String message) {
        if(progressDialog != null)
            progressDialog.dismiss();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.setCancelable(false);
        progressDialog.show();
}

    public static void dismiss() {
        if(progressDialog!=null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if(alert != null) {
            try {
                alert.dismiss();
            }catch (Exception e){}
         }
    }

    public static boolean isShowingLoading() {
        return progressDialog != null && progressDialog.isShowing();
    }


    public static  void showEditWarningDialog(Context context, final DialogCallBackListener dialogCallBackListener) {

        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.edit_warning_alert_view);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        TextView cancelBtn=(TextView)dialog.findViewById(R.id.cancelBtn);
        TextView btnEdit=(TextView)dialog.findViewById(R.id.btnEdit);



        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialogCallBackListener.onCancel();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogCallBackListener.onOk();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static  void showConfirmationDialog(Context context, final DialogCallBackListener dialogCallBackListener) {

        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.save_alert_view);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        TextView cancelBtn=(TextView)dialog.findViewById(R.id.cancelBtn);
        TextView saveBtn=(TextView)dialog.findViewById(R.id.saveBtn);
        TextView discardBtn=(TextView)dialog.findViewById(R.id.discardBtn);

        discardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
                dialog.dismiss();
                dialogCallBackListener.onDiscard();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialogCallBackListener.onCancel();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialogCallBackListener.onOk();
            }
        });

        dialog.show();
    }


    public static void showAlert(Context context, String message, final DialogCallBackListener callbackListener){

        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.simple_alert_view);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        TextView tvOk =(TextView)dialog.findViewById(R.id.btnOk);
        TextView tvMessage =(TextView)dialog.findViewById(R.id.tvMessage);

        tvMessage.setText(message);


        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
                dialog.dismiss();
                if(callbackListener!=null){
                    callbackListener.onOk();
                }

            }
        });


        dialog.show();

    }

    public static void showAlert(Context context,String message) {

      showAlert(context,message,null);
    }


    public static void askForSignNowOrLater(Context context, final SignatureNowOrLaterListener signatureNowOrLaterListener) {

        final ArrayList<String> data = new ArrayList<>();
        data.add(context.getString(R.string.sign_now_and_send));
        data.add(context.getString(R.string.sign_now_and_send_later));
        data.add(context.getString(R.string.cancel));
        DialogsUtils.showSheetDialog(context, true, data, new CallbackListener() {
            @Override
            public void callback(String s) {
                if (s.equals(data.get(0)))
                    signatureNowOrLaterListener.signatureNow();
                else if (s.equals(data.get(1)))
                    signatureNowOrLaterListener.signatureLater();
                DialogsUtils.dismiss();
            }
        });
    }





    public static void showSheetDialog(final Context context, boolean showTitle, final ArrayList<String> data, final CallbackListener callbackListener) {
        View view = ((Activity)context).getLayoutInflater().inflate(R.layout.layout_dialog_bottom, null);
        ListView recyclerView = (ListView) view.findViewById(R.id.recyclerView);
        View titleView = view.findViewById(R.id.bottom_sheet_title);
        BottomSheetAdapter adapter = new BottomSheetAdapter(context, data);
        recyclerView.setAdapter(adapter);
        if (showTitle)
            titleView.setVisibility(View.VISIBLE);
        else
            titleView.setVisibility(View.GONE);

        final BottomSheetDialog dialog = new BottomSheetDialog(context);
        dialog.setContentView(view);
        dialog.show();
        recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                callbackListener.callback(data.get(position));
            }
        });

    }


}
