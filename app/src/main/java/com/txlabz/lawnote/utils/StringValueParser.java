package com.txlabz.lawnote.utils;

import android.text.TextUtils;

/**
 * Created by Ali Sabir on 9/8/2017.
 */

public class StringValueParser {
    public static int parseInt(String stringValue) {

        if(stringValue==null||TextUtils.isEmpty(stringValue.trim()))
            return 0;

        return Integer.parseInt(stringValue);
    }
}
