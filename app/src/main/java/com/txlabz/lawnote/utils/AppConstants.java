package com.txlabz.lawnote.utils;

/**
 * Created by Fatima Siddique on 7/15/2016.
 */
public class AppConstants {

    public static final String TAG  = "LN";
    public static final String API_BASE_URL = "http://api.lawnote.co.uk/";
    public static final String GET_HOME_CONTENT = API_BASE_URL + "user/getHomeContent";
    public static final String CREATE_OWN_TEMPLATE =API_BASE_URL + "user/createOwnTemplete";
    public static final String SAVE_OWN_TEMPLATE =API_BASE_URL + "user/saveOwnTemplete11";
    public static final String SEND_CONTRACT_URL =API_BASE_URL + "user/sendContract";
    public static final String SAVE_TEMPLATE_ANSWERS_URL =API_BASE_URL+"user/saveAnswers";
    public static final String NAME = "name";
    public static final String PERSONAL = "PERSONAL";
    public static final String FREELANCE = "FREELANCE";
    public static final String DIGITAL = "DIGITAL";
    public static final String CATALOG = "CATALOG";
    public static final String CHECK_CONNECTION = "Check your Internet Connection";
    public static final String CHECK_VALID_EMAIL = "Enter Valid Email Address";
    public static final String LOADING = "Loading...";
    public static final String FIRST_NAME = "Please enter First Name";
    public static final String LAST_NAME = "Please enter Last Name";
    public static final String ENTER_EMAIL = "Please enter valid Email";
    public static final String ENTER_PASSWORD = "Please enter password";
    public static final String MESSAGE = "message";
    public static final String STATUS = "status";
    public static final String CATEGORIES = "categories";
    public static final String CATEGORY_TITLE = "category_title";
    public static final String CATEGORY_DESCRIPTION = "category_description";
    public static final String TERMS_CONDITION = "You must agree with our Terms and Conditions";
    public static final String SUCCESSFUL_MESSAGE = "Successfully Register !!!";
    public static final String EMAIL_NOT_EXIST = "This email is not associated";
    public static final String TEMPLATES="templates";
    public static final String TEMPLATE_TITLE="template_title";
    public static final String TEMPLATE_ID="template_id";
    public static final String TEMPLATE_DESCRIPTION="template_description";
    public static final String IS_LOGIN="login";
    public static final String QUESTION_ARRAY="question_array";
    public static final String TEMPLATE_URL="template_url";

    public static final String PREF_KEY_USER_ID ="user_id";
    public static final String PREF_KEY_FIRST_NAME ="first_name";
    public static final String PREF_KEY_LAST_NAME ="last_name";
    public static final String PREF_KEY_EMAIL ="email";
    public static final String PREF_KEY_PASSWORD ="password";
    public static final String PREF_KEY_CREATED_DATE ="created_date";
    public static final String USER_DATA="user_data";
    public static final String PREF_KEY_PROFILE_IMAGE ="profile_image";
    public static final String NOT_FOUND="not_found";
    public static final String PROFILE_UPDATED="Profile Image Updated";
    public static final String ANSWER_ARRAY="answers_array";
    public static final String ANSWER_COUNT="answers_count";
    public static final String PENDING_TEMPLATE="pending_template";
    public static final String DRAFTS_TEMPLATE="drafts_template";
    public static final String COMPLETED_TEMPLATE="completed_template";
    public static final String CATEGORY_ID="category_id";




    public static final String RECIEVER_FIRSTNAME="reciever_first_name";
    public static final String RECIEVER_LASTNAME="reciever_lastt_name";
    public static final String RECIEVER_EMAIL="receiver_email";
    public static final String SENDER_SIGNATURE="sender_signature_image";
    public static final String RECIEVER_SIGNATURE="receiver_signature_image";

    public static final String IS_CREATED_BY_USER="is_created_by_user";
    public static final String IS_FROM_PENDING ="is_from_profile";
    public static final int RC_ADD_EDIT_DESCRIPTIOM = 144;
    public static final String QUESTION_TEXT = "question_text";
    public static final String QUESTION_DESC = "question_desc";
    public static final String POSITION = "position";


    public static final String
            QUESTION_TYPE_EDIT_TEXT="editText",
            QUESTION_TYPE_EDIT_TEXT_AREA="editTextArea",
            QUESTION_TYPE_DATE_INPUT="date",
            QUESTION_TYPE_RADIO="radio",
            QUESTION_TYPE_SPINNER="spinner",
            QUESTION_TYPE_CHECK_BOX="checkbox";
    public static final String SELECTED_TEMPLATE = "selectedTemplate";
    public static final int SERVER_RESPONSE_CODE_SUCCESS = 1;
    public static final int SERVER_RESPONSE_CODE_ERROR = 0;
    public static final String KEY_SIGNATURE = "signatureBitmap";
    public static final String API_LOGIN_URL = API_BASE_URL+"user/login";
    public static final String API_REGISTER_URL = API_BASE_URL+"user/signup";
    public static final String EDIT_PENDING_TEMPLATE_URL = API_BASE_URL+"user/editPendingTemplate";
    public static final String SERVER_KEY_USER_ID = "user_id";
    public static final String SERVER_KEY_TEMPLATE_ID = "template_id";
    public static final String RECEIVED_TEMPLATES_URL = API_BASE_URL+"user/getReceivedTemplates";
    public static final String RECEIVED = "RECEIVED";
    public static final String SEND_RECEIVER_SIGNATURE = API_BASE_URL+"user/saveReceiverSignature";
    public static final String SERVER_KEY_RECEIVER_SIGN = "receiver_signature_image";
    public static final int NOTIFICATION_TYPE_SIGNED_BY_OTHER_PENDING = 2;
    public static final int NOTIFICATION_TYPE_COMPLETED = 4;
    public static final int NOTIFICATION_TYPE_RECEIVED = 1;
    public static final int REQUEST_CODE_SIGNATURE_FOR_OWN_CONTRACT = 112;
    public static final String INTENT_KEY_CONTRACT_INFO = "contract_info";
    public static final int RC_ADD_RECEIVERS = 112;
    public static final String INTENT_KEY_RECEIVERS_LIST = "receiver";
    public static final String TEMPLATES_DETAIL_URL = API_BASE_URL+"user/getTemplateDetail";
    public static final String PUT_EXTRA_KEY_USER_ID = "userId";
    public static final String PUT_EXTRA_KEY_TEMPLATE_ID = "templateId";
    public static final String PUT_EXTRA_KEY_USER_EMAIL = "userEmail";
    public static final String PUT_EXTRA_KEY_USER_NAME = "userName";
    public static final String PUT_EXTRA_KEY_TEMPLATE_TITLE = "templateTitle";
    public static final String PUT_EXTRA_KEY_HTML_PAGE_TYPE = "htmlPageType";
    public static final int NOTIFICATION_TYPE_SIGNED_BY_SENDER = 3;
    public static final String PUT_EXTRA_KEY_OPENED_FROM = "openedFrom";
    public static final String PUT_EXTRA_KEY_REDRICT_TO = "redrictTo";
    public static final String INTENT_KEY_SHOULD_SEND_CALL = "should_send_call";
    public static final int GET_READ_PERMISSION_RC = 111;
    public static final String CATEGORY_IMAGE = "category_image";
    public static final String API_FORGOT_PASSWORD = API_BASE_URL+"user/forgotPassword";
    public static final String API_GET_PENDING_TEMPLATES = API_BASE_URL+"user/getPendingTemplate";//http://lawnotes.witsapplication.com/user/getPendingTemplate
    public static final String API_SAVE_RECEIVER_SIGNATURE = API_BASE_URL+"user/saveReceiverSignature";
    public static final String API_SAVE_ANSWERS = API_BASE_URL+"user/saveAnswers";
    public static final String API_GET_MANAGED_TEMLATES = API_BASE_URL+"user/getManagedTemplates?user_id=";
    public static final String API_EDIT_PROFILE = API_BASE_URL+"user/editProfile";
    public static final String API_GET_TEMPLATES_BY_CATEGORY_ID = API_BASE_URL+"user/get_templates_by_category_id?";
    public static final String API_GET_HOME_CONTENT = API_BASE_URL+"user/getHomeContent?";

    public enum QuestionType {
        EDIT_TEXT("editText"),
        DATE("date"),
        RADIO("radio"),
        CHECK_BOX("checkbox"),
        SPINNER("spinner"),
        EDIT_TEXT_AREA("editTextArea");
        final String  value;

        QuestionType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
