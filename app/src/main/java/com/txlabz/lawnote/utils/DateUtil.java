package com.txlabz.lawnote.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Ali Sabir on 5/30/2017.
 */

public class DateUtil {


    public static final String DATE_FORMAT_LOCAL = "dd-MM-yyyy";
    public static final java.lang.String DATE_FORMAT_SERVER = "yyyy-MM-dd";

    public static String formatLocalDateFormat(int year, int month, int day) {

        String dateFormater = DATE_FORMAT_LOCAL;
        return formatDate(year, month, day, dateFormater);
    }

    public static String formatLocalDateFormat(Calendar calendar) {

        String dateFormater =DATE_FORMAT_LOCAL;
        return formatDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), dateFormater);
    }

    public static String formatServerDateFormat(int year, int month, int day) {

        String dateFormater = DATE_FORMAT_SERVER;
        return formatDate(year, month, day, dateFormater);
    }

    public static String formatServerDateFormat(Calendar calendar) {

        String dateFormater =DATE_FORMAT_SERVER;
        return formatDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), dateFormater);
    }



    public static Calendar parserLocalDate(String dateStr)
    {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat dateFormater = new SimpleDateFormat(DATE_FORMAT_LOCAL);

        try {
            Date dateObj = dateFormater.parse(dateStr);
            calendar.setTime(dateObj);
        } catch (ParseException e) {
        }

        return calendar;
    }

    public static Calendar parserServerDate(String dateStr) {

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat dateFormater = new SimpleDateFormat(DATE_FORMAT_SERVER);

        try {
            Date dateObj = dateFormater.parse(dateStr);
            calendar.setTime(dateObj);
        } catch (ParseException e) {
        }

        return calendar;
    }


    public static String formatDateStrDayMmmDYyyy(Date date) {
        if (date == null) date = new Date();
        String dateFormat = "EEEE , MMMM d,yyyy";
        return formatDate(date, dateFormat);
    }

    public static String formatDate(int year, int month, int day, String reqDateFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        return formatDate(calendar.getTime(), reqDateFormat);

    }

    public static String formatDate(Date date, String reqDateFormat) {
        SimpleDateFormat postFormater = new SimpleDateFormat(reqDateFormat);
        String newDateStr = postFormater.format(date);
        return newDateStr;
    }


    public static String convertUtcToLocal(String purchasedAt) {

        Calendar cal = getDateInLocal(purchasedAt);

        return formatLocalDateFormat(cal);
    }

    public static Calendar getDateInLocal(String dateStr) {


        String dateFormat = DATE_FORMAT_SERVER;
        try {
            ///2016-11-29 07:52:42

            SimpleDateFormat sourceFormat = new SimpleDateFormat(dateFormat);
            TimeZone utcTime = TimeZone.getTimeZone("GMT");
            sourceFormat.setTimeZone(utcTime);
            Date parsed;
            parsed = sourceFormat.parse(dateStr);

//            SimpleDateFormat middleFormat = new SimpleDateFormat("MMMM dd hh:mm a");
//            TimeZone gmtTime = TimeZone.getTimeZone("UTC");
//            middleFormat.setTimeZone(gmtTime);
            TimeZone tz = TimeZone.getDefault();
            SimpleDateFormat destFormat = new SimpleDateFormat(dateFormat);
            destFormat.setTimeZone(tz);
            String result = destFormat.format(parsed);
            Date parsedDate = destFormat.parse(result);

            Calendar localCalendar = Calendar.getInstance();
            localCalendar.setTime(parsedDate);

            return localCalendar;

        } catch (Exception e) {

            Log.d(AppConstants.TAG, "Exception while converting UTC to local " + dateStr);
            e.printStackTrace();

            return Calendar.getInstance();
        }
    }


    public static int calculateDifferenceIn(Calendar dtThis, Calendar dtOther, int forWhat) {
        int difference = 0;

        if (dtThis.getTimeInMillis() > dtOther.getTimeInMillis()) return difference;


        while (true) {

            if (forWhat == Calendar.WEEK_OF_YEAR)
            {  dtThis.add(Calendar.DATE, 7);}
            else {dtThis.add(forWhat, 1);}

            Log.d(AppConstants.TAG,"adding 1 to first date forWhat "+forWhat+"  "+ formatLocalDateFormat(dtThis)+" - "+ formatLocalDateFormat(dtOther));

            long differ = dtThis.getTimeInMillis() - dtOther.getTimeInMillis();

            if (differ <= 1000) {
                difference++;
            } else break;
        }

        return difference;
    }





    public static String getTimeZone() {

        TimeZone tz = TimeZone.getDefault();
        System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());

        String timeZone = tz.getDisplayName(false, TimeZone.SHORT);
        Log.d(AppConstants.TAG, "Found time zone " + timeZone);
        return timeZone;
    }

}
