package com.txlabz.lawnote.utils;

import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;

import com.txlabz.lawnote.modelClasses.OptionsModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.modelClasses.TemplateFilledData;
import com.txlabz.lawnote.modelClasses.TemplateModel;

import java.sql.Array;
import java.util.ArrayList;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class WebViewUtils {

    public static void setValueToHtmlElement(WebView webView, QuestionModel questionModel)
    {

        Log.d(AppConstants.TAG,"prepopulating template answers. "+questionModel.getQuestionText()+" its answ is "+questionModel.getAnswer());


        if(!TextUtils.isEmpty(questionModel.getAnswer()))
        {
            ArrayList<String> urlsToUpdate=getUpdateElementUrlForQuestion(questionModel);

            if(urlsToUpdate!=null)
            {
                for(int i=0;i<urlsToUpdate.size();i++)
                {
                    Log.d(AppConstants.TAG,"Updating url "+urlsToUpdate.get(i));
                    webView.loadUrl(urlsToUpdate.get(i));
                }
            }
        }

        else {

            Log.d(AppConstants.TAG,"Its answer is null");

        }
    }

    private static ArrayList<String> getUpdateElementUrlForQuestion(QuestionModel questionModel) {

        String questionType=questionModel.getQuestionType();
        switch (questionType)
        {
            case AppConstants.QUESTION_TYPE_EDIT_TEXT:
            case AppConstants.QUESTION_TYPE_EDIT_TEXT_AREA:
                return editTextUpdateUrl(questionModel);

            case AppConstants.QUESTION_TYPE_DATE_INPUT:
                return dateUpdateUrl(questionModel);

            case AppConstants.QUESTION_TYPE_RADIO:
                return radioCheckUrl(questionModel);

            case AppConstants.QUESTION_TYPE_SPINNER:
                return spinnerUpdateUrl(questionModel);


            case AppConstants.QUESTION_TYPE_CHECK_BOX:
                return checkBoxUpdateUrl(questionModel);

        }

        return null;
    }




    private static ArrayList<String> spinnerUpdateUrl(QuestionModel questionModel) {
        ArrayList<String> urls=new ArrayList<>();

        String updatingUrl="javascript: (function() {document.getElementById('"+questionModel.getTagId()+"').value= '"+questionModel.getAnswer()+"';}) ();";

        urls.add(updatingUrl);
        return urls;

    }

    private static ArrayList<String> radioCheckUrl(QuestionModel questionModel) {

        ArrayList<String> urls=new ArrayList<>();

        urls.add("javascript: (function() {document.querySelector(\"input[name='"+questionModel.getTagId()+"'][value='"+questionModel.getAnswer()+"']\").checked = true})();");

        return urls;
    }

    private static ArrayList<String> dateUpdateUrl(QuestionModel questionModel) {

        ArrayList<String > urls=new ArrayList<>();

        String updatingUrl="javascript: (function() {document.getElementById('"+questionModel.getTagId()+"').value= '"+questionModel.getAnswer()+"';}) ();";
        urls.add(updatingUrl);

        return urls;
    }

    private static ArrayList<String> editTextUpdateUrl(QuestionModel questionModel) {

        ArrayList<String> urls=new ArrayList<>();

        String updatingUrl="javascript: (function() { document.getElementById('"+questionModel.getTagId()+"').value= '"+questionModel.getAnswer()+"';}) ();";

         urls.add(updatingUrl);

        return urls;

    }

    private static ArrayList<String> checkBoxUpdateUrl(QuestionModel questionModel) {



        ArrayList<String> urls=new ArrayList<>();

        int[] selectedOptions=Utils.getSelectedOptions(questionModel.getAnswer());
        if(selectedOptions.length>0)
        {
                for(int i=0;i<selectedOptions.length;i++) {

                    urls.add("javascript: (function() {document.querySelector(\"input[name='"+questionModel.getTagId()+"'][value='"+selectedOptions[i]+"']\").checked = true})();");

                }
        }

        return urls;
    }

    public static void populateUserFilledData(WebView webView, TemplateModel templateToPreFill)
    {

        ArrayList<QuestionModel> questionsList = templateToPreFill.getQuestionModels();
        if(webView==null|| questionsList==null)
            return;

        for(int i=0;i<questionsList.size();i++)
        {
            QuestionModel filledQuestion = questionsList.get(i);
            setValueToHtmlElement(webView,filledQuestion);
        }
    }

    public  static ArrayList<String> getFetchValueUrlForHtmlElements(QuestionModel questionModel) {

        String questionType=questionModel.getQuestionType();
        switch (questionType)
        {
            case AppConstants.QUESTION_TYPE_EDIT_TEXT:
            case AppConstants.QUESTION_TYPE_EDIT_TEXT_AREA:
                return getInputElementValue(questionModel);

            case AppConstants.QUESTION_TYPE_DATE_INPUT:
                return getInputElementValue(questionModel);

            case AppConstants.QUESTION_TYPE_RADIO:
                return getRadioButtonValue(questionModel);

            case AppConstants.QUESTION_TYPE_SPINNER:
                return getInputElementValue(questionModel);


            case AppConstants.QUESTION_TYPE_CHECK_BOX:
                return getCheckBoxValue(questionModel);

        }

        return null;
    }


    private static ArrayList<String > getInputElementValue(QuestionModel questionModel)
    {
        ArrayList<String> urls=new ArrayList<>();
        String updatingUrl="javascript: (function() { return document.getElementById('"+questionModel.getTagId()+"').value;}) ();";

        urls.add(updatingUrl);

        return urls;
    }

    private static ArrayList<String > getRadioButtonValue(QuestionModel questionModel)
    {
        ArrayList<String> urls=new ArrayList<>();

       String quertyFun="javascript: (function() {return document.querySelector('input[name = \""+questionModel.getTagId()+"\"]:checked').value;})();";

        urls.add(quertyFun);

        return urls;
    }


    private static ArrayList<String > getCheckBoxValue(QuestionModel questionModel)
    {
        ArrayList<String> urls=new ArrayList<>();

        ArrayList<OptionsModel> questionOptions = questionModel.getOptionsModel();

       // for(int i=0;i<questionOptions.size();i++)
        {
            String quertyFun="javascript: (function() {var checks=document.getElementsByClassName('"+questionModel.getTagId()+"');\n" +
                    "var result=\"\";\n" +
                    "\n" +
                    "for (var index = 0; index <  checks.length; ++index) {\n" +
                    "   if(checks[index].checked)\n" +
                    "   {\n" +
                    "     if(result!=\"\")\n" +
                    "         result+=\",\"+checks[index].value;\n" +
                    "        else\n" +
                    "         result+=checks[index].value;\n" +
                    "   }\n" +
                    "   \n" +
                    "}\n" +
                    "return result;})();";
          //  String quertyFun="javascript: (function() {return document.getElementsByName('"+questionModel.get+"').value})();";
            urls.add(quertyFun);

        }


        return urls;
    }
}
