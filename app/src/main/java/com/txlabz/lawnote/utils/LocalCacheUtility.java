package com.txlabz.lawnote.utils;

import com.txlabz.lawnote.modelClasses.Categories;
import com.txlabz.lawnote.modelClasses.NotificationDataModel;
import com.txlabz.lawnote.modelClasses.NotificationModel;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 13/08/2017.
 */

public class LocalCacheUtility {
    private static LocalCacheUtility localCacheUtitlty;
    private ArrayList<Categories> categoriesArrayList;
    private NotificationDataModel notificationDataModel;

    private LocalCacheUtility() {
        categoriesArrayList = new ArrayList<>();
    }
    public static LocalCacheUtility getInstance() {
        if (localCacheUtitlty == null)
            localCacheUtitlty = new LocalCacheUtility();

        return localCacheUtitlty;
    }

    public ArrayList<Categories> getCategoriesArrayList() {
        return categoriesArrayList;
    }

    public void setCategoriesArrayList(ArrayList<Categories> categoriesArrayList) {
        this.categoriesArrayList = categoriesArrayList;
    }

    public ArrayList<NotificationModel> getNotificationDataModel() {

        ArrayList<NotificationModel> notificationModels=new ArrayList<>();
        notificationModels.addAll(notificationDataModel.getNotificationModel());
        return notificationModels;
    }

    public void setNotificationDataModel(NotificationDataModel notificationDataModel) {
        this.notificationDataModel = notificationDataModel;
    }

    public void removeNotification(NotificationModel toBeRemove) {

        ArrayList<NotificationModel> notifications = notificationDataModel.getNotificationModel();
        for(int i=0;i<notifications.size();i++){

            if(notifications.get(i).getTemplateId().equals(toBeRemove.getTemplateId()))
            {
                notifications.remove(i);break;
            }
        }
    }

    public NotificationDataModel getNotificationDataModelMain() {
      return   notificationDataModel;
    }
}
