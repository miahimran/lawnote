package com.txlabz.lawnote.utils;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.fragments.BaseFragment;
import com.txlabz.lawnote.interfaces.CallbackListener;
import com.txlabz.lawnote.interfaces.DialogCallBackListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Fatima Siddique on 10/8/2016.
 */
public class Utils {

    private static ProgressDialog progressDialog;


    public static void showDatePickerDialog(Context context, DatePickerDialog.OnDateSetListener onDateSetListener, boolean limitMax, boolean limitMin,Calendar preselectedDate) {

         Calendar c;
        if(preselectedDate!=null)
        {c=preselectedDate;}
        else {c = Calendar.getInstance();}

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(context, R.style.DialogTheme, onDateSetListener, year, month, day);
        if (limitMax) dpd.getDatePicker().setMaxDate(new Date().getTime());
        if (limitMin) dpd.getDatePicker().setMinDate(new Date().getTime()-1000);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dpd.create();
        }
        else {dpd.show();return;}

        Button btnPositive = dpd.getButton(DialogInterface.BUTTON_POSITIVE);
        if(btnPositive!=null)
        {
            btnPositive.setTextColor(context.getResources().getColor(R.color.black));
        }
        Button btnNegative = dpd.getButton(DialogInterface.BUTTON_NEGATIVE);

        if(btnNegative!=null)
        {
            btnNegative.setTextColor(context.getResources().getColor(R.color.black));

        }
        Button btnNeutural = dpd.getButton(DialogInterface.BUTTON_NEUTRAL);

        if(btnNeutural!=null)
        {
            btnNeutural.setTextColor(context.getResources().getColor(R.color.black));

        }

        dpd.show();
    }


    public static void showSoftKeyboard(Activity activity,View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }


    public static void saveLoginData(String userId,String fristName,String lastName,String email,String paswword,String createdDate,String profileImage,Context context)
    {
        StorageUtility.saveDataInPreferences(context, AppConstants.PREF_KEY_USER_ID,userId);
        StorageUtility.saveDataInPreferences(context,AppConstants.PREF_KEY_FIRST_NAME,fristName);
        StorageUtility.saveDataInPreferences(context,AppConstants.PREF_KEY_LAST_NAME,lastName);
        StorageUtility.saveDataInPreferences(context,AppConstants.PREF_KEY_EMAIL,email);
        StorageUtility.saveDataInPreferences(context,AppConstants.PREF_KEY_PASSWORD,paswword);
        StorageUtility.saveDataInPreferences(context,AppConstants.PREF_KEY_CREATED_DATE,createdDate);
        StorageUtility.saveDataInPreferences(context,AppConstants.PREF_KEY_PROFILE_IMAGE,profileImage);
    }

    public static int getProgress(int curentPosition,int totalSize)
    {
        double currentPos=Double.valueOf(curentPosition);
        double total=Double.valueOf(totalSize);

        double progressResult=currentPos/total;
        progressResult=progressResult*100;

        Double d = new Double(progressResult);
        int progress=d.intValue();
        return progress;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void hideKeyboard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static boolean IsNetworkConnected(Context context) {

        ConnectivityManager connectivityManager=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager !=null)
        {
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }

        return false;
    }

    public static void showErrorMessage(Context context, String errorMSg) {

        showErrorMessage(context,errorMSg,null);
    }

    public static void showErrorMessage(Context context, String errorMSg, DialogCallBackListener callbackListener) {

        DialogsUtils.showAlert(context,errorMSg,callbackListener);
    }


    public static int[] getSelectedOptions(String answer) {

        ArrayList<Integer > intOptions=new ArrayList<>();
        if(!TextUtils.isEmpty(answer))
        {
            String[] ansParts=answer.split(",");

            if(ansParts!=null&&ansParts.length>0)
            for(int i=0;i<ansParts.length;i++)
            {
                if(!TextUtils.isEmpty(ansParts[i].trim()))
                {
                    try {
                        intOptions.add(Integer.valueOf(ansParts[i]));
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }



        }

      if(intOptions.size()>0)
      {
          int[] selectedOptions=new int[intOptions.size()];

          for(int i=0;i<intOptions.size();i++)
          {
              selectedOptions[i]=intOptions.get(i);
          }

          return selectedOptions;
      }

      return new int[0];


    }

    public static void switchToFragment(FragmentActivity mActivity, BaseFragment toFragment, boolean addToStack) {

        Log.d(AppConstants.TAG, "Previous stack Count " + mActivity.getSupportFragmentManager().getBackStackEntryCount());
        FragmentTransaction tr1 = mActivity.getSupportFragmentManager().beginTransaction();
        tr1.setCustomAnimations(0, 0, 0, 0);


        tr1.replace(R.id.fragmentContainer, toFragment, toFragment.getTag());

        if (addToStack) {
            tr1.addToBackStack(toFragment.getTag());
        }
        // tr1.commit();
        tr1.commitAllowingStateLoss();
    }



    public static String  saveSignature(Bitmap bitmap) {

        String fileName="sign.jpg";
        File file;


        File sdcard = Environment.getExternalStorageDirectory();

        File dir = new File(sdcard.getAbsolutePath() + "/LawNote");

        if (dir.exists())
        {
            file = new File(dir,fileName);

        }else {
            dir.mkdir();
            file = new File(dir,fileName);
        }

        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.toString();
    }

    public static void showProgressDialog(Context context) {

        Log.d(AppConstants.TAG,"Showing progress dialog ");
        if(progressDialog!=null)
        {  progressDialog.dismiss();
        progressDialog=null;}

         progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");

        progressDialog.show();
    }

    public static void hideProgressDialog()
    {
        Log.d(AppConstants.TAG,"hiding progress dialog ");
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog=null;
        }
    }

    public static Bitmap getBitmapFromPath(String filePath) {

      Log.d(AppConstants.TAG,"Getting bitmap from path "+filePath);
        File image = new File(filePath);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);
        return bitmap;

    }

    public static int  getUniqueCode(Object selectedTemplate) {
        return  System.identityHashCode(selectedTemplate);
    }

    public static String cleanUpAnswer(String answer) {
        return answer.replace("\"","");
    }

    public static SharedPreferences getSharedPreference(Activity activity) {
       return PreferenceManager.getDefaultSharedPreferences(activity);
    }

    public static void loadImage(Context context,ImageView imageView, String url) {

        Log.d(AppConstants.TAG,"Loading image  url "+url);

        if(TextUtils.isEmpty(url))
            return;

        Picasso.with(context).load(url).into(imageView);

    }

    public static boolean toBoolean(String isCreatedByUser) {
        if(TextUtils.isEmpty(isCreatedByUser)||isCreatedByUser.trim().equals("0"))
            return false;
        return true;
    }

    public static Calendar getDateInLocal(String dateStr) {
        try {
            ///2016-11-29 07:52:42

            SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone utcTime = TimeZone.getTimeZone("GMT");
            sourceFormat.setTimeZone(utcTime);
            Date parsed;
            parsed = sourceFormat.parse(dateStr);

//            SimpleDateFormat middleFormat = new SimpleDateFormat("MMMM dd hh:mm a");
//            TimeZone gmtTime = TimeZone.getTimeZone("UTC");
//            middleFormat.setTimeZone(gmtTime);
            TimeZone tz = TimeZone.getDefault();
            SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            destFormat.setTimeZone(tz);
            String result = destFormat.format(parsed);
            Date parsedDate = destFormat.parse(result);

            Calendar localCalendar = Calendar.getInstance();
            localCalendar.setTime(parsedDate);

            return localCalendar;

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static String calToStr(Calendar dateInLocal) {

        return (String) android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", dateInLocal.getTime());

    }

    public static void putExtraForClearLast(Intent intent) {

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    public static String getDeviceId(Context context)
    {
        String did = "";
        if(ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            did = tm.getDeviceId();
            if (did == null) {
                did = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        return did;
    }

    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});

        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

}
