package com.txlabz.lawnote.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by Ali Sabir on 8/23/2017.
 */

public class DynamicViewsHalper {

    public static LinearLayout.LayoutParams getViewParameters()
    {
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0,20,0,0);
        return layoutParams;
    }

    public static Typeface getFont(Context context) {

        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/AvenirNextLTPro-Regular.otf");
        return face;
    }
}
