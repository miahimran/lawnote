package com.txlabz.lawnote.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.txlabz.lawnote.modelClasses.TemplateAswerModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fatima Siddique on 10/19/2016.
 */
public class TemplateDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "templateManager";

    // Contacts table name
    private static final String TABLE_TEMPLATE = "template";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_TEMPLATE_ID = "template_id";
    private static final String KEY_TEMPLATE_NAME = "template_name";
    private static final String KEY_QUESTION_ID = "question_id";
    private static final String KEY_QUESTION_TYPE = "question_type";
    private static final String KEY_ANSWER = "answer";

    public TemplateDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_TEMPLATE + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_USER_ID + " TEXT,"
                + KEY_TEMPLATE_ID + " TEXT,"
                + KEY_TEMPLATE_NAME + " TEXT,"
                + KEY_QUESTION_ID + " TEXT,"
                + KEY_QUESTION_TYPE + " TEXT,"
                + KEY_ANSWER + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMPLATE);

        // Create tables again
        onCreate(db);
    }

    public void addTemplates(TemplateAswerModel templateAswerModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, templateAswerModel.getUserId());
        values.put(KEY_TEMPLATE_ID, templateAswerModel.getTemplateId());
        values.put(KEY_TEMPLATE_NAME,templateAswerModel.getTemplateName());
        values.put(KEY_QUESTION_ID,templateAswerModel.getQuestionID());
        values.put(KEY_QUESTION_TYPE,templateAswerModel.getQuestionType());
        values.put(KEY_ANSWER,templateAswerModel.getAnswer());

        // Inserting Row
        db.insert(TABLE_TEMPLATE, null, values);
        db.close(); // Closing database connection
    }

    public List<TemplateAswerModel> getAllTemplates() {
        List<TemplateAswerModel> templateAswerModelArrayList = new ArrayList<TemplateAswerModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TEMPLATE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TemplateAswerModel templateAswerModel = new TemplateAswerModel();
                templateAswerModel.setId(Integer.parseInt(cursor.getString(0)));
                templateAswerModel.setUserId(cursor.getString(1));
                templateAswerModel.setTemplateId(cursor.getString(2));
                templateAswerModel.setTemplateName(cursor.getString(3));
                templateAswerModel.setQuestionID(cursor.getString(4));
                templateAswerModel.setQuestionType(cursor.getString(5));
                templateAswerModel.setAnswer(cursor.getString(6));
                // Adding contact to list
                templateAswerModelArrayList.add(templateAswerModel);
            } while (cursor.moveToNext());
        }

        // return contact list
        return templateAswerModelArrayList;
    }

//    public int updateContact(Contact contact) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, contact.getName());
//        values.put(KEY_PH_NO, contact.getPhoneNumber());
//
//        // updating row
//        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
//                new String[] { String.valueOf(contact.getID()) });
//    }


    // Deleting single contact
//    public void deleteContact(Contact contact) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
//                new String[] { String.valueOf(contact.getID()) });
//        db.close();
//    }
//
//
//    // Getting contacts Count
//    public int getContactsCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        cursor.close();
//
//        // return count
//        return cursor.getCount();
//    }

}
