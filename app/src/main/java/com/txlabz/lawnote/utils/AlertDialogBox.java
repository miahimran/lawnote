package com.txlabz.lawnote.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;

import com.txlabz.lawnote.R;

/**
 * Created by Fatima Siddique on 7/25/2016.
 */
public class AlertDialogBox {

    AlertDialog.Builder alertDialogBuilder;

    public void loadAlert(String message,Context context)
    {

        alertDialogBuilder=new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));

        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert=alertDialogBuilder.create();
        alert.show();


    }
}
