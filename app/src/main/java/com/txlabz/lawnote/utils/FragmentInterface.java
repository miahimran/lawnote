package com.txlabz.lawnote.utils;

/**
 * Created by Fatima Siddique on 10/20/2016.
 */
public interface FragmentInterface {
    String getAnswer();
    String getQuestionTag();
    String getQuestionID();
}
