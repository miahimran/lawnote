package com.txlabz.lawnote.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.PermissionUtils;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends BaseActivity {

    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int RESULT_LOAD_CAMERA = 0;
    ImageView iconBack;
    CircleImageView userImage;

    SharedPreferences myData;
    String profileImage;
    EditText firstEditText, lastEditText;
    Button submitBtn;
    private String imagePath;
    String userID;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        userImage = (CircleImageView) findViewById(R.id.userImage);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        submitBtn = (Button) findViewById(R.id.submit);


        myData = Utils.getSharedPreference(EditProfileActivity.this);

        profileImage = myData.getString(AppConstants.PREF_KEY_PROFILE_IMAGE, AppConstants.NOT_FOUND);
        firstEditText = (EditText) findViewById(R.id.firstEditText);
        lastEditText = (EditText) findViewById(R.id.lastEditText);


        String firstName = myData.getString(AppConstants.PREF_KEY_FIRST_NAME, AppConstants.NOT_FOUND);
        String lastName = myData.getString(AppConstants.PREF_KEY_LAST_NAME, AppConstants.NOT_FOUND);
        userID = myData.getString(AppConstants.PREF_KEY_USER_ID, AppConstants.NOT_FOUND);

        firstEditText.setText(firstName);
        lastEditText.setText(lastName);



        Picasso.with(this).load(profileImage).placeholder(R.drawable.loading).into(userImage);

        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (PermissionUtils.isCameraPermissionGranted(EditProfileActivity.this)) {

                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {
                        ShowDialog();

                    }else {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},2);
                    }
                    }else {
                    ShowDialog();
                }

                }
//                }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imagePath != null) {
                    callWithImage();
                } else {
                    callWithoutProfileImage();
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == RESULT_LOAD_IMAGE && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            imagePath = picturePath;
            cursor.close();

            userImage.setImageURI(selectedImage);
        }else if (requestCode==RESULT_LOAD_CAMERA && null!=data)
        {
            try {
                Uri selectedImage=data.getData();
                userImage=(CircleImageView)findViewById(R.id.userImage);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                userImage.setImageBitmap(bitmap);
                String path=getRealPathFromURI(selectedImage);
                imagePath=path;

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    public void startIntentforImageFromGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , RESULT_LOAD_IMAGE);
    }

    public void startIntentforImageFromCamera() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, RESULT_LOAD_CAMERA);
    }

    public void callWithImage(){

        if (Build.VERSION.SDK_INT >= 23) {
            if (PermissionUtils.isStoragePermissionGranted(EditProfileActivity.this))
            {
                Log.d("tag","Permission Granted");

                final File file=new File(imagePath);

                updateProfilePic(imagePath);


            }

        }else {
            updateProfilePic(imagePath);
        }
    }

    private void updateProfilePic(String imagePath) {


        final File file=new File(imagePath);
        Utils.showProgressDialog(EditProfileActivity.this);

        Ion.with(this)
                .load(AppConstants.API_EDIT_PROFILE)
                .setMultipartParameter(AppConstants.PREF_KEY_USER_ID,userID)
                .setMultipartParameter("first_name",firstEditText.getText().toString())
                .setMultipartParameter("last_name",lastEditText.getText().toString())
                .setMultipartFile(AppConstants.PREF_KEY_PROFILE_IMAGE,file)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Utils.hideProgressDialog();


                        if (e==null)
                        {
                            try {
                                JSONObject jsonObject=new JSONObject(result);
                                String message = jsonObject.getString(AppConstants.MESSAGE);

                                String status = jsonObject.getString(AppConstants.STATUS);

                                if (Integer.parseInt(status)==0)
                                {

                                    Utils.showErrorMessage(EditProfileActivity.this,message);

                                }else {

                                    MainActivity.setUserImageFromFile(file);

                                    JSONObject getUSerData=jsonObject.getJSONObject(AppConstants.USER_DATA);
                                    String userId=getUSerData.getString(AppConstants.PREF_KEY_USER_ID);
                                    String firstName=getUSerData.getString(AppConstants.PREF_KEY_FIRST_NAME);
                                    String lastName=getUSerData.getString(AppConstants.PREF_KEY_LAST_NAME);
                                    String email=getUSerData.getString(AppConstants.PREF_KEY_EMAIL);
                                    String password=getUSerData.getString(AppConstants.PREF_KEY_PASSWORD);
                                    String createdDate=getUSerData.getString(AppConstants.PREF_KEY_CREATED_DATE);
                                    String profileImage=getUSerData.getString(AppConstants.PREF_KEY_PROFILE_IMAGE);
                                    Utils.saveLoginData(userId,firstName,lastName,email,password,createdDate,profileImage,getApplicationContext());

                                    Utils.showErrorMessage(EditProfileActivity.this,message);

                                }

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else{
                            e.printStackTrace();
                        }

                    }
                });

    }

    public void callWithoutProfileImage()
    {


        Utils.showProgressDialog(EditProfileActivity.this);
        Ion.with(this)
                .load(AppConstants.API_EDIT_PROFILE)
                .setBodyParameter(AppConstants.PREF_KEY_USER_ID,userID)
                .setBodyParameter("first_name",firstEditText.getText().toString())
                .setBodyParameter("last_name",lastEditText.getText().toString())
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        Utils.hideProgressDialog();

                        if (e==null)
                        {
                            try {
                                JSONObject jsonObject=new JSONObject(result);

                                String status = jsonObject.getString(AppConstants.STATUS);
                                String message = jsonObject.getString(AppConstants.MESSAGE);

                                if (Integer.parseInt(status)==0)
                                {

                                    Utils.showErrorMessage(EditProfileActivity.this,message);

                                }else {

                                    JSONObject getUSerData=jsonObject.getJSONObject(AppConstants.USER_DATA);
                                    String userId=getUSerData.getString(AppConstants.PREF_KEY_USER_ID);
                                    String firstName=getUSerData.getString(AppConstants.PREF_KEY_FIRST_NAME);
                                    String lastName=getUSerData.getString(AppConstants.PREF_KEY_LAST_NAME);
                                    String email=getUSerData.getString(AppConstants.PREF_KEY_EMAIL);
                                    String password=getUSerData.getString(AppConstants.PREF_KEY_PASSWORD);
                                    String createdDate=getUSerData.getString(AppConstants.PREF_KEY_CREATED_DATE);
                                    String profileImage=getUSerData.getString(AppConstants.PREF_KEY_PROFILE_IMAGE);
                                    Utils.saveLoginData(userId,firstName,lastName,email,password,createdDate,profileImage,getApplicationContext());

                                    Utils.showErrorMessage(EditProfileActivity.this,message);
                                }

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else{
                            e.printStackTrace();
                        }

                    }
                });

    }

    public void ShowDialog()
    {
        final Dialog dialog=new Dialog(EditProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image_chooser);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        RelativeLayout cameraLayout=(RelativeLayout)dialog.findViewById(R.id.cameraLayout);
        RelativeLayout galleryLayout=(RelativeLayout)dialog.findViewById(R.id.galleryLayout);

        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startIntentforImageFromCamera();
                //startIntentForCamera();
                startCameraByIntent(EditProfileActivity.this);
                dialog.dismiss();
            }
        });

        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentforImageFromGallery();
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void startCameraByIntent(Activity act) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Camera.getNumberOfCameras() > 1) {
            intent.putExtra("android.intent.extras.CAMERA_FACING",
                    Camera.CameraInfo.CAMERA_FACING_FRONT);
        }

        act.startActivityForResult(intent, RESULT_LOAD_CAMERA);

    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }
}
