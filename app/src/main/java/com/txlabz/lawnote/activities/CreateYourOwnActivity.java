package com.txlabz.lawnote.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.fragments.CreateCatalogFragment;
import com.txlabz.lawnote.fragments.PreviewCatalogFragment;
import com.txlabz.lawnote.interfaces.DialogCallBackListener;
import com.txlabz.lawnote.modelClasses.ContractInfo;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AlertDialogBox;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.StorageUtility;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class CreateYourOwnActivity extends BaseActivity {

    FrameLayout container;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    TextView preview,create;
    Boolean fragmentCreate=true;
    String userID,userName,email;
    ImageView iconBack;
    ProgressDialog progressDialog;
    AlertDialogBox alertDialogBox;

    String titleOfAgreement,contentOfAgreement;
    File getJpgSign,JpgFreelanceSign;
    String senderSignature,recieverSignature;
    String freeLancerName,freeLancerEmail,getFreeLanceName,getFreeLancelastname,getFreeLanceEmail;
    String generalFreeLancerName,generalFreeLancerEmail,generalFreeLanceName,generalFreeLanceEmail;
    String templateID="0";
    private TemplateModel selectedTemplate;
    private int INIT_TABS_FOR = CreateEditTemplateActivity.INIT_TABS_FOR_CREATE_TEMPLATE;
    private String content, templateTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_your_own);

        initViews();

    }

    private void initViews() {
        container=(FrameLayout)findViewById(R.id.container);
        preview=(TextView)findViewById(R.id.tvPreview);
        create=(TextView)findViewById(R.id.tvCreateTemplate);
        userID = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_USER_ID,AppConstants.NOT_FOUND);
        userName = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_FIRST_NAME,AppConstants.NOT_FOUND);
        email = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_EMAIL,AppConstants.NOT_FOUND);
        iconBack=(ImageView)findViewById(R.id.iconBack);
        progressDialog=new ProgressDialog(CreateYourOwnActivity.this);
        alertDialogBox=new AlertDialogBox();

        populateDataFromIntent();
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEditOrPreviewTab(false);
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEditOrPreviewTab(true);
            }
        });
    }

    private void populateDataFromIntent() {
        Bundle data = getIntent().getExtras();
        if (data != null) {
            INIT_TABS_FOR = data.getInt(CreateEditTemplateActivity.INIT_TABS_FOR);
            selectedTemplate = (TemplateModel) data.getSerializable(AppConstants.SELECTED_TEMPLATE);
            if (selectedTemplate == null)
                return;
            templateID = selectedTemplate.getTemplateId();
            String templateTitle = selectedTemplate.getTemplateTitle();
            String templateDescryption = selectedTemplate.getTemplateDescription();
            String recieverFirstName = selectedTemplate.getSenderFirstName();
            String recieverLastName = selectedTemplate.getSenderLastName();
            String recieverEmail = selectedTemplate.getSenderEmail();
            String recieverSignature = selectedTemplate.getSenderSignatureImage();
            senderSignature = selectedTemplate.getSenderSignatureImage();

            if (!TextUtils.isEmpty(recieverFirstName)) {
                setGetFreeLanceName(recieverFirstName);
            }
            if (!TextUtils.isEmpty(recieverEmail)) {
                setGetFreeLanceEmail(recieverEmail);
            }
            if (!TextUtils.isEmpty(recieverSignature)) {
                setRecieverSignature(recieverSignature);
            }
            if (!TextUtils.isEmpty(senderSignature)) {
                setSenderSignature(senderSignature);
            }

            setFreeLancerName(userName);
            setFreeLancerEmail(email);
            setTitleOfAgreement(templateTitle);
            setContentOfAgreement(templateDescryption);

            if (INIT_TABS_FOR  == CreateEditTemplateActivity.INIT_TABS_FOR_COMPLETED_PREVIEW) {
                create.setVisibility(View.GONE);
                selectEditOrPreviewTab(false);
            }
            else if (INIT_TABS_FOR == CreateEditTemplateActivity.INIT_TABS_FOR_EDIT_TEMPLATE)
                selectEditOrPreviewTab(false);
            else if (INIT_TABS_FOR == CreateEditTemplateActivity.INIT_TABS_fOR_RECEIVED_TEMPLATE){
                preview.setVisibility(View.GONE);
                selectEditOrPreviewTab(true);
            } else
                selectEditOrPreviewTab(true);
        } else
            selectEditOrPreviewTab(true);
    }

    private void selectEditOrPreviewTab(boolean isCreate) {
        if (isCreate) {
            setCreateColor();
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            create.setClickable(false);
            fragmentCreate=true;
            if (INIT_TABS_FOR != CreateEditTemplateActivity.INIT_TABS_FOR_EDIT_TEMPLATE && INIT_TABS_FOR != CreateEditTemplateActivity.INIT_TABS_FOR_CREATE_TEMPLATE)
                onBackPressed();
            else {
                CreateCatalogFragment fragment1 = new CreateCatalogFragment();
                fragmentCreate=true;
                LoadFragment(fragment1);
            }
        } else {
            setPreviewColor();
            create.setClickable(true);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            PreviewCatalogFragment fragment1=new PreviewCatalogFragment();
            fragmentCreate=false;
            LoadFragment(fragment1);
        }
    }

    @Override
    public void onBackPressed() {

        Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof CreateCatalogFragment) {
            if (INIT_TABS_FOR  == CreateEditTemplateActivity.INIT_TABS_FOR_COMPLETED_PREVIEW)
                super.onBackPressed();
            else
                DialogsUtils.showConfirmationDialog(CreateYourOwnActivity.this, new DialogCallBackListener() {
                    @Override
                    public void onOk() {
                        ContractInfo contractInfo = new ContractInfo();
                        contractInfo.setUserId(getUserID());
                        if (selectedTemplate != null) {
                            contractInfo.setTemplateId(selectedTemplate.getTemplateId());
                            contractInfo.setReceivers(selectedTemplate.getReceivers());
                        } if(recieverSignature!=null)
                            contractInfo.setUserSignature(recieverSignature);

                        postDataToServer(contractInfo);
                    }

                    @Override
                    public void onDiscard() {

                        finish();
                    }

                    @Override
                    public void onCancel() {

                    }
                });

        }else if (fragment instanceof PreviewCatalogFragment) {

            if (INIT_TABS_FOR  == CreateEditTemplateActivity.INIT_TABS_FOR_EDIT_TEMPLATE)
                selectEditOrPreviewTab(true);
            else if (INIT_TABS_FOR  == CreateEditTemplateActivity.INIT_TABS_FOR_COMPLETED_PREVIEW)
                super.onBackPressed();
            else {
                setCreateColor();
                super.onBackPressed();
            }
        } else
            super.onBackPressed();
    }


    public void LoadFragment(Fragment fragment)
    {
        fragmentManager=getSupportFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setCreateColor()
    {
        create.setSelected(true);
        preview.setSelected(false);
        preview.setTextColor(getResources().getColor(R.color.colorAccentW));
        create.setTextColor(getResources().getColor(R.color.white));
    }
    public void setPreviewColor()
    {
        preview.setSelected(true);
        create.setSelected(false);
        preview.setTextColor(getResources().getColor(R.color.white));
        create.setTextColor(getResources().getColor(R.color.colorAccentW));
    }


    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getUserID() {
        return userID;
    }

    public String getContentOfAgreement() {
        return contentOfAgreement;
    }

    public void setContentOfAgreement(String contentOfAgreement) {
        this.contentOfAgreement = contentOfAgreement;
    }

    public String getTitleOfAgreement() {
        return titleOfAgreement;
    }

    public void setTitleOfAgreement(String titleOfAgreement) {
        this.titleOfAgreement = titleOfAgreement;
    }

    public File getGetJpgSign() {
        return getJpgSign;
    }

    public void setGetJpgSign(File getJpgSign) {
        this.getJpgSign = getJpgSign;
    }

    public String getFreeLancerEmail() {
        return freeLancerEmail;
    }

    public void setFreeLancerEmail(String freeLancerEmail) {
        this.freeLancerEmail = freeLancerEmail;
    }

    public String getFreeLancerName() {
        return freeLancerName;
    }

    public void setFreeLancerName(String freeLancerName) {
        this.freeLancerName = freeLancerName;
    }

    public File getJpgFreelanceSign() {
        return JpgFreelanceSign;
    }

    public void setJpgFreelanceSign(File jpgFreelanceSign) {
        JpgFreelanceSign = jpgFreelanceSign;
    }

    public String getGetFreeLanceEmail() {
        return getFreeLanceEmail;
    }

    public void setGetFreeLanceEmail(String getFreeLanceEmail) {
        this.getFreeLanceEmail = getFreeLanceEmail;
    }

    public String getGetFreeLanceName() {
        return getFreeLanceName;
    }

    public void setGetFreeLanceName(String getFreeLanceName) {
        this.getFreeLanceName = getFreeLanceName;
    }

    public String getRecieverSignature() {
        return recieverSignature;
    }

    public void setRecieverSignature(String recieverSignature) {
        this.recieverSignature = recieverSignature;
    }

    public String getSenderSignature() {
        return senderSignature;
    }

    public void setSenderSignature(String senderSignature) {
        this.senderSignature = senderSignature;
    }


    public String getGeneralFreeLanceEmail() {
        return generalFreeLanceEmail;
    }

    public void setGeneralFreeLanceEmail(String generalFreeLanceEmail) {
        this.generalFreeLanceEmail = generalFreeLanceEmail;
    }

    public String getGeneralFreeLanceName() {
        return generalFreeLanceName;
    }

    public void setGeneralFreeLanceName(String generalFreeLanceName) {
        this.generalFreeLanceName = generalFreeLanceName;
    }

    public String getGeneralFreeLancerEmail() {
        return generalFreeLancerEmail;
    }

    public void setGeneralFreeLancerEmail(String generalFreeLancerEmail) {
        this.generalFreeLancerEmail = generalFreeLancerEmail;
    }

    public String getGeneralFreeLancerName() {
        return generalFreeLancerName;
    }

    public void setGeneralFreeLancerName(String generalFreeLancerName) {
        this.generalFreeLancerName = generalFreeLancerName;
    }

    public String getGetFreeLancelastname() {
        return getFreeLancelastname;
    }

    public void setGetFreeLancelastname(String getFreeLancelastname) {
        this.getFreeLancelastname = getFreeLancelastname;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTemplateTitle() {
        return templateTitle;
    }

    public void setTemplateTitle(String templateTitle) {
        this.templateTitle = templateTitle;
    }

    public TemplateModel getSelectedTemplate() {
        return selectedTemplate;
    }

    public void postDataToServer(ContractInfo contractInfo) {
        contractInfo.setContent(contentOfAgreement);
        contractInfo.setTemplateTitle(titleOfAgreement);

        final Gson gson=new Gson();
        DialogsUtils.showLoading(this);

        String url=AppConstants.CREATE_OWN_TEMPLATE;
//        if (selectedTemplate != null && selectedTemplate.isCreatedByUser())
//            url = AppConstants.SAVE_OWN_TEMPLATE;

        final String requestString = gson.toJson(contractInfo);

        Ion.with(this)
                .load(url)
                .setStringBody(requestString)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        DialogsUtils.dismiss();

                        if (e==null) {
                            try {
                                JSONObject jsonObject=new JSONObject(result);
                                String status=jsonObject.getString(AppConstants.STATUS);
                                String message=jsonObject.getString(AppConstants.MESSAGE);

                                if (Integer.parseInt(status)==1) {
                                    Intent intent=new Intent(CreateYourOwnActivity.this, ManageActivity.class);
                                    Utils.putExtraForClearLast(intent);
                                    intent.putExtra("isSignaturePreview","2");
                                    startActivity(intent);
                                    finish();
                                }else {
                                    DialogsUtils.showAlert(CreateYourOwnActivity.this, message);

                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }else {
                            e.printStackTrace();
                        }
                    }
                });

    }


    public int getINIT_TABS_FOR() {
        return INIT_TABS_FOR;
    }
}
