package com.txlabz.lawnote.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends Activity implements View.OnClickListener {

    EditText email, password;

    ProgressDialog progressDialog;
    SharedPreferences myLoginData;
    Bundle data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setBackgroundDrawableResource(R.drawable.background_login);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        progressDialog = new ProgressDialog(this);
        myLoginData = Utils.getSharedPreference(LoginActivity.this);


        init();
    }

    private void init() {

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        TextView register = (TextView) findViewById(R.id.lblRegister);
        TextView forgotPassword = (TextView) findViewById(R.id.lblForgotPassword);
        Button login = (Button) findViewById(R.id.btnLogin);
        register.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        login.setOnClickListener(this);
        data = getIntent().getExtras();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.lblRegister:
                intent = new Intent(this, RegisterActivity.class);
                if (data != null) {
                    intent.putExtras(data);
                }
                startActivity(intent);
                finish();
                break;
            case R.id.lblForgotPassword:
                intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btnLogin:
                getReadStatePermission();
                break;
        }
    }

    public void getReadStatePermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, AppConstants.GET_READ_PERMISSION_RC);
        } else{
            login();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstants.GET_READ_PERMISSION_RC:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    login();
                }
                break;

        }
    }
    private boolean isDataValid() {
        String getEmail = email.getText().toString();
        String getPass = password.getText().toString();
        String message = "";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (getEmail.length() == 0 || !getEmail.matches(emailPattern)) {
            message = AppConstants.ENTER_EMAIL;
        } else if (getPass.length() == 0) {
            message = AppConstants.ENTER_PASSWORD;
        } else if (!Utils.IsNetworkConnected(getApplicationContext()))
            message = getString(R.string.no_internet);
        if (!TextUtils.isEmpty(message)) {
            DialogsUtils.showAlert(this, getString(R.string.no_internet));
            return false;
        }
        return true;
    }
    private void login() {
        if (!isDataValid())
            return;

        Utils.hideSoftKeyboard(this);
        progressDialog.setMessage(AppConstants.LOADING);
        progressDialog.show();

        String loginUrl = AppConstants.API_LOGIN_URL;
        String token = FirebaseInstanceId.getInstance().getToken();
        Ion.with(getApplicationContext())
                .load(loginUrl)
                .setBodyParameter("email", email.getText().toString())
                .setBodyParameter("password", password.getText().toString())
                .setBodyParameter("type", "1")//android
                .setBodyParameter("device_id", Utils.getDeviceId(this))
                .setBodyParameter("gcm_key", token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.d(AppConstants.TAG, "Login data " + result);

                        if (e == null) {
                            if (result != null) {
                                try {
                                    progressDialog.dismiss();

                                    JSONObject jsonObject = new JSONObject(result);
                                    String status = jsonObject.getString(AppConstants.STATUS);

                                    if (Integer.parseInt(status) == 1) {
                                        SharedPreferences.Editor editor = myLoginData.edit();
                                        editor.putBoolean(AppConstants.IS_LOGIN, true);
                                        editor.commit();

                                        JSONObject getUSerData = jsonObject.getJSONObject(AppConstants.USER_DATA);
                                        String userId = getUSerData.getString(AppConstants.PREF_KEY_USER_ID);
                                        String firstName = getUSerData.getString(AppConstants.PREF_KEY_FIRST_NAME);
                                        String lastName = getUSerData.getString(AppConstants.PREF_KEY_LAST_NAME);
                                        String email = getUSerData.getString(AppConstants.PREF_KEY_EMAIL);
                                        String password = getUSerData.getString(AppConstants.PREF_KEY_PASSWORD);
                                        String createdDate = getUSerData.getString(AppConstants.PREF_KEY_CREATED_DATE);
                                        String profileImage = getUSerData.getString(AppConstants.PREF_KEY_PROFILE_IMAGE);
                                        Utils.saveLoginData(userId, firstName, lastName, email, password, createdDate, profileImage, getApplicationContext());


                                        if (data != null) {
                                            Intent intent = new Intent(LoginActivity.this, PreviewSignatureDataActivity.class);
                                            intent.putExtras(data);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }

                                    } else {
                                        String message = jsonObject.getString(AppConstants.MESSAGE);
                                        DialogsUtils.showAlert(LoginActivity.this, message);
                                    }

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }

                        } else {
                            e.printStackTrace();
                        }
                    }
                });


    }
}
