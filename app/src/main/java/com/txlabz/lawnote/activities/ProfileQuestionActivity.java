package com.txlabz.lawnote.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.fragments.CreateTemplateFragment;
import com.txlabz.lawnote.fragments.HtmlTemplateFragment;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AlertDialogBox;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProfileQuestionActivity extends BaseActivity {
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    TextView preview,create,saveButton;
    String freelancerName;
    ArrayList<QuestionModel> arrayList;
    String templateUrl,templateDescription;
    int currentFragementosition;
    int progress;
    String templateName;
    ImageView iconSkip;
    TextView titleTemplate;
    Boolean fragmentCreate=true;
    String clientName,freeLanceName,workScope,completeData,
            flatFee,revisionRounds,freelanceFee,deadLine,spinnerItem,radioItem,checkBoxValue1,checkBoxValue2,checkBoxValue3="";
    JSONArray resultJsonArray;
    SharedPreferences myData;
    String userID,userName,email;
    ProgressDialog progressDialog;
    AlertDialogBox alertDialogBox;
    String templateID;
    String answerCount;
    Boolean isFromManage=false;
    String isFromPending;
    JSONArray newJsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_question);
        preview=(TextView)findViewById(R.id.tvPreview);
        create=(TextView)findViewById(R.id.tvCreateTemplate);
        arrayList=new ArrayList<>();
        titleTemplate=(TextView)findViewById(R.id.title_template);
        iconSkip=(ImageView)findViewById(R.id.iconSkip);
        saveButton=(TextView)findViewById(R.id.save);
        resultJsonArray=new JSONArray();
        myData= Utils.getSharedPreference(ProfileQuestionActivity.this);
        userID=myData.getString(AppConstants.PREF_KEY_USER_ID,AppConstants.NOT_FOUND);
        userName=myData.getString(AppConstants.PREF_KEY_FIRST_NAME,AppConstants.NOT_FOUND);
        email=myData.getString(AppConstants.PREF_KEY_EMAIL,AppConstants.NOT_FOUND);
        final CreateTemplateFragment fragment=new CreateTemplateFragment();
        LoadFragment(fragment);
        progressDialog=new ProgressDialog(ProfileQuestionActivity.this);
        alertDialogBox=new AlertDialogBox();
        newJsonArray=new JSONArray();

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preview.setTextColor(getResources().getColor(R.color.white));
                create.setTextColor(getResources().getColor(R.color.colorAccentW));
                create.setClickable(true);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                HtmlTemplateFragment fragment1=new HtmlTemplateFragment();
                fragmentCreate=false;
                LoadFragment(fragment1);
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preview.setTextColor(getResources().getColor(R.color.colorAccentW));
                create.setTextColor(getResources().getColor(R.color.white));
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                create.setClickable(false);
                fragmentCreate=true;
                onBackPressed();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    saveDataCall();

            }
        });
        Bundle data=getIntent().getExtras();
        if (data!=null)
        {
            isFromManage=true;
            isFromPending=data.getString(AppConstants.IS_FROM_PENDING);
            templateID=data.getString(AppConstants.TEMPLATE_ID);
            templateName=data.getString(AppConstants.TEMPLATE_TITLE);
            templateUrl=data.getString(AppConstants.TEMPLATE_URL);
            templateDescription=data.getString(AppConstants.TEMPLATE_DESCRIPTION);
            arrayList=(ArrayList<QuestionModel>) data.getSerializable(AppConstants.QUESTION_ARRAY);
            answerCount=data.getString(AppConstants.ANSWER_COUNT);
        }
        titleTemplate.setText(templateName);

        iconSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog=new Dialog(ProfileQuestionActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.save_alert_view);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);

                TextView cancelBtn=(TextView)dialog.findViewById(R.id.cancelBtn);
                TextView saveBtn=(TextView)dialog.findViewById(R.id.saveBtn);
                TextView discardBtn=(TextView)dialog.findViewById(R.id.discardBtn);

                discardBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        saveDataCall();
                    }
                });

                dialog.show();
                //finish();
            }
        });
    }
    public void LoadFragment(Fragment fragment)
    {
        fragmentManager=getSupportFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setFreelancerName(String name)
    {
        freelancerName=name;
    }

    public String getFreelancerName()
    {
        return freelancerName;
    }

    public String getTemplateUrl()
    {
        return templateUrl;
    }

    public ArrayList<QuestionModel> getArrayList()
    {
        return arrayList;
    }

    public int getQuestionArrayListSize()
    {
        return arrayList.size();
    }

    public void setPosition(int position)
    {
        currentFragementosition=position;
    }
    public int getPosition()
    {
        return currentFragementosition;
    }

    public void setDonut_progress(int getProgress) {
        progress=getProgress;
    }
    public  int getProgress()
    {
        return progress;
    }

    public boolean getFragment()
    {
        return fragmentCreate;
    }

    public void setClientName(String name)
    {
        clientName=name;
    }
    public String getClientName()
    {
        return  clientName;
    }

    public void setFreelanceFee(String freelanceFee) {
        this.freelanceFee = freelanceFee;
    }

    public void setRevisionRounds(String revisionRounds) {
        this.revisionRounds = revisionRounds;
    }

    public void setFlatFee(String flatFee) {
        this.flatFee = flatFee;
    }

    public void setCompleteData(String completeData) {
        this.completeData = completeData;
    }

    public void setWorkScope(String workScope) {
        this.workScope = workScope;
    }

    public void setFreeLanceName(String freeLanceName) {
        this.freeLanceName = freeLanceName;
    }

    public String getFreelanceFee() {
        return freelanceFee;
    }

    public String getRevisionRounds() {
        return revisionRounds;
    }

    public String getFlatFee() {
        return flatFee;
    }

    public String getCompleteData() {
        return completeData;
    }

    public String getWorkScope() {
        return workScope;
    }

    public String getFreeLanceName() {
        return freeLanceName;
    }

    public String getTemplateDescription()
    {
        return templateDescription;
    }

    public String getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(String deadLine) {
        this.deadLine = deadLine;
    }

    public String getSpinnerItem() {
        return spinnerItem;
    }

    public void setSpinnerItem(String spinnerItem) {
        this.spinnerItem = spinnerItem;
    }

    public String getRadioItem() {
        return radioItem;
    }

    public void setRadioItem(String radioItem) {
        this.radioItem = radioItem;
    }

    public String getCheckBoxValue3() {
        return checkBoxValue3;
    }

    public void setCheckBoxValue3(String checkBoxValue3) {
        this.checkBoxValue3 = checkBoxValue3;
    }

    public String getCheckBoxValue2() {
        return checkBoxValue2;
    }

    public void setCheckBoxValue2(String checkBoxValue2) {
        this.checkBoxValue2 = checkBoxValue2;
    }

    public String getCheckBoxValue1() {
        return checkBoxValue1;
    }

    public void setCheckBoxValue1(String checkBoxValue1) {
        this.checkBoxValue1 = checkBoxValue1;
    }

    @Override
    public void onBackPressed() {
      //  super.onBackPressed();

        Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.container);

        if (fragment instanceof CreateTemplateFragment)
        {

            final Dialog dialog=new Dialog(ProfileQuestionActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.save_alert_view);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);

            TextView cancelBtn=(TextView)dialog.findViewById(R.id.cancelBtn);
            TextView saveBtn=(TextView)dialog.findViewById(R.id.saveBtn);
            TextView discardBtn=(TextView)dialog.findViewById(R.id.discardBtn);

            discardBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveDataCall();
                }
            });

            dialog.show();
        }else if (fragment instanceof HtmlTemplateFragment){
            setCreateColor();
            super.onBackPressed();
        }


//        Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.container);
//        if (fragment==null)
//        {
//            finish();
//        }
    }

    public void setCreateColor()
    {
        preview.setTextColor(getResources().getColor(R.color.colorAccentW));
        create.setTextColor(getResources().getColor(R.color.white));
    }
    public void setPreviewColor()
    {
        preview.setTextColor(getResources().getColor(R.color.white));
        create.setTextColor(getResources().getColor(R.color.colorAccentW));
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getAnswerCount() {
        return answerCount;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getTemplateID() {
        return templateID;
    }

    public String getUserID() {
        return userID;
    }

    public void saveDataCall()
    {
       // CreateTemplateFragment createTemplateFragment=(CreateTemplateFragment)getSupportFragmentManager().findFragmentById(R.id.container);
        resultJsonArray=  getNewJsonArray(); //createTemplateFragment.getJsonArray();
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put(AppConstants.PREF_KEY_USER_ID,userID);
            jsonObject.put(AppConstants.TEMPLATE_ID,templateID);
            jsonObject.put(AppConstants.ANSWER_ARRAY,resultJsonArray);

            if (resultJsonArray.length()>0)
            {
                progressDialog.setMessage(AppConstants.LOADING);
                progressDialog.show();

                JsonParser jsonParser = new JsonParser();
                JsonObject gsonObject = (JsonObject)jsonParser.parse(jsonObject.toString());

                Ion.with(ProfileQuestionActivity.this)
                        .load(AppConstants.API_SAVE_ANSWERS)
                        .setJsonObjectBody(gsonObject)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                progressDialog.dismiss();
                                if (e==null)
                                {
                                    try {
                                        JSONObject jsonObject1=new JSONObject(result);
                                        String status=jsonObject1.getString(AppConstants.STATUS);
                                        String message=jsonObject1.getString(AppConstants.MESSAGE);
                                        if (Integer.parseInt(status)==1)
                                        {
                                            Intent intent=new Intent(ProfileQuestionActivity.this,ManageActivity.class);
                                            Utils.putExtraForClearLast(intent);
                                            if (isFromPending!=null && isFromPending.equals("1"))
                                            {
                                                intent.putExtra("isSignaturePreview","2");
                                            }
                                            startActivity(intent);
                                            finish();
                                        }else {
                                            alertDialogBox.loadAlert(message,ProfileQuestionActivity.this);
                                        }
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }else {
                                    e.printStackTrace();
                                }

                            }
                        });
            }else {
                alertDialogBox.loadAlert("Fill some data to save template",ProfileQuestionActivity.this);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isFromManage()
    {
        return isFromManage;
    }

    public JSONArray getNewJsonArray() {
        return newJsonArray;
    }

    public void setNewJsonArray(JSONArray newJsonArray) {
        this.newJsonArray = newJsonArray;
    }
}
