package com.txlabz.lawnote.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.fragments.HtmlTemplateFragment;
import com.txlabz.lawnote.handler.TemplateDetailHandler;
import com.txlabz.lawnote.interfaces.TemplateDetailListener;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.StorageUtility;
import com.txlabz.lawnote.utils.Utils;

import org.w3c.dom.Text;

public class TemplateDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_detail2);

        findViewById(R.id.ibCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);

        String templateId=getIntent().getStringExtra(AppConstants.PUT_EXTRA_KEY_TEMPLATE_ID);
        String templateTitle=getIntent().getStringExtra(AppConstants.PUT_EXTRA_KEY_TEMPLATE_TITLE);
        final int  htmlPageType=getIntent().getIntExtra(AppConstants.PUT_EXTRA_KEY_HTML_PAGE_TYPE,-1);

        if(TextUtils.isEmpty(templateId)){
            Utils.showErrorMessage(this,getString(R.string.msg_template_id_not_found));
            finish();
        }

        if(!TextUtils.isEmpty(templateTitle)) {tvTitle.setText(templateTitle);}
        else tvTitle.setText("");


        String userId= StorageUtility.getDataFromPreferences(this,AppConstants.PREF_KEY_USER_ID,"0");


        TemplateDetailHandler.getTemplateDetail(this, userId, templateId, new TemplateDetailListener() {
            @Override
            public void onTemplateDetailReceived(TemplateModel templateModel) {

                if(templateModel!=null)
                Utils.switchToFragment(TemplateDetailActivity.this, HtmlTemplateFragment.newInstance(htmlPageType,templateModel),false);
                else {
                    Utils.showErrorMessage(TemplateDetailActivity.this,getString(R.string.msg_unknown_error));
                    finish();
                }
            }
        });
    }
}
