package com.txlabz.lawnote.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.GsonBuilder;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.fragments.TemplatesListFragment;
import com.txlabz.lawnote.modelClasses.ManageModel;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.StorageUtility;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ManageActivity extends BaseActivity {



    public static final int TAB_INDEX_DRAFT = 0;
    public static final int TAB_INDEX_PENDING = 1;
    public static final int TAB_INDEX_COMPLETED = 2;


    SharedPreferences myData;
    String userID;
    ArrayList<TemplateModel> draftsModelArrayList;
    ArrayList<TemplateModel> pendingModelArrayList;
    ArrayList<TemplateModel> completedModelArrayList;
    TabLayout tabLayout;
    ViewPager viewPager;
    String isSignaturePreview;
    Intent getIntent;
    ImageView iconBack;
    int viewPagerPosition=0;
    SwipeRefreshLayout swipeLayout;
    private int redrictTo=TAB_INDEX_DRAFT;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);
        myData=Utils.getSharedPreference(ManageActivity.this);


        iconBack=(ImageView)findViewById(R.id.iconBack);
        viewPager = (ViewPager) findViewById(R.id.pager);
        swipeLayout=(SwipeRefreshLayout)findViewById(R.id.swipeLayout);
        swipeLayout.setColorSchemeResources(R.color.mainPrimaryLight, R.color.colorAccentMain, R.color.mainPrimaryDark);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            swipeLayout.setProgressViewOffset(false, 0, Utils.getToolbarHeight(this));

        userID= StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_USER_ID,AppConstants.NOT_FOUND);

        draftsModelArrayList=new ArrayList<>();
        pendingModelArrayList=new ArrayList<>();
        completedModelArrayList=new ArrayList<>();

        getIntent=getIntent();


        isSignaturePreview=getIntent.getStringExtra("isSignaturePreview");

        loadUserTemplates();

        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                redrictTo=viewPager.getCurrentItem();

                loadUserTemplates();
            }
        });




        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                viewPagerPosition=tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                swipeLayout.setEnabled(false);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        swipeLayout.setEnabled(true);
                        break;
                }
                return false;
            }
        });

       checkForRedrict();
    }

    private void checkForRedrict() {

         redrictTo = getIntent().getIntExtra(AppConstants.PUT_EXTRA_KEY_REDRICT_TO, TAB_INDEX_DRAFT);

      redrictTo(redrictTo);
    }


    private void redrictTo(int redrictTo) {
            viewPager.setCurrentItem(redrictTo);

    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(TemplatesListFragment.newInstence(draftsModelArrayList,TemplatesListFragment.TEMPLATES_TYPE_DRAFT), getString(R.string.draft));
        adapter.addFrag(TemplatesListFragment.newInstence(pendingModelArrayList,TemplatesListFragment.TEMPLATES_TYPE_PENDING), getString(R.string.pending));
        adapter.addFrag(TemplatesListFragment.newInstence(completedModelArrayList,TemplatesListFragment.TEMPLATES_TYPE_COMPLETED), getString(R.string.completed));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void loadUserTemplates()
    {
        //Utils.showProgressDialog(this);

        swipeLayout.setRefreshing(true);
        if (Utils.IsNetworkConnected(this))
        {
            String url=AppConstants.API_GET_MANAGED_TEMLATES+userID;
            Log.d(AppConstants.TAG,"Loading templates "+url);


            Ion.with(this)
                    .load(url)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {

                            Utils.hideProgressDialog();
                            if (e==null)
                            {
                                if (result!=null)
                                {
                                    try {

                                        ManageModel array = new GsonBuilder().create().fromJson(result, ManageModel.class);
                                        draftsModelArrayList=array.getDraftsTemplateModels();
                                        pendingModelArrayList=array.getPendingTemplateModels();
                                        completedModelArrayList=array.getCompletedTemplateModels();

                                        setupViewPager(viewPager);


                                        swipeLayout.setRefreshing(false);

                                        redrictTo(redrictTo);

                                    }catch (Exception ex)
                                    {
                                        ex.printStackTrace();
                                    }
                                }
                            }else {
                                e.printStackTrace();
                            }
                        }
                    });
        }
        else {
            DialogsUtils.showAlert(this, AppConstants.CHECK_CONNECTION);
        }

    }

    public int getViewPagerPosition() {
        return viewPagerPosition;
    }
}
