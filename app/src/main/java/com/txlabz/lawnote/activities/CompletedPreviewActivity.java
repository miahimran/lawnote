package com.txlabz.lawnote.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.PendingTemplateModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AppConstants;

import java.util.ArrayList;

public class CompletedPreviewActivity extends BaseActivity {

    TextView freelancerName, freelancerEmail, freelanceName, freelanceEmail;
    WebView webView;
    ImageView senderSignature, recieverSignature;
    String templateID,templateUrl;
    public String fileName="myDataFile";
    SharedPreferences myData;
    String userID,userName,userEmail;
    String recieverFirstName;
    String recieverEmail;
    String getRecieverSignature;
    String getSenderSignature;
    String isCreatedByUser;
    ProgressDialog progressDialog;
    ArrayList<QuestionModel> questionModelArrayList;
    TextView title_template;
    String templateTitle;
    ImageView iconSkip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_preview);
        myData=getSharedPreferences(fileName,0);
        freelancerName=(TextView)findViewById(R.id.freelancerName);
        freelancerEmail=(TextView)findViewById(R.id.freelancerEmail);
        freelanceName=(TextView)findViewById(R.id.freelanceName);
        freelanceEmail=(TextView)findViewById(R.id.freelanceEmail);
        senderSignature=(ImageView)findViewById(R.id.senderSignature);
        recieverSignature=(ImageView)findViewById(R.id.recieverSignature);
        title_template=(TextView)findViewById(R.id.title_template);
        progressDialog=new ProgressDialog(this);
        questionModelArrayList=new ArrayList<>();
        iconSkip=(ImageView)findViewById(R.id.iconSkip);

        userID=myData.getString(AppConstants.PREF_KEY_USER_ID,AppConstants.NOT_FOUND);
        userName=myData.getString(AppConstants.PREF_KEY_FIRST_NAME,AppConstants.NOT_FOUND);
        userEmail=myData.getString(AppConstants.PREF_KEY_EMAIL,AppConstants.NOT_FOUND);

        webView=(WebView)findViewById(R.id.mainLayout);
        webView.getSettings().setJavaScriptEnabled(true);

        Bundle data = getIntent().getExtras();
        if (data != null) {

            templateID = data.getString(AppConstants.TEMPLATE_ID);
            templateTitle = data.getString(AppConstants.TEMPLATE_TITLE);
            templateUrl=data.getString(AppConstants.TEMPLATE_URL);
            String templateDescryption = data.getString(AppConstants.TEMPLATE_DESCRIPTION);
            recieverFirstName = data.getString(AppConstants.RECIEVER_FIRSTNAME);
            String recieverLastName = data.getString(AppConstants.RECIEVER_LASTNAME);
            recieverEmail = data.getString(AppConstants.RECIEVER_EMAIL);
            getRecieverSignature = data.getString(AppConstants.RECIEVER_SIGNATURE);
            getSenderSignature  = data.getString(AppConstants.SENDER_SIGNATURE);
            isCreatedByUser=data.getString(AppConstants.IS_CREATED_BY_USER);

        }

        freelancerName.setText(userName);
        freelanceEmail.setText(userEmail);
        freelanceName.setText(recieverFirstName);
        freelanceName.setText(recieverEmail);
        title_template.setText(templateTitle);

        if (getSenderSignature!=null&&!getSenderSignature.isEmpty())
        {
            Picasso.with(CompletedPreviewActivity.this).load(getSenderSignature).into(senderSignature);
        }

        if (!getRecieverSignature.isEmpty())
        {
            Picasso.with(CompletedPreviewActivity.this).load(getRecieverSignature).into(recieverSignature);
        }
        webView.loadUrl(templateUrl);


        iconSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if (Integer.parseInt(isCreatedByUser)==0)
        {
            progressDialog.setMessage(AppConstants.LOADING);
            progressDialog.show();
            Ion.with(this)
                    .load(AppConstants.API_GET_PENDING_TEMPLATES)
                    .setBodyParameter(AppConstants.PREF_KEY_USER_ID,userID)
                    .setBodyParameter(AppConstants.TEMPLATE_ID,templateID)
                    .setBodyParameter(AppConstants.IS_CREATED_BY_USER,isCreatedByUser)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {

                            if (e==null)
                            {
                                PendingTemplateModel array = new GsonBuilder().create().fromJson(result, PendingTemplateModel.class);
                                questionModelArrayList=array.getTemplateModel().getQuestionModels();

                                webView.setWebViewClient(new WebViewClient(){

                                    @Override
                                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                                        super.onPageStarted(view, url, favicon);
                                    }

                                    @Override
                                    public void onPageFinished(WebView view, String url) {
                                        super.onPageFinished(view, url);

                                        for (int i=0; i<questionModelArrayList.size(); i++)
                                        {
                                            if (questionModelArrayList.get(i).getTagId().equals("client_name"))
                                            {
                                                view.loadUrl("javascript:setValue('client_name','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("freelance_name"))
                                            {
                                                view.loadUrl("javascript:setValue('freelance_name','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("work_scope"))
                                            {
                                                view.loadUrl("javascript:setValue('work_scope','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("flat_fee"))
                                            {
                                                view.loadUrl("javascript:setValue('flat_fee','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else  if (questionModelArrayList.get(i).getTagId().equals("revision_rounds"))
                                            {
                                                view.loadUrl("javascript:setValue('revision_rounds','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("freelancer_fee"))
                                            {
                                                view.loadUrl("javascript:setValue('freelancer_fee','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("complete_date"))
                                            {
                                                view.loadUrl("javascript:set_date('complete_date','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("invoice_due"))
                                            {
                                                view.loadUrl("javascript:set_spinner('invoice_due','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("payment_due"))
                                            {
                                                view.loadUrl("javascript:set_radio('payment_due','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("payment_due"))
                                            {
                                                view.loadUrl("javascript:set_radio('payment_due','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("approval_checkbox"))
                                            {
                                                view.loadUrl("javascript:set_checkbox('approval_checkbox','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }
                                        }

                                    }
                                });

                                webView.loadUrl(templateUrl);
                                progressDialog.dismiss();

                            }else {
                                e.printStackTrace();
                            }
                        }
                    });
        }


    }
}
