package com.txlabz.lawnote.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.GsonBuilder;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.special.ResideMenu.ResideMenu;
import com.squareup.picasso.Picasso;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.fragments.CatalogFragment;
import com.txlabz.lawnote.fragments.HomeFragment;
import com.txlabz.lawnote.fragments.ReceivedTemplatesListFragment;
import com.txlabz.lawnote.fragments.TemplateFragment;
import com.txlabz.lawnote.modelClasses.HomeDataModel;
import com.txlabz.lawnote.modelClasses.NotificationDataModel;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.LocalCacheUtility;
import com.txlabz.lawnote.utils.StorageUtility;
import com.txlabz.lawnote.utils.Utils;

import java.io.File;

public class MainActivity extends BaseActivity implements View.OnClickListener{

    ResideMenu resideMenu;
    TextView toolbarHeader, tvNotificatinCount;
    ImageView addIcon;
    EditText headerEditText;
    private static ImageView userImage;
    String userID;
    private TextView tvNotManageCount;
    private TextView tvNotReceivedCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        getReadStatePermission();

        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
    }

    public void getReadStatePermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, AppConstants.GET_READ_PERMISSION_RC);
        } else{
            loadCategoriesAndNotifications();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstants.GET_READ_PERMISSION_RC:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadCategoriesAndNotifications();
                }
                break;

        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        updateNotificationCount();
    }

    private void initViews() {

        headerEditText=(EditText)findViewById(R.id.headerEditText);
        View navView = LayoutInflater.from(this).inflate(R.layout.nav_drawer, null, false);
        resideMenu = new ResideMenu(this);
        resideMenu.addView(navView);
        resideMenu.setBackgroundColor(getResources().getColor(R.color.colorPrimaryMain));
        resideMenu.attachToActivity(this);
        resideMenu.setScaleValue(0.5f);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        tvNotificatinCount = (TextView) findViewById(R.id.notification_area);
        userImage=(ImageView)navView.findViewById(R.id.userImage);
        TextView userName=(TextView)navView.findViewById(R.id.userName);
        TextView userEmail=(TextView)navView.findViewById(R.id.userEmail);
        LinearLayout navHome=(LinearLayout)navView.findViewById(R.id.nav_home);
        LinearLayout navCatalog = (LinearLayout) navView.findViewById(R.id.nav_catalog);
        LinearLayout navManage=(LinearLayout)navView.findViewById(R.id.nav_manage);
        LinearLayout navReceived=(LinearLayout)navView.findViewById(R.id.nav_received);
        LinearLayout navSettings=(LinearLayout)navView.findViewById(R.id.nav_settings);
        LinearLayout navLogout=(LinearLayout)navView.findViewById(R.id.nav_logout);
        LinearLayout navHowItWork = (LinearLayout)navView.findViewById(R.id.nav_how_it_works);

       tvNotManageCount= (TextView) navView.findViewById(R.id.tvManageCount);
       tvNotReceivedCount= (TextView) navView.findViewById(R.id.tvReceivedCount);

        String name = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_FIRST_NAME,AppConstants.NOT_FOUND);
        String email = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_EMAIL,AppConstants.NOT_FOUND);
        String profileImage = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_PROFILE_IMAGE,AppConstants.NOT_FOUND);
        userID = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_USER_ID,AppConstants.NOT_FOUND);

        userName.setText(name);
        userEmail.setText(email);
        Picasso.with(this).load(profileImage).placeholder(R.drawable.loading).into(userImage);

        ImageView menuIcon = (ImageView) findViewById(R.id.menuIcon);
        menuIcon.setOnClickListener(this);
        toolbarHeader = (TextView) findViewById(R.id.toolbarTitle);
        addIcon = (ImageView) findViewById(R.id.addIcon);

        navHome.setOnClickListener(this);
        navCatalog.setOnClickListener(this);
        navManage.setOnClickListener(this);
        navReceived.setOnClickListener(this);
        navSettings.setOnClickListener(this);
        navLogout.setOnClickListener(this);
        navHowItWork.setOnClickListener(this);
        addIcon.setOnClickListener(this);
    }

    private void initMainFragment() {
        HomeFragment myf = new HomeFragment();
        Bundle bundle=new Bundle();
        myf.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.mainFrame, myf, myf.getClass().getName());
        transaction.commit();

    }

    @Override
    public void onBackPressed() {
        if (resideMenu.isOpened()) {
            resideMenu.closeMenu();
        } else {

            if(isSearchViewCleared())
            super.onBackPressed();

        }
    }

    private boolean isSearchViewCleared() {

        if(headerEditText.getText().length()>0)
        {
            Utils.hideKeyboard(this);
            headerEditText.setText("");return false;
        }
        else if(headerEditText.getVisibility()==View.VISIBLE)
        {
            toolbarHeader.setVisibility(View.VISIBLE);
            headerEditText.setVisibility(View.GONE);return false;
        }

        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void loadFragment(final Fragment fragment) {
        resideMenu.closeMenu();
        if(fragment.isVisible())
            return;
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrame, fragment,fragment.getClass().getName());
        transaction.addToBackStack(null);

        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.nav_home:
                Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.mainFrame);
                String tag ="";
                if (fragment!=null) {
                    tag=fragment.getTag();
                }
                if (!TextUtils.isEmpty(tag) &&  tag.equals(HomeFragment.FRAGMENT_TAG)) {
                    resideMenu.closeMenu();
                }else {
                    resideMenu.closeMenu();
                    onBackPressed();
                }
                break;
            case R.id.nav_manage:
                intent = new Intent(MainActivity.this,ManageActivity.class);
                Utils.putExtraForClearLast(intent);
                startActivity(intent);
                break;
            case R.id.nav_received:
                    loadReceivedFragment();
                break;
            case R.id.nav_settings:
                intent = new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_logout:
                StorageUtility.saveDataInPreferences(this, AppConstants.IS_LOGIN,false);
                intent=new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.menuIcon:
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
                break;
            case R.id.addIcon:
                handleAddOption();
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.NAME,AppConstants.CATALOG);
                fragment.setArguments(bundle);
                loadFragment(fragment);
                break;
            case R.id.nav_how_it_works:
                intent = new Intent(this, WalkthroughActivity.class);
                intent.putExtra(AppConstants.PUT_EXTRA_KEY_OPENED_FROM,WalkthroughActivity.OPENED_FROM_DRAWER);
                startActivity(intent);
                break;
        }
    }

    public  void loadReceivedFragment() {


        ReceivedTemplatesListFragment fragment = ReceivedTemplatesListFragment.newInstance();
        Bundle bundle1 = new Bundle();
        bundle1.putString(AppConstants.NAME,AppConstants.RECEIVED);
        fragment.setArguments(bundle1);
        loadFragment(fragment);


    }

    private void handleAddOption() {

        final Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.mainFrame);
        String tag = fragment.getTag();
        if (tag.equals(HomeFragment.FRAGMENT_TAG)) {
            HomeFragment homeFragment=(HomeFragment) fragment;
            homeFragment.loadCategories();
        } else if (tag.equals(TemplateFragment.FRAGMENT_TAG) || tag.equals(CatalogFragment.FRAGMENT_TAG)) {
            toolbarHeader.setVisibility(View.GONE);
            headerEditText.setVisibility(View.VISIBLE);

            InputMethodManager inputMethodManager =  (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(headerEditText.getApplicationWindowToken(),InputMethodManager.SHOW_FORCED, 0);
            headerEditText.requestFocus();
            setTextWatcher(fragment);
        }

    }


    private void setTextWatcher(final Fragment fragment) {
        headerEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String tag = fragment.getTag();
                if(TextUtils.isEmpty(tag))
                {
                    return;
                }
                if (tag.equals(TemplateFragment.FRAGMENT_TAG)) {
                    TemplateFragment templateFragment=(TemplateFragment)fragment;
                    templateFragment.searchTemplateName(headerEditText.getText().toString());
                } else if (tag.equals(CatalogFragment.FRAGMENT_TAG)) {
                    CatalogFragment catalogFragment=(CatalogFragment) fragment;
                    catalogFragment.searchCategoryTemplate(headerEditText.getText().toString());
                }

                if (headerEditText.getText().toString().equals("")) {
                    Utils.hideSoftKeyboard(MainActivity.this);
                }
            }
        });
    }
    public void setTitle(String title){
        toolbarHeader.setText(title);
    }
    public void changeAddIcon(int resource){

        if(resource>0)
        {  addIcon.setImageResource(resource);
        addIcon.setVisibility(View.VISIBLE);}
        else {addIcon.setVisibility(View.GONE);}
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    public void loadCategoriesAndNotifications() {
        if (!Utils.IsNetworkConnected(this)) {
            DialogsUtils.showAlert(this, AppConstants.CHECK_CONNECTION);
            return;
        }
        String token = FirebaseInstanceId.getInstance().getToken();

        DialogsUtils.showLoading(this);
        Ion.with(this)
                .load(AppConstants.GET_HOME_CONTENT + "?user_id="+userID
                        +"&type=1&device_id="+Utils.getDeviceId(this)+"&gcm_key="+token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        DialogsUtils.dismiss();
                        if (e == null) {
                            if (result!=null) {
                                try {
                                    HomeDataModel array = new GsonBuilder().create().fromJson(result, HomeDataModel.class);
                                    if (array != null) {
                                        LocalCacheUtility localCacheUtility = LocalCacheUtility.getInstance();
                                        localCacheUtility.setCategoriesArrayList(array.getCategories());
                                        NotificationDataModel notificationDataModel = array.getNotificationDataModel();
                                        if (notificationDataModel != null) {

                                            localCacheUtility.setNotificationDataModel(array.getNotificationDataModel());

                                            updateNotificationCount();
                                        }
                                    }
                                    initMainFragment();

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }else {
                            e.printStackTrace();
                        }
                    }
                });

    }

    private void updateNotificationCount() {


        LocalCacheUtility localCacheUtility=LocalCacheUtility.getInstance();
        NotificationDataModel notificationDataModel=  localCacheUtility.getNotificationDataModelMain();

        if(notificationDataModel==null) return;

        int completedCount=notificationDataModel.getCompletedNotificationCount();
        int pendingCount=notificationDataModel.getPendingNotificationCount();
        int receivedCount=notificationDataModel.getReceivedNotificationCount();
        int totalCount=completedCount+pendingCount+receivedCount;
        int manageCount=pendingCount+completedCount;


        if (totalCount > 0) {
            tvNotificatinCount.setText(String.valueOf(totalCount));
            tvNotificatinCount.setVisibility(View.VISIBLE);
        } else
            tvNotificatinCount.setVisibility(View.GONE);

        if (receivedCount > 0) {
            tvNotReceivedCount.setText(String.valueOf(receivedCount));
            tvNotReceivedCount.setVisibility(View.VISIBLE);
        } else
            tvNotReceivedCount.setVisibility(View.GONE);


        if (manageCount > 0) {
            tvNotManageCount.setText(String.valueOf(manageCount));
            tvNotManageCount.setVisibility(View.VISIBLE);
        } else
            tvNotManageCount.setVisibility(View.GONE);




    }

    public void headerSettingForHome() {
        toolbarHeader.setVisibility(View.VISIBLE);
        headerEditText.setVisibility(View.GONE);
    }

    public static void setUserImageFromFile(File file) {
        if(file.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            userImage.setImageBitmap(myBitmap);
        }
    }



}
