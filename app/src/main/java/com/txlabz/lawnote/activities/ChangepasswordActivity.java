package com.txlabz.lawnote.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AlertDialogBox;
import com.txlabz.lawnote.utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangepasswordActivity extends BaseActivity {

    ImageView iconBack;
    Button submitBtn;
    EditText newPassword;
    AlertDialogBox alertDialogBox;
    ProgressDialog progressDialog;
    public String fileName = "myDataFile";
    SharedPreferences myData;
    String userID;
    public static String CheckLoginfile="myLoginFile";
    SharedPreferences myLoginData;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        iconBack = (ImageView) findViewById(R.id.iconBack);
        submitBtn = (Button) findViewById(R.id.submit);
        newPassword=(EditText)findViewById(R.id.newPassword);
        myData = getSharedPreferences(fileName, 0);
        userID = myData.getString(AppConstants.PREF_KEY_USER_ID, AppConstants.NOT_FOUND);
        alertDialogBox = new AlertDialogBox();
        progressDialog=new ProgressDialog(ChangepasswordActivity.this);
        myLoginData=getSharedPreferences(CheckLoginfile,0);

        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (newPassword.getText().toString().length()>0)
                {
                    progressDialog.setMessage(AppConstants.LOADING);
                    progressDialog.show();

                    Ion.with(ChangepasswordActivity.this)
                            .load(AppConstants.API_EDIT_PROFILE)
                            .setBodyParameter(AppConstants.PREF_KEY_USER_ID,userID)
                            .setBodyParameter("password",newPassword.getText().toString())
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {

                                    progressDialog.dismiss();
                                    if (e==null)
                                    {
                                        JSONObject jsonObject= null;
                                        try {
                                            jsonObject = new JSONObject(result);
                                            String message = jsonObject.getString(AppConstants.MESSAGE);
                                            String status = jsonObject.getString(AppConstants.STATUS);

                                            if (Integer.parseInt(status)==0)
                                            {

                                                alertDialogBox.loadAlert(message,ChangepasswordActivity.this);

                                            }else {
                                                alertDialogBox.loadAlert(message,ChangepasswordActivity.this);
                                                SharedPreferences.Editor editor=myLoginData.edit();
                                                editor.putBoolean(AppConstants.IS_LOGIN,false);
                                                editor.commit();

                                                Intent intent=new Intent(ChangepasswordActivity.this,LoginActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        } catch (JSONException e1) {
                                            e1.printStackTrace();
                                        }


                                    }else {
                                        e.printStackTrace();
                                    }
                                }
                            });
                }else {
                    alertDialogBox.loadAlert(AppConstants.ENTER_PASSWORD,ChangepasswordActivity.this);
                }
            }
        });
    }
}
