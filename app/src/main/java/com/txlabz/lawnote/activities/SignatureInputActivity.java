package com.txlabz.lawnote.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.PermissionUtils;
import com.txlabz.lawnote.utils.Utils;

public class SignatureInputActivity extends AppCompatActivity {

    private String savedSignaturePath="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature_input);

        TextView clearBtn,cancelBtn,saveBtn;

        clearBtn=(TextView) findViewById(R.id.clearBtn);
        cancelBtn=(TextView) findViewById(R.id.cancelBtn);
        saveBtn=(TextView) findViewById(R.id.saveBtn);

        final SignaturePad signaturePad=(SignaturePad)findViewById(R.id.signature_pad);

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {


                if (PermissionUtils.isStoragePermissionGranted(SignatureInputActivity.this))
                {
                    Bitmap bitmap=signaturePad.getSignatureBitmap();
                  savedSignaturePath=  Utils.saveSignature(bitmap);

                }else {
                    DialogsUtils.showAlert(SignatureInputActivity.this,"Give Permission and Sign again");
                }


            }

            @Override
            public void onClear() {

                savedSignaturePath=null;
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(savedSignaturePath))
                 returnResult(true);
                else returnResult(false);




            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signaturePad.clear();
                returnResult(false);

       }
        });
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
            }
        });



    }

    private void returnResult(boolean resultOk) {

        Intent intent=new Intent();
        intent.putExtra(AppConstants.KEY_SIGNATURE,savedSignaturePath);

      if(resultOk)  {setResult(RESULT_OK, intent);}
        else
      {
          setResult(RESULT_CANCELED);
      }
        finish();

    }
}
