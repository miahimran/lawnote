package com.txlabz.lawnote.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.GsonBuilder;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.modelClasses.PendingTemplateModel;
import com.txlabz.lawnote.modelClasses.QuestionModel;
import com.txlabz.lawnote.utils.AlertDialogBox;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.PermissionUtils;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PreviewSignatureDataActivity extends BaseActivity {

    WebView webLayout;
    String templateId,userID,isCreatedByUser;
    ArrayList<QuestionModel> questionModelArrayList;
    ImageView freelancerSignature;
    TextView nameHeading2,emaiHeading2,reciewverNameHeading2,reciewverEmaiHeading2,clearBtn;

    SharedPreferences myData;
    SignaturePad signature_pad;
    File getJpgImageFile;
    AlertDialogBox alertDialogBox;
    TextView heading;
    ImageView iconSkip;
    Button submitBtn;
    String recieverFirstName,recieverLastName,recieverEmail;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_signature_data);
        freelancerSignature=(ImageView)findViewById(R.id.freelancerSignature);
        nameHeading2=(TextView)findViewById(R.id.nameHeading2);
        emaiHeading2=(TextView)findViewById(R.id.emaiHeading2);
        reciewverNameHeading2=(TextView)findViewById(R.id.reciewverNameHeading2);
        reciewverEmaiHeading2=(TextView)findViewById(R.id.reciewverEmaiHeading2);
        signature_pad=(SignaturePad)findViewById(R.id.signature_pad);
        submitBtn=(Button)findViewById(R.id.submit);
        heading=(TextView)findViewById(R.id.heading);
        iconSkip=(ImageView)findViewById(R.id.iconSkip);
        alertDialogBox=new AlertDialogBox();
        clearBtn=(TextView)findViewById(R.id.clearBtn);
        myData= Utils.getSharedPreference(PreviewSignatureDataActivity.this);
        webLayout=(WebView)findViewById(R.id.webLayout);
        webLayout.getSettings().setJavaScriptEnabled(true);
        questionModelArrayList=new ArrayList<>();


        Utils.showProgressDialog(PreviewSignatureDataActivity.this);

        final String name=myData.getString(AppConstants.PREF_KEY_FIRST_NAME,AppConstants.NOT_FOUND);
        final String email=myData.getString(AppConstants.PREF_KEY_EMAIL,AppConstants.NOT_FOUND);


        Bundle data=getIntent().getExtras();

        if (data!=null)
        {
            templateId=data.getString(AppConstants.TEMPLATE_ID);
            userID=data.getString(AppConstants.PREF_KEY_USER_ID);
            isCreatedByUser=data.getString(AppConstants.IS_CREATED_BY_USER);
        }

        if (templateId!=null)
        {
            Ion.with(PreviewSignatureDataActivity.this)
                    .load(AppConstants.API_GET_PENDING_TEMPLATES)
                    .setBodyParameter(AppConstants.TEMPLATE_ID,templateId)
                    .setBodyParameter(AppConstants.PREF_KEY_USER_ID,userID)
                    .setBodyParameter(AppConstants.IS_CREATED_BY_USER,isCreatedByUser)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {

                            if (e==null)
                            {
                                PendingTemplateModel array = new GsonBuilder().create().fromJson(result, PendingTemplateModel.class);
                                questionModelArrayList=array.getTemplateModel().getQuestionModels();

                                String templateUrl=array.getTemplateModel().getTemplateURL();

                                Picasso.with(PreviewSignatureDataActivity.this).load(array.getTemplateModel().getSenderSignatureImage()).into(freelancerSignature);

//                                recieverFirstName=array.getTemplateModel().getRecieverFirstName();
//                                recieverLastName=array.getTemplateModel().getRecieverLastName();
//                                recieverEmail=array.getTemplateModel().getRecieverEmail();

                                heading.setText(array.getTemplateModel().getTemplateTitle());
                                nameHeading2.setText(name);
                                emaiHeading2.setText(email);
                                reciewverNameHeading2.setText(recieverFirstName);

                                reciewverEmaiHeading2.setText(recieverEmail);
                                webLayout.loadUrl(templateUrl);

                                webLayout.setWebViewClient(new WebViewClient(){

                                    @Override
                                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                                        super.onPageStarted(view, url, favicon);
                                    }

                                    @Override
                                    public void onPageFinished(WebView view, String url) {
                                        super.onPageFinished(view, url);

                                        for (int i=0; i<questionModelArrayList.size(); i++)
                                        {
                                            if (questionModelArrayList.get(i).getTagId().equals("client_name"))
                                            {
                                                view.loadUrl("javascript:setValue('client_name','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("freelance_name"))
                                            {
                                                view.loadUrl("javascript:setValue('freelance_name','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("work_scope"))
                                            {
                                                view.loadUrl("javascript:setValue('work_scope','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("flat_fee"))
                                            {
                                                view.loadUrl("javascript:setValue('flat_fee','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else  if (questionModelArrayList.get(i).getTagId().equals("revision_rounds"))
                                            {
                                                view.loadUrl("javascript:setValue('revision_rounds','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("freelancer_fee"))
                                            {
                                                view.loadUrl("javascript:setValue('freelancer_fee','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("complete_date"))
                                            {
                                                view.loadUrl("javascript:set_date('complete_date','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("invoice_due"))
                                            {
                                                view.loadUrl("javascript:set_spinner('invoice_due','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("payment_due"))
                                            {
                                                view.loadUrl("javascript:set_radio('payment_due','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("payment_due"))
                                            {
                                                view.loadUrl("javascript:set_radio('payment_due','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }else if (questionModelArrayList.get(i).getTagId().equals("approval_checkbox"))
                                            {
                                                view.loadUrl("javascript:set_checkbox('approval_checkbox','"+questionModelArrayList.get(i).getAnswer()+"')");
                                            }
                                        }

                                    }
                                });

                                webLayout.loadUrl(templateUrl);



                                Utils.hideProgressDialog();



                            }else {
                                e.printStackTrace();
                            }

                        }
                    });
        }

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signature_pad.clear();
            }
        });

        signature_pad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {

                if (PermissionUtils.isStoragePermissionGranted(PreviewSignatureDataActivity.this))
                {
                    Bitmap bitmap=signature_pad.getSignatureBitmap();
                    String fileName="test3.jpg";
                    File file;


                    File sdcard = Environment.getExternalStorageDirectory();

                    File dir = new File(sdcard.getAbsolutePath() + "/LawNote");

                    if (dir.exists())
                    {
                        file = new File(dir,fileName);

                    }else {
                        dir.mkdir();
                        file = new File(dir,fileName);
                    }

                    try {
                        FileOutputStream out = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,100,out);
                        out.flush();
                        out.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (dir.exists())
                    {
                        getJpgImageFile = new File(Environment.getExternalStorageDirectory()
                                .getAbsolutePath() + "/LawNote", fileName);
                    }
                }else {
                    alertDialogBox.loadAlert("Give Permission and Sign again",PreviewSignatureDataActivity.this);
                }
            }

            @Override
            public void onClear() {

            }
        });

        iconSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PreviewSignatureDataActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getJpgImageFile!=null)
                {
                    Ion.with(PreviewSignatureDataActivity.this)
                            .load(AppConstants.API_SAVE_RECEIVER_SIGNATURE)
                            .setMultipartParameter(AppConstants.TEMPLATE_ID,templateId)
                            .setMultipartFile("receiver_signature_image",getJpgImageFile)
                            .setMultipartParameter("receiver_first_name",recieverFirstName)
                            .setMultipartParameter("receiver_last_name",recieverLastName)
                            .setMultipartParameter("receiver_email",recieverEmail)
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {

                                    if (e==null)
                                    {
                                        try {
                                            JSONObject jsonObject=new JSONObject(result);

                                            String status=jsonObject.getString(AppConstants.STATUS);
                                            String message=jsonObject.getString(AppConstants.MESSAGE);

                                            if (Integer.parseInt(status)==1)
                                            {
                                                Intent intent=new Intent(PreviewSignatureDataActivity.this,ManageActivity.class);
                                                intent.putExtra("isFromPreview","1");
                                                Utils.putExtraForClearLast(intent);
                                                startActivity(intent);
                                                finish();
                                            }else {
                                                alertDialogBox.loadAlert(message,PreviewSignatureDataActivity.this);
                                            }
                                        } catch (JSONException e1) {
                                            e1.printStackTrace();
                                        }

                                    }else {
                                        e.printStackTrace();
                                    }

                                }
                            });
                }else {
                    alertDialogBox.loadAlert("Please Sign first then try again",PreviewSignatureDataActivity.this);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent(PreviewSignatureDataActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
