package com.txlabz.lawnote.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.Utils;

import java.util.Date;

public class RegisterActivity extends BaseActivity implements View.OnClickListener{

    EditText firstName,lastName,email,password;
    CheckBox permissionCheckBox;
    ProgressDialog progressDialog;
    Button register;
    Date date=new Date();
    public static String CheckLoginfile="myLoginFile";
    SharedPreferences myLoginData;
    Bundle data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getWindow().setBackgroundDrawableResource(R.drawable.background_login);
        init();

        firstName=(EditText)findViewById(R.id.firstName) ;
        lastName=(EditText)findViewById(R.id.lastName) ;
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.password);
        permissionCheckBox=(CheckBox)findViewById(R.id.permissionCheckBox);
        register = (Button) findViewById(R.id.register);
        progressDialog=new ProgressDialog(this);
        myLoginData=getSharedPreferences(CheckLoginfile,0);


        data=getIntent().getExtras();


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.IsNetworkConnected(getApplicationContext())) {

                    if (firstName.length() == 0) {
                        DialogsUtils.showAlert(RegisterActivity.this, AppConstants.FIRST_NAME);

                    }else if (lastName.length() == 0)
                    {
                        DialogsUtils.showAlert(RegisterActivity.this, AppConstants.LAST_NAME);

                    }else if (email.length() == 0)
                    {
                        DialogsUtils.showAlert(RegisterActivity.this, AppConstants.ENTER_EMAIL);
                    }
                    else if ( password.length() == 0)
                    {
                        DialogsUtils.showAlert(RegisterActivity.this, AppConstants.ENTER_PASSWORD);

                    }
                    else if (permissionCheckBox.isChecked())
                    {
                        String fName = firstName.getText().toString();
                        String lName = lastName.getText().toString();
                        String getEmail = email.getText().toString();
                        String getPass = password.getText().toString();
                        String getDate = date.toString();

                        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                        if (getEmail.matches(emailPattern))
                        {
                            Utils.hideKeyboard(RegisterActivity.this);

                            progressDialog.setMessage(AppConstants.LOADING);
                            progressDialog.show();

                            String apiRegister=AppConstants.API_REGISTER_URL;
                            Ion.with(getApplicationContext())
                                    .load(apiRegister)

                                    .setBodyParameter("email", getEmail)
                                    .setBodyParameter("first_name",fName)
                                    .setBodyParameter("last_name",lName)
                                    .setBodyParameter("password",getPass)
                                    .setBodyParameter("created_date",getDate)
                                    .asString()
                                    .setCallback(new FutureCallback<String>() {
                                        @Override
                                        public void onCompleted(Exception e, String result) {

                                            progressDialog.dismiss();

                                            String status;
                                            String message;

                                            try {
                                                JSONObject jsonObject=new JSONObject(result);

                                                status=jsonObject.getString(AppConstants.STATUS);

                                                if (Integer.parseInt(status)==0)
                                                {
                                                    message=jsonObject.getString(AppConstants.MESSAGE);
                                                    DialogsUtils.showAlert(RegisterActivity.this, message);

                                                }else {

                                                    SharedPreferences.Editor editor=myLoginData.edit();
                                                    editor.putBoolean(AppConstants.IS_LOGIN,true);
                                                    editor.commit();

                                                    JSONObject getUSerData=jsonObject.getJSONObject(AppConstants.USER_DATA);
                                                    String userId=getUSerData.getString(AppConstants.PREF_KEY_USER_ID);
                                                    String firstName=getUSerData.getString(AppConstants.PREF_KEY_FIRST_NAME);
                                                    String lastName=getUSerData.getString(AppConstants.PREF_KEY_LAST_NAME);
                                                    String email=getUSerData.getString(AppConstants.PREF_KEY_EMAIL);
                                                    String password=getUSerData.getString(AppConstants.PREF_KEY_PASSWORD);
                                                    String createdDate=getUSerData.getString(AppConstants.PREF_KEY_CREATED_DATE);
                                                    String profileImage=getUSerData.getString(AppConstants.PREF_KEY_PROFILE_IMAGE);
                                                    Utils.saveLoginData(userId,firstName,lastName,email,password,createdDate,profileImage,getApplicationContext());



                                                    if (data!=null)
                                                    {
                                                        Intent intent=new Intent(RegisterActivity.this,PreviewSignatureDataActivity.class);
                                                        intent.putExtras(data);
                                                        startActivity(intent);
                                                        finish();
                                                    }else {
                                                        Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                }

                                            } catch (JSONException e1) {
                                                e1.printStackTrace();
                                            }


                                        }
                                    });

                        }else {
                           DialogsUtils.showAlert(RegisterActivity.this, AppConstants.CHECK_VALID_EMAIL);
                        }



                    }
                    else {
                        DialogsUtils.showAlert(RegisterActivity.this,AppConstants.TERMS_CONDITION);
                    }

                }else {
                    DialogsUtils.showAlert(RegisterActivity.this, AppConstants.CHECK_CONNECTION);

                }

            }
        });

    }

    private void init() {
        TextView backToLogin = (TextView) findViewById(R.id.lblBackToLogin);

        backToLogin.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lblBackToLogin:
                startLoginActivity();
                break;
        }
    }
    private void startLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public void onBackPressed(){
        startLoginActivity();
    }

}
