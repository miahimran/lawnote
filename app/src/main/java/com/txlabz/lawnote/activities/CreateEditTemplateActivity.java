package com.txlabz.lawnote.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.adapters.TemplateFillingTabsFragmentAdapter;
import com.txlabz.lawnote.fragments.BaseTemplateQuestionsFragment;
import com.txlabz.lawnote.fragments.CreateCatalogFragment;
import com.txlabz.lawnote.fragments.CreateTemplateInputFragment;
import com.txlabz.lawnote.fragments.CreateTemplateIntroFragment;
import com.txlabz.lawnote.fragments.HtmlTemplateFragment;
import com.txlabz.lawnote.fragments.EmptyTemplateFragment;
import com.txlabz.lawnote.fragments.TemplateEditPromptFragment;
import com.txlabz.lawnote.handler.EditPendingTemplateHandler;
import com.txlabz.lawnote.interfaces.DialogCallBackListener;
import com.txlabz.lawnote.interfaces.SaveCompleteListener;
import com.txlabz.lawnote.interfaces.ServerResponseListener;
import com.txlabz.lawnote.modelClasses.TemplateModel;
import com.txlabz.lawnote.modelClasses.server_responses.GeneralResponse;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.StorageUtility;
import com.txlabz.lawnote.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateEditTemplateActivity extends BaseActivity {

    public static final int TAB_INDEX_INTRO_FOR_TEMPLATE = 0;
    public static final int TAB_INDEX_CREATE_TEMPLATE = 1;
    public static final int TAB_INDEX_PREVIEW_TEMPLATE = 2;
    public static final int TAB_INDEX_EDIT_TEMPLATE = 3;

    public static final String INIT_TABS_FOR = "initTabsFor";
    public static final int INIT_TABS_FOR_CREATE_TEMPLATE = 1;
    public  static final int INIT_TABS_FOR_EDIT_TEMPLATE = 2;
    public  static final int INIT_TABS_FOR_COMPLETED_PREVIEW = 3;
    public  static final int INIT_TABS_fOR_RECEIVED_TEMPLATE=4;


    public static final String ALREADY_SAVED = "alreadySaved";
    @BindView(R.id.icon_cancel)
    ImageView mIconCancel;
    @BindView(R.id.actions_container)
    View mCreatePreviewEditButtons;
    @BindView(R.id.toolbarTitle)
    TextView mToolbarTitle;
    @BindView(R.id.tvCreateTemplate)
    TextView tvCreateTemplate;
    @BindView(R.id.tvPreview)
    TextView tvPreviewTemplate;
    @BindView(R.id.tvEdit)
    TextView tvEditTemplate;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private String userID,userName,email;

    private BaseTemplateQuestionsFragment[] tabFragments={};

    private ViewPager viewPagerTabs;
    private TemplateModel selectedTemplate;
    private boolean alreadySaved;
    private int initTabsFor;
    private boolean inEditMode=false;
    private String categoryImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_detail);
        ButterKnife.bind(this);

        Bundle data = getIntent().getExtras();
        if (data != null) {
            selectedTemplate= (TemplateModel) data.getSerializable(AppConstants.SELECTED_TEMPLATE);
            initTabsFor = data.getInt(INIT_TABS_FOR,INIT_TABS_FOR_CREATE_TEMPLATE);
            alreadySaved = data.getBoolean(ALREADY_SAVED,false);
            categoryImage = data.getString(AppConstants.CATEGORY_IMAGE);
        }

        if(selectedTemplate==null) {
            Utils.showErrorMessage(this,getString(R.string.no_template_to_show));
            finish();
            return;
        }

        if(initTabsFor==INIT_TABS_FOR_CREATE_TEMPLATE)
        {
            tabFragments=new BaseTemplateQuestionsFragment[4];

            tabFragments[TAB_INDEX_INTRO_FOR_TEMPLATE]=CreateTemplateIntroFragment.newInstance(categoryImage);
            tabFragments[TAB_INDEX_CREATE_TEMPLATE]=CreateTemplateInputFragment.newInstance();
            tabFragments[TAB_INDEX_PREVIEW_TEMPLATE]= HtmlTemplateFragment.newInstance(HtmlTemplateFragment.HTML_PAGE_TYPE_PREVIEW);
            tabFragments[TAB_INDEX_EDIT_TEMPLATE]= TemplateEditPromptFragment.newInstance();

        }
        else if(initTabsFor==INIT_TABS_FOR_EDIT_TEMPLATE)
        {
            tabFragments=new BaseTemplateQuestionsFragment[4];

            tabFragments[TAB_INDEX_INTRO_FOR_TEMPLATE]=EmptyTemplateFragment.newInstance();;
            tabFragments[TAB_INDEX_CREATE_TEMPLATE]=EmptyTemplateFragment.newInstance();;
            tabFragments[TAB_INDEX_PREVIEW_TEMPLATE]= HtmlTemplateFragment.newInstance(HtmlTemplateFragment.HTML_PAGE_TYPE_PREVIEW);
            if (selectedTemplate.isCreatedByUser())
                tabFragments[TAB_INDEX_EDIT_TEMPLATE]= CreateCatalogFragment.newInstance();
            else
                tabFragments[TAB_INDEX_EDIT_TEMPLATE]= HtmlTemplateFragment.newInstance(HtmlTemplateFragment.HTML_PAGE_TYPE_EDIT);


        }
        else if(initTabsFor== INIT_TABS_FOR_COMPLETED_PREVIEW)
        {
            tabFragments=new BaseTemplateQuestionsFragment[4];

            tabFragments[TAB_INDEX_INTRO_FOR_TEMPLATE]= EmptyTemplateFragment.newInstance();;
            tabFragments[TAB_INDEX_CREATE_TEMPLATE]=EmptyTemplateFragment.newInstance();;
            tabFragments[TAB_INDEX_PREVIEW_TEMPLATE]= HtmlTemplateFragment.newInstance(HtmlTemplateFragment.HTML_PAGE_TYPE_COMPLETED);
            tabFragments[TAB_INDEX_EDIT_TEMPLATE]= EmptyTemplateFragment.newInstance();

        }

        else if(initTabsFor==INIT_TABS_fOR_RECEIVED_TEMPLATE)
        {
            tabFragments=new BaseTemplateQuestionsFragment[4];

            tabFragments[TAB_INDEX_INTRO_FOR_TEMPLATE]= EmptyTemplateFragment.newInstance();;
            tabFragments[TAB_INDEX_CREATE_TEMPLATE]=EmptyTemplateFragment.newInstance();;
            tabFragments[TAB_INDEX_PREVIEW_TEMPLATE]= HtmlTemplateFragment.newInstance(HtmlTemplateFragment.HTML_PAGE_TYPE_RECEIVED);
            tabFragments[TAB_INDEX_EDIT_TEMPLATE]= EmptyTemplateFragment.newInstance();


        }
        initViews();
        if(initTabsFor==INIT_TABS_FOR_EDIT_TEMPLATE)
        {
            mToolbarTitle.setVisibility(View.GONE);
            mCreatePreviewEditButtons.setVisibility(View.VISIBLE);
            tvCreateTemplate.setVisibility(View.GONE);
            loadFragment(TAB_INDEX_PREVIEW_TEMPLATE);
            tvTitle.setVisibility(View.GONE);
        }
        else if(initTabsFor== INIT_TABS_FOR_COMPLETED_PREVIEW || initTabsFor == INIT_TABS_fOR_RECEIVED_TEMPLATE) {
            mToolbarTitle.setVisibility(View.GONE);
            mCreatePreviewEditButtons.setVisibility(View.GONE);
            loadFragment(TAB_INDEX_PREVIEW_TEMPLATE);
            tvTitle.setText(selectedTemplate.getTemplateTitle());
            tvTitle.setVisibility(View.VISIBLE);
        }

    }

    private void initViews() {

        viewPagerTabs= (ViewPager) findViewById(R.id.viewPagerTabs);
        viewPagerTabs.setAdapter(new TemplateFillingTabsFragmentAdapter(getSupportFragmentManager(),tabFragments));
        viewPagerTabs.setOffscreenPageLimit(3);


        mIconCancel.setOnClickListener(this);
        tvPreviewTemplate.setOnClickListener(this);
        tvCreateTemplate.setOnClickListener(this);
        tvEditTemplate.setOnClickListener(this);


        mCreatePreviewEditButtons.setVisibility(View.GONE);
        mToolbarTitle.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.GONE);


        userID = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_USER_ID,AppConstants.NOT_FOUND);
        userName = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_FIRST_NAME,AppConstants.NOT_FOUND);
        email = StorageUtility.getDataFromPreferences(this, AppConstants.PREF_KEY_EMAIL,AppConstants.NOT_FOUND);

    }

    @Override
    public void handleClick(View view) {
        super.handleClick(view);
        switch (view.getId()) {
            case R.id.icon_cancel:
                closeActivty();

                break;
            case R.id.tvPreview:
                loadFragment(TAB_INDEX_PREVIEW_TEMPLATE);
                break;
            case R.id.tvEdit:
                loadFragment(TAB_INDEX_EDIT_TEMPLATE);
                break;
            case R.id.tvCreateTemplate:
                loadFragment(TAB_INDEX_CREATE_TEMPLATE);
                break;
        }
    }

    private void closeActivty() {

        if(initTabsFor==INIT_TABS_FOR_CREATE_TEMPLATE&&viewPagerTabs.getCurrentItem()!=TAB_INDEX_INTRO_FOR_TEMPLATE)
        {

            DialogsUtils.showConfirmationDialog(this, new DialogCallBackListener() {
                @Override
                public void onOk() {

                    tabFragments[viewPagerTabs.getCurrentItem()].submitTemplate();
                }

                @Override
                public void onDiscard() {

                    finish();
                }

                @Override
                public void onCancel() {

                }
            });

        }
        else if(initTabsFor==INIT_TABS_FOR_EDIT_TEMPLATE){
            tabFragments[viewPagerTabs.getCurrentItem()].saveTemplateAnswers(true, new DialogCallBackListener() {
                @Override
                public void onOk() {

                    finish();
                }

                @Override
                public void onDiscard() {
                    finish();
                }

                @Override
                public void onCancel() {
                    finish();
                }
            });

        }
        else finish();

    }

    public void loadFragment(final int  tabIndex) {

        if(alreadySaved&& tabIndex==TAB_INDEX_EDIT_TEMPLATE&&!inEditMode)
        {

            DialogsUtils.showEditWarningDialog(CreateEditTemplateActivity.this, new DialogCallBackListener() {
                @Override
                public void onOk() {

                    inEditMode=true;
                    Utils.showProgressDialog(CreateEditTemplateActivity.this);
                    EditPendingTemplateHandler.sendEditTemplateRequest(CreateEditTemplateActivity.this, selectedTemplate.getTemplateId(), userID, selectedTemplate.isCreatedByUser(), new ServerResponseListener() {
                        @Override
                        public void onServerResponse(GeneralResponse generalResponse) {

                            if(generalResponse!=null){
                                Utils.hideProgressDialog();
                                if(generalResponse.getStatus()==AppConstants.SERVER_RESPONSE_CODE_SUCCESS) {

                                    changeTab(tabIndex);

                                    removeReceivers();
                                }
                                else {
                                    Utils.showErrorMessage(CreateEditTemplateActivity.this,generalResponse.getMessage());
                                }
                            }

                        }
                    });

                }

                @Override
                public void onDiscard() {

                }

                @Override
                public void onCancel() {

                }
            });
        }

        else changeTab(tabIndex);


    }

    private void removeReceivers() {

        selectedTemplate.getReceivers().clear();

        for(int i=0;i<tabFragments.length;i++){

            tabFragments[i].refreshReceivers();
        }
    }

    private void changeTab(final int tabIndex) {

        if(tabFragments[viewPagerTabs.getCurrentItem()]!=null)
        {
            tabFragments[viewPagerTabs.getCurrentItem()] .saveUserInput(new SaveCompleteListener() {
                @Override
                public void onDataSavedCompleted(TemplateModel selectedTemplate) {

                    try {
                        viewPagerTabs.setCurrentItem(tabIndex);
                        tabFragments[tabIndex].repopulateData(selectedTemplate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        }

        changeTabText(tabIndex);

    }

    private void changeTabText(int tabIndex) {

        tvCreateTemplate.setTextColor(getResources().getColor(R.color.tab_un_check_color));
        tvPreviewTemplate.setTextColor(getResources().getColor(R.color.tab_un_check_color));
        tvEditTemplate.setTextColor(getResources().getColor(R.color.tab_un_check_color));
        tvCreateTemplate.setSelected(false);
        tvPreviewTemplate.setSelected(false);
        tvEditTemplate.setSelected(false);
        switch (tabIndex){

            case TAB_INDEX_CREATE_TEMPLATE:
                tvCreateTemplate.setTextColor(getResources().getColor(R.color.tab_checked_color));
                tvCreateTemplate.setSelected(true);
                break;
            case TAB_INDEX_PREVIEW_TEMPLATE:
                tvPreviewTemplate.setTextColor(getResources().getColor(R.color.tab_checked_color));
                tvPreviewTemplate.setSelected(true);
                break;
            case TAB_INDEX_EDIT_TEMPLATE:
                tvEditTemplate.setTextColor(getResources().getColor(R.color.tab_checked_color));
                tvEditTemplate.setSelected(true);
                break;
        }
    }


    public void setPreviewColor() {

        tvPreviewTemplate.setTextColor(getResources().getColor(R.color.white));
        tvCreateTemplate.setTextColor(getResources().getColor(R.color.colorAccentW));
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getUserID() {
        return userID;
    }



    public void continueToCreateTemplate() {
        mToolbarTitle.setVisibility(View.GONE);
        loadFragment(TAB_INDEX_CREATE_TEMPLATE);
        mCreatePreviewEditButtons.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.GONE);
    }

    public TemplateModel getSelectedTemplate() {
        return selectedTemplate;
    }
}
