package com.txlabz.lawnote.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.adapters.MyReceiversRecyclerViewAdapter;
import com.txlabz.lawnote.modelClasses.ContractInfo;
import com.txlabz.lawnote.modelClasses.ReceiversInfo;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import java.util.ArrayList;


public class ReceiversInputActivity extends BaseActivity implements View.OnClickListener {

    ArrayList<ReceiversInfo> receiversInfo = new ArrayList<>();
    private MyReceiversRecyclerViewAdapter receiversAdapter;
    private ContractInfo contractInfo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_receivers_list);

        initView();
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            contractInfo = (ContractInfo) bundle.getSerializable(AppConstants.INTENT_KEY_CONTRACT_INFO);
        }
        if (contractInfo != null && contractInfo.getReceivers() != null&&contractInfo.getReceivers().size()>0) {
            if (contractInfo.getReceivers().size() > 0)
                receiversInfo = contractInfo.getReceivers();
        } else
            receiversInfo.add(new ReceiversInfo());//add one item by default

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        receiversAdapter=new MyReceiversRecyclerViewAdapter(receiversInfo);
        recyclerView.setAdapter(receiversAdapter);
        findViewById(R.id.icon_cancel).setOnClickListener(this);
        findViewById(R.id.ivAdd).setOnClickListener(this);
        findViewById(R.id.btnSend).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.icon_cancel: onBackPressed();break;
            case R.id.ivAdd:addNewReceiver();break;
            case R.id.btnSend:sendTemplate();break;
        }
    }

    private void sendTemplate() {

        if(validateAllReceivers()) {
            contractInfo.setReceivers(receiversInfo);
            setResultAndNavigate(true);
        }
    }

    private boolean validateAllReceivers() {
        for(int i = 0; i< receiversInfo.size(); i++) {
            if(validateReceiver(receiversInfo.get(i))) continue;
            return false;
        }

        return true;
    }

    private boolean validateReceiver(ReceiversInfo receiversInfo) {
        if(TextUtils.isEmpty(receiversInfo.getFirstName())) {
            Utils.showErrorMessage(this,getString(R.string.msg_enter_first_name));
            return false;
        }

        if(TextUtils.isEmpty(receiversInfo.getLastName())) {
            Utils.showErrorMessage(this,getString(R.string.msg_enter_last_name));
            return false;
        }

        if(TextUtils.isEmpty(receiversInfo.getEmail())) {
            Utils.showErrorMessage(this,getString(R.string.msg_enter_email));
            return false;
        }
        return true;
    }

    private void addNewReceiver() {

        receiversInfo.add(new ReceiversInfo());
        receiversAdapter.notifyItemInserted(receiversInfo.size()-1);
    }

    @Override
    public void onBackPressed() {
        setResultAndNavigate(false);
    }

    private void setResultAndNavigate(boolean isFromBack) {

        contractInfo.setReceivers(receiversInfo);
        Intent data = new Intent();
        if (receiversInfo.size() == 1) {
            ReceiversInfo info = receiversInfo.get(0);
            if (TextUtils.isEmpty(info.getFirstName()) && TextUtils.isEmpty(info.getLastName()) && TextUtils.isEmpty(info.getEmail()))
                receiversInfo.clear();
        }
        data.putExtra(AppConstants.INTENT_KEY_RECEIVERS_LIST, receiversInfo);
        data.putExtra(AppConstants.INTENT_KEY_CONTRACT_INFO, contractInfo);
        data.putExtra(AppConstants.INTENT_KEY_SHOULD_SEND_CALL, isFromBack);
        setResult(Activity.RESULT_OK, data);
        finish();
    }
}
