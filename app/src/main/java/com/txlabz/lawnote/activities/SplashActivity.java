package com.txlabz.lawnote.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class SplashActivity extends BaseActivity {

    public static String CheckLoginfile="myLoginFile";
    SharedPreferences myLoginData;
    String templateId,isCreatedByUser,userID;
    JSONObject jsonObject= null;

    @Override
    public void onStart() {
        super.onStart();
        Branch branch = Branch.getInstance();

        branch.initSession(new Branch.BranchReferralInitListener(){
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    try {
                        jsonObject = new JSONObject(String.valueOf(referringParams));

                        templateId=jsonObject.getString(AppConstants.TEMPLATE_ID);
                        isCreatedByUser=jsonObject.getString(AppConstants.IS_CREATED_BY_USER);
                        userID=jsonObject.getString(AppConstants.PREF_KEY_USER_ID);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.i("MyApp", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        myLoginData= Utils.getSharedPreference(SplashActivity.this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Boolean isLogin=myLoginData.getBoolean(AppConstants.IS_LOGIN,false);

                if (isLogin)
                {
                    if (templateId!=null)
                    {
                        Intent intent = new Intent(SplashActivity.this,PreviewSignatureDataActivity.class);
                        Bundle bundle=new Bundle();
                        bundle.putString(AppConstants.TEMPLATE_ID,templateId);
                        bundle.putString(AppConstants.PREF_KEY_USER_ID,userID);
                        bundle.putString(AppConstants.IS_CREATED_BY_USER,isCreatedByUser);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }else {
                        Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }else {
                    Intent intent = new Intent(SplashActivity.this,WalkthroughActivity.class);
                    if (templateId!=null)
                    {
                        Bundle bundle=new Bundle();
                        bundle.putString(AppConstants.TEMPLATE_ID,templateId);
                        bundle.putString(AppConstants.PREF_KEY_USER_ID,userID);
                        bundle.putString(AppConstants.IS_CREATED_BY_USER,isCreatedByUser);
                        intent.putExtras(bundle);
                    }
                    startActivity(intent);
                    finish();
                }
            }
        },3000);
    }
}
