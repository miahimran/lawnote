package com.txlabz.lawnote.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.DialogsUtils;
import com.txlabz.lawnote.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener{

    EditText emailAddress;
    Button send_btn;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getWindow().setBackgroundDrawableResource(R.drawable.background_login);
        init();

        emailAddress=(EditText)findViewById(R.id.emailAddress);
        send_btn=(Button)findViewById(R.id.send_btn);
        progressDialog=new ProgressDialog(this);

        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.IsNetworkConnected(getApplicationContext()))
                {
                    String email=emailAddress.getText().toString();
                    if (email.length()==0)
                    {
                       DialogsUtils.showAlert(ForgotPasswordActivity.this, AppConstants.ENTER_EMAIL);
                    }else {

                        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                        if (email.matches(emailPattern))
                        {
                            progressDialog.setMessage(AppConstants.LOADING);
                            progressDialog.show();


                            Ion.with(getApplicationContext())
                                    .load(AppConstants.API_FORGOT_PASSWORD)
                                    .setBodyParameter("email", email)
                                    .asString()
                                    .setCallback(new FutureCallback<String>() {
                                        @Override
                                        public void onCompleted(Exception e, String result) {

                                            progressDialog.dismiss();
                                            String status;
                                            String message;

                                            try {
                                                JSONObject jsonObject=new JSONObject(result);

                                                status=jsonObject.getString(AppConstants.STATUS);
                                                message=jsonObject.getString(AppConstants.MESSAGE);


                                                if (Integer.parseInt(status)==1)
                                                {
                                                    DialogsUtils.showAlert(ForgotPasswordActivity.this, message);

                                                }
                                                else {
                                                    DialogsUtils.showAlert(ForgotPasswordActivity.this, AppConstants.EMAIL_NOT_EXIST);
                                                }

                                            } catch (JSONException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    });

                        }else {
                            DialogsUtils.showAlert(ForgotPasswordActivity.this, AppConstants.CHECK_VALID_EMAIL);

                        }

                    }
                }else {
                    DialogsUtils.showAlert(ForgotPasswordActivity.this, AppConstants.CHECK_CONNECTION);
                }
            }
        });

    }

    private void init() {
        TextView backToLogin = (TextView) findViewById(R.id.lblBackToLogin);


        backToLogin.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lblBackToLogin:
                startLoginActivity();
                break;
        }
    }



    private void startLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public void onBackPressed(){
        startLoginActivity();
    }

}
