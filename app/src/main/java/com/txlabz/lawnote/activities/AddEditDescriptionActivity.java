package com.txlabz.lawnote.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;
import com.txlabz.lawnote.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddEditDescriptionActivity extends BaseActivity {
    @BindView(R.id.tv_question)
    TextView mQuestion;
    @BindView(R.id.et_description_details)
    EditText mDescriptionDetails;
    @BindView(R.id.icon_cancel)
    ImageView mCancel;
    @BindView(R.id.tv_done)
    TextView mDone;
    private int position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_description);
        ButterKnife.bind(this);

        initViews();
    }

    private void initViews() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String text = bundle.getString(AppConstants.QUESTION_TEXT, "");
            String desc = bundle.getString(AppConstants.QUESTION_DESC, "");
            position    = bundle.getInt(AppConstants.POSITION);
            mQuestion.setText(text);
            mDescriptionDetails.setText(desc);
        }
        mCancel.setOnClickListener(this);
        mDone.setOnClickListener(this);
    }

    @Override
    protected void handleClick(View view) {
        super.handleClick(view);
        switch (view.getId()) {
            case R.id.icon_cancel:
                finish();
                break;
            case R.id.tv_done:
                setResultAndNavigate();
                break;
        }
    }

    private void setResultAndNavigate() {
        Intent data = new Intent();
        data.putExtra(AppConstants.QUESTION_DESC, mDescriptionDetails.getText().toString());
        data.putExtra(AppConstants.POSITION, position);
        setResult(Activity.RESULT_OK, data);
        finish();
    }
}
