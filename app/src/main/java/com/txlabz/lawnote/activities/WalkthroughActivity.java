package com.txlabz.lawnote.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.txlabz.lawnote.R;
import com.txlabz.lawnote.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WalkthroughActivity extends BaseActivity implements View.OnClickListener{
    public static final int OPENED_FROM_SPLASH = 1;
    public  static final int OPENED_FROM_DRAWER = 2;
    private ArrayList<String> al;
    private ArrayAdapter<String> arrayAdapter;
    private int i;

    @BindView(R.id.frame)
    SwipeFlingAdapterView flingContainer;
    ImageView indicator1, indicator2, indicator3, indicator4;
    int position = 0;
    Bundle data;
    private int openedFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);
        ImageView iconSkip = (ImageView) findViewById(R.id.iconSkip);
        indicator1 = (ImageView) findViewById(R.id.indicator1);
        indicator2 = (ImageView) findViewById(R.id.indicator2);
        indicator3 = (ImageView) findViewById(R.id.indicator3);
        indicator4 = (ImageView) findViewById(R.id.indicator4);

        iconSkip.setOnClickListener(this);

        ButterKnife.bind(this);

        data=getIntent().getExtras();
      openedFrom=  getIntent().getIntExtra(AppConstants.PUT_EXTRA_KEY_OPENED_FROM,OPENED_FROM_SPLASH);


        al = new ArrayList<>();
        al.add("Create, sign and manage legal documents on the go");
        al.add("Save time and increase efficiency");
        al.add("Lawnote offers everything you need for legal documents  that are easy to understand. With clear terms, all parties know what to expect.");
        al.add("Close deals faster increase productivity for your personal or business needs Reduce time spent on administrative tasks.");
        al.add("Collaborate with your legal documents with ease");

        arrayAdapter = new ArrayAdapter<>(this, R.layout.card_content, R.id.desc, al );


        flingContainer.setAdapter(arrayAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                String item = arrayAdapter.getItem(0);
                al.remove(0);
                arrayAdapter.notifyDataSetChanged();
                al.add(item);
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                setIndicatorImage();
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                setIndicatorImage();
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {

            }
        });
        setIndicatorImage();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iconSkip:
                skipIntro();
                break;
        }
    }

    private void skipIntro() {

        if(openedFrom==OPENED_FROM_DRAWER)
        {
            finish();
            return;
        }

        Intent intent = null;
        intent = new Intent(this, LoginActivity.class);
        if (data!=null)
        {
            intent.putExtras(data);
        }
        startActivity(intent);
        finish();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void setIndicatorImage(){
        int pos = position % 4;
        int dotSelected = 0, dotUnSelected = 0;
        dotSelected = R.drawable.indicator_active;
        dotUnSelected = R.drawable.indicator_inactive;

        switch (pos){
            case 0:
                indicator1.setImageResource(dotSelected);
                indicator2.setImageResource(dotUnSelected);
                indicator3.setImageResource(dotUnSelected);
                indicator4.setImageResource(dotUnSelected);
                break;
            case 1:
                indicator1.setImageResource(dotUnSelected);
                indicator2.setImageResource(dotSelected);
                indicator3.setImageResource(dotUnSelected);
                indicator4.setImageResource(dotUnSelected);
                break;
            case 2:
                indicator1.setImageResource(dotUnSelected);
                indicator2.setImageResource(dotUnSelected);
                indicator3.setImageResource(dotSelected);
                indicator4.setImageResource(dotUnSelected);
                break;
            case 3:
                indicator1.setImageResource(dotUnSelected);
                indicator2.setImageResource(dotUnSelected);
                indicator3.setImageResource(dotUnSelected);
                indicator4.setImageResource(dotSelected);
                break;
        }
        position++;
    }
}
